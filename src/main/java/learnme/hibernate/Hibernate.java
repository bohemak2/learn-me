package learnme.hibernate;

import java.io.File;
import java.util.EnumSet;
import org.hibernate.Session;

import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.hibernate.tool.schema.TargetType;

import learnme.application.AppConfig;
import learnme.hibernate.entities.*;
import learnme.hibernate.entities.user.*;
import learnme.hibernate.entities.content.*;
import learnme.hibernate.entities.assignment.*;

public class Hibernate {
    private final static String CFG = "hibernate-cfg.xml";
    private final static String SCRIPT_FILE = "query.sql";

    private static SessionFactory sessionFactory;
    
    private static ServiceRegistry buildRegistry() {
        return new StandardServiceRegistryBuilder()
                .configure(CFG)
                .build();
    }

    private static Metadata getMetaData() {
        return new MetadataSources(buildRegistry()).getMetadataBuilder().build();
    }

    private static Configuration cleanConfig() {
    	Configuration config = new Configuration();
    	
    	config.setProperty("hibernate.connection.username", "root")
	    	.setProperty("hibernate.connection.password", "")
	    	.setProperty("hibernate.connection.pool_size", "100");
	    	
    	if (AppConfig.DEBUG) {
	    	config.setProperty("hibernate.hbm2ddl.auto", "create-drop");
	    } else {
	    	config.setProperty("hibernate.hbm2ddl.auto", "update");
	    }
    	
    	config.setProperty("hibernate.enable_lazy_load_no_trans", "true");
    	config.setProperty("hibernate.event.merge.entity_copy_observer", "allow");
    	
    	if (AppConfig.DEBUG) {
    		config.setProperty("hibernate.show_sql", "true");
    		config.setProperty("hibernate.format_sql", "true");
    		config.setProperty("hibernate.use_sql_comments", "true");
    	}
    	
    	config.addAnnotatedClass(User.class)
	    	.addAnnotatedClass(Place.class)
	    	.addAnnotatedClass(Course.class)
	        .addAnnotatedClass(User.class)
	        .addAnnotatedClass(Place.class)
	        .addAnnotatedClass(Course.class)
	        .addAnnotatedClass(Tutor.class)
	        .addAnnotatedClass(Student.class)
	        .addAnnotatedClass(Chapter.class)
	        .addAnnotatedClass(Content.class)
	        .addAnnotatedClass(Html.class)
	        .addAnnotatedClass(Pdf.class)
	        .addAnnotatedClass(Video.class)
	        .addAnnotatedClass(Review.class)
	        .addAnnotatedClass(Significancy.class)
	        .addAnnotatedClass(Activity.class)
	        .addAnnotatedClass(Assignment.class)
	        .addAnnotatedClass(Event.class)
	        .addAnnotatedClass(Task.class)
	        .addAnnotatedClass(Test.class)
	        .addAnnotatedClass(Result.class)
	        .addAnnotatedClass(Field.class)
	        .addAnnotatedClass(Tag.class)
	        .addAnnotatedClass(Conversation.class)
	        .addAnnotatedClass(Message.class)
	        .addAnnotatedClass(Notification.class)
	        .addAnnotatedClass(File.class);
    	
    	return config;
    }
    
    private static SessionFactory buildSessionFactory() {
    	
    	try {
    		Configuration config = cleanConfig();
    		config.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");
    		config.setProperty("hibernate.connection.url", "jdbc:mysql://localhost:3306/learnme");
    		config.setProperty("hibernate.connection.driver_class", "com.mysql.jdbc.Driver");
    		
    		SessionFactory factory = config.buildSessionFactory();
    		// TEST MYSQL, ELSE HSQL	
    		factory.openSession().beginTransaction();
    		
    		return factory;
        } catch (Exception e){
        	Configuration config = cleanConfig();
    		config.setProperty("hibernate.dialect", "org.hibernate.dialect.HSQLDialect");
    		config.setProperty("hibernate.connection.url", "jdbc:hsqldb:hsql//localhost/learnme");
    		config.setProperty("hibernate.connection.driver_class", "org.hsqldb.jdbc.JDBCDriver");
    		return config.buildSessionFactory();
        }
    }

    public static SessionFactory getSessionFactory() {
    	return getSessionFactory(false);
    }
    
	public static SessionFactory getSessionFactory(boolean reconnect) {
        if (sessionFactory == null || reconnect) {
        	sessionFactory = buildSessionFactory();
        }
        return sessionFactory;
    }
    
    public static Session getSessionEntityFactory(){
    	try {
    		Session session = getSessionFactory().getCurrentSession();
    		if (session.isOpen() && session.isConnected()) { // always false because of isConnected is always false - why ? -> session is closed automatically
    			return session;
    		} else {
    			throw new Exception();
    		}
    	} catch(Exception e) {
    		return getSessionFactory().openSession(); // create new Session each time -> not efficient, but needed because of AjaxRequest -> session has to be closed
    	}
    }
    
    public static Session getSession(){
    	Session session;
    	try {
        	session = getSessionEntityFactory().getEntityManagerFactory().createEntityManager().unwrap(Session.class);
        	if(!session.getTransaction().isActive()) {
        		session.beginTransaction();
        	}
        } catch (Exception e) {
        	try {
	        	getSessionFactory(true);
	        	session = getSessionEntityFactory().getEntityManagerFactory().createEntityManager().unwrap(Session.class);
        	} catch (Exception e1) {
        		e.printStackTrace();
        		return null;
        	}
        }
        
    	return session;
    }
    
     public static void closeSession(){
        try {
            getSessionFactory().getCurrentSession().close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    

    // Close caches and connection pools
    public void shutdown() {
        getSessionFactory().close();
    }
    
    protected static class Generator {

        private static void generate() {
            SchemaExport export = getSchemaExport();

            System.out.println("Drop Database...");
            dropDataBase(export, getMetaData());

            System.out.println("Create Database...");
            createDataBase(export, getMetaData());
        }

        private static SchemaExport getSchemaExport() {
            SchemaExport export = new SchemaExport();
            // Script file.
            File outputFile = new File(SCRIPT_FILE);
            outputFile.delete();
            
            String outputFilePath = outputFile.getAbsolutePath();

            System.out.println("Export file: " + outputFilePath);

            export.setDelimiter(";");
            export.setOutputFile(outputFilePath);

            // No Stop if Error
            export.setHaltOnError(false);
            return export;
        }

        // TargetType.DATABASE - Execute on Databse
        // TargetType.SCRIPT - Write Script file.
        // TargetType.STDOUT - Write log to Console.
        public static void dropDataBase(SchemaExport export, Metadata metadata) {
            EnumSet<TargetType> targetTypes = EnumSet.of(TargetType.DATABASE, TargetType.SCRIPT, TargetType.STDOUT);
            export.drop(targetTypes, metadata);
        }

        public static void createDataBase(SchemaExport export, Metadata metadata) {
            EnumSet<TargetType> targetTypes = EnumSet.of(TargetType.DATABASE, TargetType.SCRIPT, TargetType.STDOUT);
            SchemaExport.Action action = SchemaExport.Action.CREATE;
            export.execute(targetTypes, action, metadata);
            System.out.println("Export OK");
        }
    }

    public static void main(String[] args) {
        Generator.generate();
    }
}
