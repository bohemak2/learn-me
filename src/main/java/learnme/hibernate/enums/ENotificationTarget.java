package learnme.hibernate.enums;

public enum ENotificationTarget {
	COURSE_STUDENT, COURSE_TUTOR,
	POST_MESSAGE, POST_WALL,
	USER_FOLLOW,
	ASSIGNMENT, ASSIGNMENT_COURSE,
	
	SYSTEM_WELLCOME;
}
