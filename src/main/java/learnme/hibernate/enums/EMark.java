package learnme.hibernate.enums;

public enum EMark {
	TOTAL, COURSE, USER;
	
	public static final int MAX_MARK = 5;
}
