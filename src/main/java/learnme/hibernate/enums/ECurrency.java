package learnme.hibernate.enums;

public enum ECurrency {
    EUR, USD, CZK;
}
