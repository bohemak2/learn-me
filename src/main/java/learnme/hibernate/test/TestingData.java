package learnme.hibernate.test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.hibernate.Session;

import learnme.hibernate.entities.Chapter;
import learnme.hibernate.entities.Conversation;
import learnme.hibernate.entities.Course;
import learnme.hibernate.entities.Field;
import learnme.hibernate.entities.Message;
import learnme.hibernate.entities.Place;
import learnme.hibernate.entities.Significancy;
import learnme.hibernate.entities.Tag;
import learnme.hibernate.entities.User;
import learnme.hibernate.entities.assignment.Event;
import learnme.hibernate.entities.content.Html;
import learnme.hibernate.entities.content.Video;
import learnme.hibernate.enums.ECurrency;

public class TestingData {
	
	public static User generateTestUser(String name, String surname, String email, Session session) {
		List<User> users = new ArrayList<>();
		User me = new User(name, surname, email);
		User frasier, martin, daphne, lilith, bulldog, roz, eddie, kenny, gill, frederick, noel, sherry, faye;

		users.add(me);
		users.add(new User("Maris", "Crane", "ghost@frasier.com"));
		users.add(frasier = new User("Frasier", "Crane", "frasier@frasier.com"));
		users.add(lilith = new User("Lilith", "Sternin", "devil@frasier.com"));
		users.add(martin = new User("Martin", "Crane", "martin@frasier.com"));
		users.add(frederick = new User("Frederick", "Crane", "frederick@frasier.com"));
		users.add(daphne = new User("Daphne", "Moon", "daphne@frasier.com"));
		users.add(eddie = new User("Eddie", null, "eddie@frasier.com"));
		users.add(roz = new User("Roz", "Doyle", "roz@frasier.com"));
		users.add(new User("Ellis", "Doyle", "ellis@frasier.com"));
		users.add(bulldog = new User("Bob", "Briscoe", "bulldock@frasier.com"));
		users.add(gill = new User("Gil", "Chesterton", "cooker@frasier.com"));
		users.add(kenny = new User("Kenny", "Dally", "boss@frasier.com"));		
		users.add(noel = new User("Noel", "Schempsky", "noel@frasier.com"));		
		users.add(sherry = new User("Sherry", "Dempsey", "sherry@frasier.com"));
		users.add(new User("Mel", "Karnofsky", "mel@frasier.com"));
		users.add(faye = new User("Faye", "Moskowitz", "faye@frasier.com"));
		
		
		frasier.addToFollow(me);
		frasier.addToFollow(lilith);
		frasier.addToFollow(frederick);
		
		daphne.addToFollow(me);
		
		martin.addToFollow(me);
		martin.addToFollow(eddie);
		martin.addToFollow(frasier);
		martin.addToFollow(daphne);
		
		bulldog.addToFollow(roz);
		noel.addToFollow(roz);
		
		kenny.addToFollow(bulldog);
		kenny.addToFollow(roz);
		kenny.addToFollow(frasier);
		kenny.addToFollow(gill);
		kenny.addToFollow(me);
		kenny.addToFollow(noel);
		
		sherry.addToFollow(martin);
		faye.addToFollow(frasier);
				
		if (session != null) {
			users.forEach((user) -> {
				session.persist(user);
			});
		}
		
		Place seattle = new Place();
		seattle.setCity("Seattle");
		seattle.setAxis(new Place.Axis(5272948.10f, 549967.97f));
		session.persist(seattle);
		
		Calendar today = new GregorianCalendar();
		today.setTime(new Date());
		
		kenny.removeFromFollow(me);
		
		gill.addPost(new Conversation() {
			{
				addMessage(new Message() {
					{
						setText("Visited Flavour of Greek restaurant. They totally deserves this name! 1/5...");
						setUser(gill);
					}
				});
			}
		});
		

		frasier.addPost(new Conversation() {
			{
				addMessage(new Message() {
					{
						setText("I am posting interesting topic each time...");
						setUser(frasier);
					}
				});

				addMessage(new Message() {
					{
						setText("I don't thing.");
						setUser(martin);
					}
				});
				
				addMessage(new Message() {
					{
						setText("Yes, it is rubish.");
						setUser(kenny);
					}
				});
			}
		});
		
		bulldog.addPost(new Conversation() {
			{
				addMessage(new Message() {
					{
						setText("I have already started some interesting fitness courses. You should check it now until they are free...");
						setUser(bulldog);
					}
				});

				addMessage(new Message() {
					{
						setText("Looks good, thanks for echo.");
						setUser(daphne);
					}
				});
			}
		});
		
		roz.addPost(new Conversation() {
			{
				addMessage(new Message() {
					{
						setText("Is anyone planning something tonight?");
						setUser(roz);
					}
				});

				addMessage(new Message() {
					{
						setText("I am going to theatre with Niles today. Join us!");
						setUser(daphne);
					}
				});
				
				addMessage(new Message() {
					{
						setText("What are you going to see?");
						setUser(roz);
					}
				});
				
				addMessage(new Message() {
					{
						setText("La Fille Mal Gardée, it is a ballet.");
						setUser(daphne);
					}
				});
				
				addMessage(new Message() {
					{
						setText("No, thanks :-) I would rather be at home.");
						setUser(roz);
					}
				});
				
				addMessage(new Message() {
					{
						setText("Come to my place.");
						setUser(bulldog);
					}
				});
			}
		});
		
		frasier.addPost(new Conversation() {
			{
				addMessage(new Message() {
					{
						setText("Tommorow, I am starting Wine sommelier course. Anyone else to join?");
						setUser(frasier);
					}
				});
			}
		});
		
		me.addPost(new Conversation() {
			{
				addMessage(new Message() {
					{
						setText("I was in Metropolitan this weekend. It was awesome. Everyone should see it!");
						setUser(me);
					}
				});
				
				addMessage(new Message() {
					{
						setText("You have never been in Prado, haven't you? Then you couldn't say that.");
						setUser(me);
					}
				});
			}
		});
		
		noel.addPost(new Conversation() {
			{
				addMessage(new Message() {
					{
						setText("Comic-Con in Vegas just in next month! I can't wait...");
						setUser(noel);
					}
				});
			}
		});
		
		me.addPost(new Conversation() {
			{
				addMessage(new Message() {
					{
						setText("Me, Frasier and Lilith prepared course of Psychology.");
						setUser(me);
					}
				});

				addMessage(new Message() {
					{
						setText("Yes, it is a quick brief into Psychology. Very usefull.");
						setUser(frasier);
					}
				});

			}
		});
		
		me.addConversation(new Conversation() {
			{
				addUser(me);
				addUser(frasier);
				
				addMessage(new Message() {
					{
						setText("Hi Niles. Wouldn't you mind of taking my wine course?");
						setUser(frasier);
					}
				});

				addMessage(new Message() {
					{
						setText("Why should I do that? You hardly improve my knowledge about wine...");
						setUser(me);
					}
				});
				
				addMessage(new Message() {
					{
						setText("Ok, never mind. Anyway, which chapter will you take for Lilith's philosophy course?");
						setUser(frasier);
					}
				});
				
				addMessage(new Message() {
					{
						setText("I was thinking about Jung... And you?");
						setUser(me);
					}
				});
				
				addMessage(new Message() {
					{
						setText("Freud...");
						setUser(frasier);
					}
				});
				
				addMessage(new Message() {
					{
						setText("Then it is settled.");
						setUser(me);
					}
				});
			}
		});
		
		Field music = new Field("Music");
		session.persist(music);
		
		Course dvorak;
		
		me.addCourseAsTutor(dvorak = new Course(me) {
			{
				setName("Dvorak's piano");
				setDescription("Beauty of Bohemian virtuous. Take a moment to appreciate his best opuses. Ability of piano control is not required.");
				setCapacity(3);
				
				setFee(120);
				setCurrency(ECurrency.USD);
				
				addTag(new Tag("Piano", music));
				addTag(new Tag("Antonin Dvorak", music));
				
				addStudent(roz);
				addStudent(faye);

				setAutoacceptance(true);
				setActive(true);
			}
		});

		Field sport = new Field("Sport");
		session.persist(sport);

		bulldog.addCourseAsTutor(new Course(bulldog) {
			{
				setName("Fitness coaching");
				setDescription("Do you want to be slim and fit? It is easy with me. Just enroll this course and after 14 days you will suit to swimming clothes.");
				setCapacity(1);

				addTag(new Tag("Fitness", sport));
				addTag(new Tag("Loosing weight", sport));

				addTutor(bulldog);

				addStudent(daphne);
				
				Chapter chapter;

				addChapter(chapter = new Chapter("Basics of fitness",
						"In this chapter, I will show you some basic excercises.", 30) {
					{

						/* NEED TO BE INSTANTILIZED DUE Hibernate extends -> then can't resolve class */
						Html htmlContent = new Html();
						htmlContent.setHtml("<p>First of all, you have to control your <b>body</b>.</p>");

						addContent(htmlContent);
					}
				});
				chapter.setAuthor(bulldog);

				addChapter(chapter = new Chapter("Warming up", "By warm up body hould starts each excercise.", 15) {
					{

						/* NEED TO BE INSTANTILIZED DUE Hibernate extends -> then can't resolve class */
						Html htmlContent = new Html();
						htmlContent.setHtml("<p>Warm <b>hands</b> and <b>legs</b>.</p>");

						Html htmlContent2 = new Html();
						htmlContent2.setHtml("<p>Rotate with <b>head</b> and <b>body</b>.</p>");

						Video videoContent = new Video();
						videoContent.setUrl("/upload/videos/warming.mp4");

						addContent(htmlContent);
						addContent(htmlContent2);
						addContent(videoContent);
					}
				});
				chapter.setAuthor(bulldog);

				setAutoacceptance(true);
				setActive(true);
			}
		});

		Field food = new Field("Food");
		session.persist(food);
		
		frasier.addCourseAsTutor(new Course(frasier) {
			{
				setName("Wine sommelier");
				setDescription("Nothing is more important than our life and pleasures that brings. I am offering one of the pearl of life's joys - wine courses.");
				setCapacity(3);

				addTag(new Tag("Wine", food));

				addTutor(frasier);
				
				addStudent(roz);

				setAutoacceptance(true);
				setActive(true);
			}
		});
		
		Field skills = new Field("Skills");
		session.persist(skills);
		
		gill.addCourseAsTutor(new Course(gill) {
			{
				setName("The 10 best tricks in Cooking");
				setDescription("Prepare your tongue for ecstasy! I will show you the best technics for cooking. You will never go to restaurant again.");
				setCapacity(10);

				setFee(20);
				setCurrency(ECurrency.USD);

				addTag(new Tag("Cooking", skills));
				addTag(new Tag("Tricks in Kitchen", food));

				addTutor(gill);
				
				addStudent(faye);
				addStudent(kenny);

				setAutoacceptance(true);
				setActive(true);
			}
		});
		
		frasier.addCourseAsTutor(new Course(frasier) {
			{
				setName("Chess");
				setDescription("King's game. Control the board and impress your friends.");
				setCapacity(3);
				
				setFee(30);
				setCurrency(ECurrency.USD);
				
				addTag(new Tag("Chess", sport));
				addTag(new Tag("Chess", skills));

				addTutor(frasier);
				
				addStudent(frederick);
				addStudent(martin);

				setAutoacceptance(true);
				setActive(true);
			}
		});

		Field psychology = new Field("Psychology");
		session.persist(psychology);

		lilith.addCourseAsTutor(new Course(lilith) {
			{
				setName("Psychology I.");
				setDescription("Introduction to psychology. Take first steps into magic of human's mind. Now without fee!");
				setCapacity(5);

				addTag(new Tag("Introduction", psychology));

				addTutor(lilith);
				
				addStudent(kenny);
				
				setAutoacceptance(true);
				setActive(true);
			}
		});
		
		lilith.addCourseAsTutor(new Course(lilith) {
			{
				setName("Psychology II.");
				setDescription("Intensive course of intermediate psychology with Dr. Sternin et al. Make one the most decisions in your life - Freud or Jung?");
				setCapacity(10);

				addTag(new Tag("Sigmund Freud", psychology));
				addTag(new Tag("C. G. Jung", psychology));

				setFee(200);
				setCurrency(ECurrency.USD);
				
				addTutor(lilith);
				addTutor(frasier);
				addTutor(me);

				addStudent(kenny);
				
				setAutoacceptance(true);
				setActive(true);
			}
		});

		Course basketball;
		bulldog.addCourseAsTutor(basketball = new Course(bulldog) {
			{
				setName("One hand Basketball's tricks");
				setDescription("Like LeBron James or Shaq - be the best and charm your friends.");
				setCapacity(15);

				addTag(new Tag("Basketball", sport));
				addTag(new Tag("Basketball Tricks", skills));
				
				addTutor(bulldog);
				
				addStudent(me);
				addStudent(kenny);
				addStudent(frasier);

				setAutoacceptance(true);
				setActive(true);
			}
		});
		
		if (session != null) {
			users.forEach((user) -> {
				session.persist(user);
			});
		}
		
		/* JUST FOR TEST DATA - IN PRODUCTION -> course.addAssignment() // inside notifications */
		Event assignment = new Event();
			assignment.setSince(today.getTime());
		assignment.setName("Op. 46, 72. - Slavonic Dances");
		assignment.setSender(me);
		assignment.setCourse(dvorak);
		assignment.setPlace(seattle);
			/* JUST FOR TEST DATA - Not persisted students yet */
			assignment.addUser(me);
			assignment.addUser(roz);
			assignment.addUser(faye);
		session.persist(assignment);
		
		assignment = new Event();
			today.add(Calendar.DAY_OF_MONTH, +7);
			assignment.setSince(today.getTime());
			today.add(Calendar.DAY_OF_MONTH, -7);
		assignment.setName("Op. 95 - New World");
		assignment.setSender(me);
		assignment.setCourse(dvorak);
		assignment.setPlace(seattle);
			/* JUST FOR TEST DATA - Not persisted students yet */
			assignment.addUser(me);
			assignment.addUser(roz);
			assignment.addUser(faye);
		session.persist(assignment);
		
		assignment = new Event();
			today.add(Calendar.DAY_OF_MONTH, +14);
			assignment.setSince(today.getTime());
			today.add(Calendar.DAY_OF_MONTH, -14);	
		assignment.setSender(me);
		assignment.setCourse(dvorak);
		assignment.setName("Op. 88 - G Major");
		assignment.setPlace(seattle);
			/* JUST FOR TEST DATA - Not persisted students yet */
			assignment.addUser(me);
			assignment.addUser(roz);
			assignment.addUser(faye);
		session.persist(assignment);
		
		assignment = new Event();
			today.add(Calendar.DAY_OF_MONTH, +21);
			assignment.setSince(today.getTime());
			today.add(Calendar.DAY_OF_MONTH, -21);
		assignment.setSender(me);
		assignment.setCourse(dvorak);
		assignment.setName("Op. 62 - Homeland");
		assignment.setPlace(seattle);
			/* JUST FOR TEST DATA - Not persisted students yet */
			assignment.addUser(me);
			assignment.addUser(roz);
			assignment.addUser(faye);
		session.persist(assignment);
		
		assignment = new Event();
			today.add(Calendar.DAY_OF_MONTH, +3);
			assignment.setSince(today.getTime());
			today.add(Calendar.DAY_OF_MONTH, -3);
		assignment.setCourse(basketball);
		assignment.setName("First session");
		assignment.setDescription("I would like to say hello and start this course. Presence required!");
		assignment.setPlace(seattle);
			/* JUST FOR TEST DATA - Not persisted students yet */
			assignment.addUser(me);
			assignment.addUser(kenny);
			assignment.addUser(frasier);
			/* END */
		session.persist(assignment);
		/* END */
		
		assignment = new Event();
			today.add(Calendar.DAY_OF_MONTH, +30);
			assignment.setSince(today.getTime());
			today.add(Calendar.DAY_OF_MONTH, -30);
		assignment.setSender(frasier);
		assignment.setName("Coffee Niles + Frasier");
		assignment.addUser(me);
		assignment.addUser(frasier);
		session.persist(assignment);
		
		return me;
	}
}
