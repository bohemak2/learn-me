package learnme.hibernate.dto;

import java.io.Serializable;

public class DTOCalendarEvent implements Serializable {
	public String title;
	public String start;
	public String end;
	public String color;
}
