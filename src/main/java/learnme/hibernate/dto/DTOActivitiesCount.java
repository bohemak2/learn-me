package learnme.hibernate.dto;

import java.io.Serializable;

import learnme.hibernate.enums.EActivityType;

public class DTOActivitiesCount implements Serializable {
	public EActivityType type;
	public long count;
}
