package learnme.hibernate.dto;

import java.io.Serializable;

import learnme.hibernate.entities.Field;

public class DTOFieldsCount implements Serializable {
	public Field field;
	public long count;
}
