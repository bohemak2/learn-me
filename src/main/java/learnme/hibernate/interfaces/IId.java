package learnme.hibernate.interfaces;

import learnme.hibernate.BasicEntity;

public interface IId <T extends BasicEntity>{
	int getId();
	boolean equals(T entity);
	int hashCode();
}
