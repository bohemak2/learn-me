package learnme.hibernate.interfaces;

import java.util.Set;

import org.hibernate.Criteria;

import learnme.hibernate.entities.Conversation;

public interface IPostable {
	Set<Conversation> getPosts();
	Criteria getLastPosts();
}
