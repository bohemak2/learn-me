package learnme.hibernate.interfaces;

import java.io.Serializable;

public interface IPositioned extends Serializable{
	int getPosition();
	void setPosition(int position);
}
