package learnme.hibernate.interfaces;

import java.util.Date;

public interface ICreated {
    public Date getCreated();
    public void setCreated(Date created);
}
