package learnme.hibernate.interfaces;

import org.hibernate.Session;

public interface IPersist {
    Session persist();
}
