package learnme.hibernate.interfaces;

import org.hibernate.Criteria;

public interface IMessagable {
	Criteria getLastMessages();
}
