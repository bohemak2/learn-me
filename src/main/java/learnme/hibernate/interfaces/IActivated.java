package learnme.hibernate.interfaces;

import java.io.Serializable;

public interface IActivated extends Serializable{
	public void setActive(boolean active);
	public boolean isActive();
}
