package learnme.hibernate.interfaces;

import learnme.hibernate.entities.Course;

public interface ICoursable extends IActivated {
	Course getCourse();
}
