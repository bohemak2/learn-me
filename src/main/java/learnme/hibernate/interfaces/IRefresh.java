package learnme.hibernate.interfaces;

import org.hibernate.Session;

public interface IRefresh {
	Session refresh();
}
