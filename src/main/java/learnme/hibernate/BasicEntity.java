package learnme.hibernate;

import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import org.hibernate.Session;

import learnme.hibernate.interfaces.IId;
import learnme.hibernate.interfaces.IPersist;
import learnme.hibernate.interfaces.IRefresh;

public abstract class BasicEntity implements IId, IPersist, IRefresh, Serializable {
	
    public BasicEntity() {
        super();

        invokeAnnotation(PrePersist.class);
    }
    
    public Session refresh() {
    	return refresh(false);
    }
    
    public Session refresh(boolean commit) {
    	Session session = Hibernate.getSession();
    	session.refresh(this);
    	return session;
    }

    public Session persist() {
        return persist(Hibernate.getSession());
    }
    
    public Session persist(boolean close) {
        return persist(Hibernate.getSession(), false);
    }
    
    public Session persist(Session session) {
        return persist(session, true);
    }

    public Session persist(Session session, boolean close) {
        invokeAnnotation(PreUpdate.class);

        if (!session.getTransaction().isActive()) {
        	session.getTransaction().begin();
        }


        if (session.contains(this)) {
            session.persist(this);
        } else {
        	session.merge(this);
        }
        
        try {
        	session.getTransaction().commit();
        } catch(Exception e) {
        	session.getTransaction().rollback();
        	e.printStackTrace();
        }
        
        if (close) {
            session.close();
        }
        return session;
    }

    private void invokeAnnotation(Class<? extends Annotation> annotationClass) {
        try {
        	Class<?> clazz = getClass();
        	if(clazz.getSuperclass() != BasicEntity.class) {
        		clazz = clazz.getSuperclass();
        	}
        	
        	for (Method method : clazz.getDeclaredMethods()) {
                for (Annotation annotation : method.getAnnotations()) {
                    if (annotation.annotationType() == annotationClass) {
                        try {
                            method.invoke(this);
                        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }
    
    @Override
    public boolean equals(BasicEntity entity) {
    	return this.getId() == entity.getId();
    }
    
    @Override
    public int hashCode() {
    	return this.getId();
    }
}
