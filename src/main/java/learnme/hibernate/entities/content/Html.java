package learnme.hibernate.entities.content;

import javax.persistence.Entity;
import javax.persistence.Table;

import learnme.hibernate.entities.Content;

@Entity
@Table(name = "content_html")
public class Html extends Content{

	private String html;
	 
	@Override
	public void setContent(Object content) {
		 setHtml(content.toString());
	}

	@Override
	public String getContent() {
		return html;
	}

	public String getHtml() {
		return html;
	}

	public void setHtml(String html) {
		this.html = html;
	}
}