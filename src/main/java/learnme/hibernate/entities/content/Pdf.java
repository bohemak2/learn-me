package learnme.hibernate.entities.content;

import javax.persistence.Entity;
import javax.persistence.Table;

import learnme.hibernate.entities.Content;

@Entity
@Table(name = "content_pdf")
public class Pdf extends Content{
	
	private String url;
	 
	@Override
	public void setContent(Object content) {
		 setUrl(content.toString());
	}

	@Override
	public String getContent() {
		return url;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
