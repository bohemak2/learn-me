package learnme.hibernate.entities.assignment;

import javax.persistence.Entity;
import javax.persistence.Table;

import learnme.hibernate.entities.Assignment;

@Entity
@Table(name = "assignment_test")
public class Test extends Assignment {
    private boolean random;

    public boolean isRandom() {
        return random;
    }

    public void setRandom(boolean random) {
        this.random = random;
    }
}
