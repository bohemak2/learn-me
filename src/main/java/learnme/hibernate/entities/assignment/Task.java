package learnme.hibernate.entities.assignment;

import javax.persistence.Entity;
import javax.persistence.Table;

import learnme.hibernate.entities.Assignment;
import learnme.hibernate.entities.File;

@Entity
@Table(name = "assignment_task")
public class Task extends Assignment {
	
    private File file;

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }
}
