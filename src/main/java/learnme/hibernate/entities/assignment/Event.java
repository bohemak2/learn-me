package learnme.hibernate.entities.assignment;

import static learnme.hibernate.Hibernate.getSession;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import learnme.hibernate.entities.Assignment;
import learnme.hibernate.entities.Place;
import learnme.hibernate.entities.User;

@Entity
@Table(name = "assignment_event")
public class Event extends Assignment {

	@ManyToOne
    private Place place;

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }
    
    public static Criteria getEvents(Date start, Date end) {
    	return getEvents(start,  null);
    }
    
    public static Criteria getEvents(Date start, Date end, User user) {
    	
		Criteria eventsCriteria = getSession().createCriteria(Event.class);
		
		eventsCriteria.add(
				Restrictions.or(
						Restrictions.and(Restrictions.gt("since", start), Restrictions.le("since", end)),
						Restrictions.and(Restrictions.le("until", end), Restrictions.gt("until", start))
						)
				);
		eventsCriteria.addOrder(Order.asc("since"));
		
		if (user != null) {
			eventsCriteria.createAlias("results", "resultsA");
			eventsCriteria.add(Restrictions.or(
					Restrictions.eq("resultsA.user", user),
					Restrictions.eq("sender", user)
					)
				);
		}

		return eventsCriteria;
    }
}
