package learnme.hibernate.entities;

import static learnme.hibernate.Hibernate.getSession;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;

import learnme.hibernate.BasicEntity;
import learnme.hibernate.dto.DTOFieldsCount;
import learnme.hibernate.interfaces.ICoursable;

@Entity
@Table(name = "field")
public class Field extends BasicEntity {

	
	public Field() {
		super();
	}
	
	public Field(String name) {
		super();
		this.name = name;
	}

	@Id
	private String name;

	@OneToMany(mappedBy = "field", cascade = CascadeType.PERSIST)
    private Set<Tag> tags = new HashSet<Tag>();
	
	public void addTag(Tag tag) {
		if(!tags.contains(tag)) {
			tags.add(tag);
		}
	}
	
    public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Tag> getTags() {
		return tags;
	}

    @Override
    public int getId() {
    	return name.hashCode();
    }

    public static Criteria getPopularFields() {
    	return getPopularFields(null);
    }
    
	 public static Criteria getPopularFields(User user, Class<? extends ICoursable>... clazzez) {
		 	
		 	Criteria fieldsCriteria;
		 
		 
		 	if (user != null) {
		    	fieldsCriteria = getSession().createCriteria(Course.class);
		    	
		    	HashSet<Integer> courses = new HashSet<Integer>();
		    	
		    	for (Class<? extends ICoursable> clazz : clazzez){
			    	for(ICoursable result : user.getCoursesAs(clazz)) {
			    		Course course = result.getCourse();
			    		if (!courses.contains(course.getId()) && result.isActive()) {
			    			courses.add(course.getId());
			    		}
			    	}
		    	}
		    	
		    	if (!courses.isEmpty()){
		    		fieldsCriteria.add(Restrictions.in("id", courses));
		    	} else {
		    		fieldsCriteria.add(Restrictions.eq("name", "AbsoluteNonSenseWhichAvoidListOfAllTags"));
		    	}
	    	}else {
	    		fieldsCriteria = getSession().createCriteria(Field.class);
	    	}
	    	
	    	fieldsCriteria.createAlias("tags", "tagsA");
	    	fieldsCriteria.setProjection(
	    			Projections.projectionList()
	    			.add(Projections.property("tagsA.field"), "field") 
	    			.add(Projections.groupProperty("tagsA.field"))
	    			.add(Projections.count("tagsA.field").as("count")));
	    	fieldsCriteria.addOrder(Order.desc("count"));
	    	fieldsCriteria.setResultTransformer(Transformers.aliasToBean(DTOFieldsCount.class));
	    	
	    	return fieldsCriteria;
	}
    
    

}
