package learnme.hibernate.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import learnme.hibernate.BasicEntity;

@Entity
@Table(name = "tag")
public class Tag extends BasicEntity {

	public Tag() {
		super();
	}
	 

	public Tag(String name, Field field) {
		super();
		
		this.name = name;
		this.field = field;
	}

    @Id
    private String name;
	
    @Id
	@ManyToOne
    private Field field;
	
	@OneToMany(mappedBy = "tags", cascade = CascadeType.PERSIST)
    private Set<Course> course = new HashSet<Course>();

    public String getName(){
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }    

    public Field getField() {
		return field;
	}

	public void setField(Field field) {
		this.field = field;
	}

	public int getId() {
        return name.hashCode();
    }
	
	@Override
    public boolean equals(Object obj) {
    	return ((Tag)obj).name.equals(name) && ((Tag)obj).getField().getName().equals(getField().getName());
    }
    
    @Override
    public int hashCode() {
    	return name.concat(getField().getName()).hashCode();
    }
	
}
