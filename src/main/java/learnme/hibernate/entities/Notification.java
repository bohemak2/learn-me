package learnme.hibernate.entities;

import static learnme.hibernate.Hibernate.getSession;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import learnme.hibernate.BasicEntity;
import learnme.hibernate.enums.ENotificationTarget;
import learnme.hibernate.enums.ENotificationType;
import learnme.hibernate.interfaces.ICreated;


@Entity
@Table(name = "notification")
public class Notification extends BasicEntity implements ICreated {
    
	public final static int MAX_DESCRIPTION_LENGTH = 80;
	
	public Notification() {
		super();
	}
	
	public Notification(Message message) {
		super();
		
		this.message = message;
	}
	
	public Notification(String description) {
		super();
		
		if (description.length() > 80) {
			setMessage(new Message(description));
			description = description.substring(0, MAX_DESCRIPTION_LENGTH - 3) + "...";
		}
		
		this.description =  description;
	}
	
	public Notification(ENotificationTarget target) {
		super();
		this.target = target;
	}

	public Notification(ENotificationType type, ENotificationTarget target) {
		super();
		
		this.type = type;
		this.target = target;
	}

	@Id
    @GeneratedValue
    private int id;
    
    private ENotificationType type;
    
    private ENotificationTarget target;
	
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date seen;

    @ManyToOne(cascade = CascadeType.PERSIST)
    private User user;
    
    @ManyToOne(cascade = CascadeType.PERSIST)
    private User sender;
        
    @ManyToOne(cascade = CascadeType.PERSIST)
    private Message message;
    
    String description;
    
    @PrePersist
    public void onCreate() {
        created = new Date();
        
        if (message != null && description == null) {
        	String text = message.getText();
        	
        	if (text != null) {
        		description = text.length() < 80 ? text : text.substring(0, MAX_DESCRIPTION_LENGTH - 3) + "...";    		
        	}
        }
    }

    public ENotificationType getType() {
        return type;
    }

    public void setType(ENotificationType type) {
        this.type = type;
    }

    public ENotificationTarget getTarget() {
		return target;
	}

	public void setTarget(ENotificationTarget target) {
		this.target = target;
	}
    
	public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getSeen() {
        return seen;
    }

    public void setSeen(Date seen) {
        this.seen = seen;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getSender() {
		return sender;
	}

	public void setSender(User sender) {
		this.sender = sender;
	}

	public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
        if (description == null) {        	
        	description = message.getText().length() < 80 ? message.getText() : message.getText().substring(0, MAX_DESCRIPTION_LENGTH - 3) + "...";
        }
    }
        
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

	public int getId() {
    	return id;
    }
	
	public static Criteria getNotifications(User user) {
		return getNotifications(user, null, false, false);
	}
	
	public static Criteria getNotifications(User user, User sender, boolean seen, boolean notseen) {
		
		Criteria conversationCriteria = getSession().createCriteria(Notification.class);

		if (user != null) {
			conversationCriteria.add(Restrictions.eq("user", user));			
		}
		
		if (sender != null) {
			conversationCriteria.add(Restrictions.eq("user", sender));			
		}
		
		if (seen) {
			conversationCriteria.add(Restrictions.isNotNull("seen"));
		}
		
		if (notseen) {
			conversationCriteria.add(Restrictions.isNull("seen"));
		}
		
		conversationCriteria.addOrder(Order.asc("created"));

		return conversationCriteria;
		
	}
}
