package learnme.hibernate.entities;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import learnme.hibernate.BasicEntity;


@Entity
@Table(name = "conversation_message")
public class Message extends BasicEntity  {
	
    public Message() {
		super();
	}

	public Message(String text) {
		super();
		this.text = text;
	}

	@Id
    @GeneratedValue
    private int id;

    @ManyToOne(cascade = CascadeType.PERSIST)
    private User user;

    @ManyToOne(cascade = CascadeType.PERSIST)
    private Conversation conversation;
    
    @OneToMany(mappedBy = "message", cascade = CascadeType.ALL)
    private Set<Notification> notifications = new HashSet<Notification>();

    @Temporal(TemporalType.TIMESTAMP)
    private Date sent;

    private String text;

    private File file;

    @PrePersist
    public void onCreate() {
        sent = new Date();
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Conversation getConversation() {
        return conversation;
    }

    public void setConversation(Conversation conversation) {
        this.conversation = conversation;
    }

    public Date getSent() {
        return sent;
    }

    public void setSent(Date sent) {
        this.sent = sent;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public Set<Notification> getNotifications() {
        return notifications;
    }
    
    public void addNotification(Notification notification) {
    	notification.setMessage(this);
    	getNotifications().add(notification);
    }
    
    public int getId() {
        return id;
    }
}
