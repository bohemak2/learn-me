package learnme.hibernate.entities;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.ColumnDefault;

import learnme.hibernate.BasicEntity;
import learnme.hibernate.entities.user.Tutor;
import learnme.hibernate.interfaces.IActivated;
import learnme.hibernate.interfaces.IPositioned;

@Entity
@Table(name = "chapter")
public class Chapter extends BasicEntity implements IActivated, IPositioned {
	
	public Chapter() {
		super();
	}
	
	public Chapter(String name, String description) {
		super();
		this.name = name;
		this.description = description;
	}

	public Chapter(String name, String description, int minutes) {
		super();
		this.name = name;
		this.description = description;
		this.minutes = minutes;
	}

	@Id
	@GeneratedValue
	private int id;
	
	@ManyToOne
	private Tutor author;
	
	@ManyToOne
	private Course course; 
	
    @ColumnDefault("1")
    private boolean active = true;
	
	private String name;
	
	private int position;
	
	@Column(length = 300)
	private String description;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date since;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date until;
	
	private int minutes;

	@OneToMany(mappedBy = "chapter", cascade = CascadeType.PERSIST)
	private Set<Content> contents = new HashSet<Content>();
	
	public Tutor getAuthor() {
		return author;
	}

	public void setAuthor(Tutor author) {
		this.author = author;
	}
	
	public void setAuthor(User author) {
		for (Tutor tutor : author.getCoursesAsTutor()) {
			if (tutor.getCourse().equals(course)) {
				setAuthor(tutor);
				break;
			}
		}
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}
	
	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getSince() {
		return since;
	}

	public void setSince(Date since) {
		this.since = since;
	}

	public Date getUntil() {
		return until;
	}

	public void setUntil(Date until) {
		this.until = until;
	}

	public int getMinutes() {
		return minutes;
	}

	public void setMinutes(int minutes) {
		this.minutes = minutes;
	}
	
	public void addContent(Content content) {
		content.setChapter(this);
		getContents().add(content);
	}

	public Set<Content> getContents() {
		return contents;
	}

	@Override
	public int getId() {
		return id;
	}
	
}