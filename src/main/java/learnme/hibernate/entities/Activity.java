package learnme.hibernate.entities;

import static learnme.hibernate.Hibernate.getSession;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.Criteria;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;

import learnme.hibernate.BasicEntity;
import learnme.hibernate.dto.DTOActivitiesCount;
import learnme.hibernate.enums.EActivityType;
import learnme.hibernate.interfaces.ICreated;

@Entity
@Table(name = "activity")
public class Activity extends BasicEntity implements ICreated {
	
    public Activity() {
		super();
	}
    
    public Activity(EActivityType type) {
		this(type, 1);
	}
    
	public Activity(EActivityType type, int significancy) {
		this(null, null, null, type, significancy);
	}

	public Activity(User user, User participant, Course course, EActivityType type, int significancy) {
		super();

		this.user = user;
		this.participant = participant;
		this.course = course;
		this.type = type;
		this.significancy = significancy;
	}

	@Id
    @GeneratedValue
    private int id;
    
    @ManyToOne
    private User user;
    
    @ManyToOne
    private User participant;
    
    @ManyToOne
    private Course course;
    
    private EActivityType type;

    @ColumnDefault("1")
    private int significancy;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @PrePersist
    public void onCreate() {
        created = new Date();
        significancy = 1;
    }
    
    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getParticipant() {
		return participant;
	}

	public void setParticipant(User participant) {
		this.participant = participant;
	}

	public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }
    
    public EActivityType getType() {
		return type;
	}

	public void setType(EActivityType type) {
		this.type = type;
	}

	public int getSignificancy() {
		return significancy;
	}

	public void setSignificancy(int significancy) {
		this.significancy = significancy;
	}

	public int getId() {
        return id;
    }
	
	public static Criteria getActivities(User user, Date since, Date until) {
		return getActivities(user, null, null, null, since, until, false);
	}
	
	public static Criteria getActivities(User user, EActivityType type, Date since, Date until) {
		return getActivities(user, null, null, type, since, until, false);
	}
	
    public static Criteria getActivities(User user, Course course, User participant, EActivityType type, Date since, Date until, boolean count) {
    	
		Criteria assignmentsCriteria = getSession().createCriteria(Activity.class);

		if (user != null) {
			assignmentsCriteria.add(Restrictions.eq("user", user));			
		}
		
		if (course != null) {
			assignmentsCriteria.add(Restrictions.eq("course", course));			
		}
		
		if (participant != null) {
			assignmentsCriteria.add(Restrictions.eq("participant", participant));			
		}
		
		if (type != null) {
			assignmentsCriteria.add(Restrictions.eq("type", type));			
		}
		
		if (since != null) {
			assignmentsCriteria.add(Restrictions.ge("created", since));			
		}
		
		if (until != null) {
			assignmentsCriteria.add(Restrictions.ge("created", until));			
		}
		
		if (count) {
			assignmentsCriteria.setProjection(
	    			Projections.projectionList()
	    			.add(Projections.property("type").as("type")) 
	    			.add(Projections.groupProperty("type"))
	    			.add(Projections.count("id").as("count")));
			assignmentsCriteria.setResultTransformer(Transformers.aliasToBean(DTOActivitiesCount.class));
		}
		
		return assignmentsCriteria;
    }
}
