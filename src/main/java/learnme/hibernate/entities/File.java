package learnme.hibernate.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import learnme.hibernate.BasicEntity;

@Entity
@Table(name = "file")
public class File extends BasicEntity {

    @Id
    @GeneratedValue
    private int id;
    
    private String name;
    
    private String url;
    
    // TODO: ENUM of types
    //private Type preview;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getId() {
        return id;
    }
}
