package learnme.hibernate.entities;

import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import learnme.hibernate.BasicEntity;

@Entity
@Table(name = "result")
public class Result extends BasicEntity {

        @Id
        @GeneratedValue
        private int id;

        @ManyToOne(cascade = CascadeType.PERSIST)
        private User user;

        @ManyToOne(cascade = CascadeType.PERSIST)
        private User signed;

        @Temporal(TemporalType.TIMESTAMP)
        private Date seen;

        @Temporal(TemporalType.TIMESTAMP)
        private Date marked;

        private int mark;

        private String description;

        public User getUser() {
            return user;
        }

        public void setUser(User user) {
            this.user = user;
        }

        public User getSigned() {
            return signed;
        }

        public void setSigned(User signed) {
            this.signed = signed;
        }

        public boolean isSeen() {
            return seen != null;
        }

        public void setSeen(Date seen) {
            this.seen = seen;
        }

        public Date getMarked() {
            return marked;
        }

        public void setMarked(Date marked) {
            this.marked = marked;
        }

        public int getMark() {
            return mark;
        }

        public void setMark(int mark) {
            this.mark = mark;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    
        public int getId() {
            return id;
        }
    }
