package learnme.hibernate.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import learnme.hibernate.BasicEntity;
import learnme.hibernate.entities.user.Student;
import learnme.hibernate.entities.user.Tutor;

@Entity
@Table(name = "significancy")
public class Significancy extends BasicEntity{

    @Id
    @GeneratedValue
    private int id;
    
    @OneToOne
    private User user;

    /*  Marks Follows - Higher is more important */
    private double totalMark = 0;
    
    private double courseMark = 0;
    
    private double userMark = 0;
    /* END of Marks */
    
    public int getId() {
    	return id;
    }
    
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public double getCourseMark() {
        return courseMark;
    }

    public double getUserMark() {
        return userMark;
    }

    public double getTotalMark() {
        return totalMark;
    }
    
    public void setTotalMark(double totalMark) {
        this.totalMark = totalMark;
    }

    public void setCourseMark(double courseMark) {
        this.courseMark = courseMark;
    }

    protected void setUserMark(double userMark) {
        this.userMark = userMark;
    }
    
    
    /* INFO: Bellow methods will be called how often will be implemented - for default, it just on start (but it could be hooked inside code) */
    public static void refreshCourseMark(User... users){
        for (User user : users){
            int count = 0;
            double mark = 0;
            for (Tutor course : user.getCoursesAsTutor()){
            	double courseSignificancy = .5;
                for(Student result : course.getCourse().getStudentsResults()){
                    if (result.getReview() != null){
                    	courseSignificancy += 0.1 * ((result.getReview().getMark() * result.getUser().getSignificancy().getTotalMark()) / ++count);
                    }
                }
                mark += courseSignificancy;
            }
            user.getSignificancy().setCourseMark(mark);
        }
    }
    
    public static void refreshUserMark(User... users){
        for (User user : users){
            double mark = 0;
            double userSignificancy = 0.1;
        	for (User follower : user.getFollowers()) {
        		userSignificancy += 0.25 * follower.getSignificancy().getTotalMark();
        	}
        	mark += userSignificancy;
        	user.getSignificancy().setUserMark(mark);
        }
    }
    
    public static void refreshTotalMark(User... users){
        for (User user : users){
            refreshCourseMark(user);
            refreshUserMark(user);
            
            Significancy significancy = user.getSignificancy();
            significancy.setTotalMark(significancy.getCourseMark() + significancy.getUserMark() / 2.0);
        }
    }
    
}
