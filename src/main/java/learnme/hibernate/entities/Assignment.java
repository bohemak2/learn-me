package learnme.hibernate.entities;

import static learnme.hibernate.Hibernate.getSession;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import learnme.hibernate.BasicEntity;
import learnme.hibernate.entities.assignment.Event;
import learnme.hibernate.entities.user.Student;
import learnme.hibernate.enums.ENotificationTarget;
import learnme.hibernate.enums.ENotificationType;

/* ATTENTION! NO ANONYMOUS CHILDREN! CAUSES Hibernate Exception -> No class found! */

@Entity
@Table(name = "assignment")
@Inheritance(strategy = InheritanceType.JOINED)
public class Assignment extends BasicEntity {

	public static final String MESSAGE_ASSIGNMENT_NEW = "New assignment%s.";
	public static final String MESSAGE_ASSIGNMENT_REMOVED = "Removed assignment%s.";
	
	public Assignment() {
		super();
	}
		
    public Assignment(User sender) {
		super();
		this.sender = sender;
	}

	@Id
    @GeneratedValue
    private int id;
    
    @ManyToOne
    private User sender;
    
    @ManyToOne
    private Course course;

    @OneToMany(cascade = CascadeType.PERSIST)
    private Set<Result> results = new HashSet<Result>();
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date since;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date until;
    
    private String name;
    
    private String description;
    
    private int attempts;
    
    private int minimum;
    
    @OneToOne(cascade = CascadeType.PERSIST)
    private Conversation conversation;
    
    public User getSender() {
    	if (sender == null && course != null) {
    		return course.getOwner();
    	}
		return sender;
	}

	public void setSender(User sender) {
		this.sender = sender;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public void addUser(User user) {
    	addUser(user, true);
    }
    
    public void addUser(User user, boolean notificate) {
    	
    	if (!results.contains(user)) {
	    	results.add(new Result() {
	    		{
	    			setUser(user);
	    		} 
	    	});	    	
	    	
	    	if (notificate && ((sender != null && !user.equals(sender)) || course != null)) {
	    		user.addNotification(makeNotification());
	    	}
    	}	
    }
    
	public void removeUser(User user) {
    	removeUser(user, true);
    }
	
	public void removeUser(User user, boolean notificate) {
		for (Result result : results) {
			if (result.getUser().equals(user)) {
				results.remove(result);
				persist();
				break;
			}
		}
	}
    
    public void notificateStudents() {
    	if (course != null) {
    		Notification notification = makeNotification();
    		
    		for (Student student : course.getStudentsResults()) {
    			notificateStudent(student, notification);
    		}
    	}
    }
    
    public void notificateStudent(Student student, Notification notification) {
    	if (course != null) {
    		if (notification == null) {
    			notification = makeNotification();
    		}
    		
			student.getUser().addNotification(notification);
    	}
    }
    
    private Notification makeNotification(){
    	boolean isCourse = course != null;
    	return new Notification(ENotificationType.INSERT, isCourse ? ENotificationTarget.ASSIGNMENT_COURSE : ENotificationTarget.ASSIGNMENT) {
			{
				if (isCourse && sender == null) {
					setSender(course.getOwner());	    					
				} else {
					setSender(sender);
				}
				
				if (description == null) {
					if (name != null) {
						setDescription(name);
					} else {
						setDescription(String.format(MESSAGE_ASSIGNMENT_NEW, course != null ? " " + course.getName() : "" ));	    					
					}
				}
				
//				TODO: Better :)
				setMessage(new Message(
						(course != null ? course.getName() + ",\n " : "")
						.concat(description != null ?  description + ",\n " : "")
						.concat(since.toGMTString() + (until != null && until != since ? " - " + until.toGMTString() + ",\n ": "") + "" )
						.concat(Assignment.this instanceof Event ? ((Event)Assignment.this).getPlace() != null ? ",\n " + ((Event)Assignment.this).getPlace().getAddress() : "" : "")
						.concat(".")
						)
					);
			}
		};
    }
    
    public Set<Result> getResults() {
        return results;
    }

    public Date getSince() {
        return since;
    }

    public void setSince(Date since) {
        this.since = since;
    }

    public Date getUntil() {
        return until;
    }

    public void setUntil(Date until) {
        this.until = until;
    }

    public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getAttempts() {
        return attempts;
    }

    public void setAttempts(int repeat) {
        this.attempts = repeat;
    }

    public int getMinimum() {
        return minimum;
    }

    public void setMinimum(int minimum) {
        this.minimum = minimum;
    }

    public Conversation getConversation() {
        return conversation;
    }

    public void setConversation(Conversation conversation) {
        this.conversation = conversation;
    }
    
    public int getId() {
        return id;
    }
    
    public static Criteria getAssignments(Course course) {
    	return getAssignments(null, course);
    }
    
    public static Criteria getAssignments(User user) {
    	return getAssignments(user, null);
    }
    
    public static Criteria getAssignments(User user, Course course) {
    	
		Criteria assignmentsCriteria = getSession().createCriteria(Assignment.class);
		
		if (course != null) {
			assignmentsCriteria.add(Restrictions.eq("course", course));
		}
		
		if (user != null) {
			assignmentsCriteria.createAlias("results", "resultsA");
			assignmentsCriteria.add(Restrictions.or(
					Restrictions.eq("resultsA.user", user),
					Restrictions.eq("sender", user)
					)
				);
		}
		
		return assignmentsCriteria;
    }
}
   