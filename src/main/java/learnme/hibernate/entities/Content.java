package learnme.hibernate.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;

import learnme.hibernate.BasicEntity;
import learnme.hibernate.interfaces.IActivated;
import learnme.hibernate.interfaces.IPositioned;

/* ATTENTION! NO ANONYMOUS CHILDREN! CAUSES Hibernate Exception -> No class found! */  

@Entity
@Table(name = "content")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Content extends BasicEntity implements IActivated, IPositioned {

	@Id
	@GeneratedValue
	private int id;
	
	@ManyToOne
	private Chapter chapter;
	
	@ColumnDefault("1")
	private boolean active = true;
	
	private String name;
	
	private int position;
	
	abstract public void setContent(Object content);
	abstract public Object getContent();
	
	@Override
	public int getId() {
		return id;
	}
	
	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Chapter getChapter() {
		return chapter;
	}

	public void setChapter(Chapter chapter) {
		this.chapter = chapter;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public int getPosition() {
		return position;
	}
	
	@Override
	public void setPosition(int position) {
		this.position = position;
	}
}
