package learnme.hibernate.entities;

import com.neovisionaries.i18n.CountryCode;
import java.io.Serializable;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import learnme.hibernate.BasicEntity;

@Entity
@Table(name = "place")
public class Place extends BasicEntity {

    @Id
    @GeneratedValue
    private int id;

    private String city;

    private int zip;

    private Axis axis;

    private CountryCode country;

    private String street;

    @Embeddable
    public static class Axis implements Serializable {
    	
        public Axis() {
			super();
		}

		public Axis(float north, float east) {
			super();
			this.north = north;
			this.east = east;
		}

		private float east;

        private float north;

        public float getEast() {
            return east;
        }

        public void setEast(float east) {
            this.east = east;
        }

        public float getNorth() {
            return north;
        }

        public void setNorth(float west) {
            this.north = west;
        }
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getZip() {
        return zip;
    }

    public void setZip(int zip) {
        this.zip = zip;
    }

    public Axis getAxis() {
        return axis;
    }

    public void setAxis(Axis axis) {
        this.axis = axis;
    }

    public CountryCode getCountry() {
        return country;
    }

    public void setCountry(CountryCode country) {
        this.country = country;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getAddress() {
    	String result = "";
    	
    	if (street != null) {
    		result += street + ", ";
    	}
    	
    	if (city != null) {
    		result += city + ", ";
    	}
    	
    	if (country != null) {
    		result += country + ", ";
    	}
    	
    	if (zip != 0) {
    		result += zip + ", ";
    	}
    	
    	
    	int length = result.length();
    	
    	if (length > 1) {
    		return  result.substring(0, length - 2);
    	} else {
    		return null;
    	}
    }
    
    public int getId() {
        return id;
    }
}
