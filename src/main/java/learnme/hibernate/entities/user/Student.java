package learnme.hibernate.entities.user;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.ColumnDefault;

import learnme.hibernate.BasicEntity;
import learnme.hibernate.entities.Assignment;
import learnme.hibernate.entities.Course;
import learnme.hibernate.entities.Notification;
import learnme.hibernate.entities.Review;
import learnme.hibernate.entities.User;
import learnme.hibernate.enums.ENotificationTarget;
import learnme.hibernate.enums.ENotificationType;
import learnme.hibernate.interfaces.ICoursable;

@Entity
@Table(name = "course_student")
public class Student extends BasicEntity implements ICoursable{

	public Student() {
		super();
	}
	
    public Student(User user) {
		super();
		this.user = user;
	}

	@Id
    @GeneratedValue
    private int id;

    @ManyToOne
    private User user;

    @ManyToOne
    private Course course;

    @Temporal(TemporalType.TIMESTAMP)
    private Date registered;

    @ColumnDefault("1")
    boolean active;

    @Temporal(TemporalType.TIMESTAMP)
    private Date achieved;

    @ManyToOne
    private Tutor ratedBy;

    private int value;
    
    @OneToOne
    private Review review;
    
    @PrePersist
    public void onCreate() {
    	active = true;
        registered = new Date();
    }
    
    public Date getRegistered() {
        return registered;
    }

    public void setRegistered(Date registered) {
        this.registered = registered;
    }

    public Date getAchieved() {
        return achieved;
    }

    public void setAchieved(Date achieved) {
        this.achieved = achieved;
    }

    public Tutor getRatedBy() {
        return ratedBy;
    }

    public void setRatedBy(Tutor ratedBy) {
        this.ratedBy = ratedBy;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Review getReview() {
        return review;
    }

    public void setReview(Review review) {
        this.review = review;
    }
    
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

	@Override
	public void setActive(boolean active) {
		setActive(active, false);
	}

	public void setActive(boolean active, boolean notificate) {
		this.active = active;
		
		if (notificate) {
			for (Tutor tutor : course.getTutors()) {
				Notification notification = new Notification(active ? ENotificationType.INSERT : ENotificationType.DELETE, ENotificationTarget.COURSE_STUDENT);
				
				notification.setUser(tutor.getUser());
				notification.setSender(user);
				
				if (active) {
					notification.setDescription(String.format(Course.MESSAGE_COURSE_ADDED, course.getName()));
				} else {
					notification.setDescription(String.format(Course.MESSAGE_STUDENT_REMOVED, course.getName()));
				}
				
				try {
					notification.persist();
				} catch (Exception e) {
					tutor.getUser().addNotification(notification);
				}
			}
			
			for (Assignment assignment : course.getAssignments()) {
				if (active) {
					assignment.addUser(user);
				} else {
					assignment.removeUser(user);
				}
			}
		}
	}
	
	@Override
	public boolean isActive() {
		return active;
	}
}
