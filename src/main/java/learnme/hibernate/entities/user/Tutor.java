package learnme.hibernate.entities.user;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.ColumnDefault;

import learnme.hibernate.BasicEntity;
import learnme.hibernate.entities.Course;
import learnme.hibernate.entities.User;
import learnme.hibernate.interfaces.IActivated;
import learnme.hibernate.interfaces.ICoursable;

@Entity
@Table(name = "course_tutor")
public class Tutor extends BasicEntity implements ICoursable {

	public Tutor() {
		super();
	}
	
	public Tutor(User user) {
		this.user = user;
	}
	
    @Id
    @GeneratedValue
    private int id;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "course_id")
    private Course course;

    @Temporal(TemporalType.TIMESTAMP)
    private Date registered;

    @ColumnDefault("1")
    boolean active;

    @PrePersist
    public void onCreate() {
    	active = true;
        registered = new Date();
    }
    
    public int getId() {
    	return id;
    }
    
    public User getUser() {
        return user;
    }
    
    public void setUser(User user) {
        this.user = user;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Date getRegistered() {
        return registered;
    }

    public void setRegistered(Date registered) {
        this.registered = registered;
    }

	@Override
	public void setActive(boolean active) {
		this.active = active;
	}

	@Override
	public boolean isActive() {
		return active;
	}
}

