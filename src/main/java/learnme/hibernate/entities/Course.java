package learnme.hibernate.entities;

import static learnme.hibernate.Hibernate.getSession;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.Criteria;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import learnme.hibernate.BasicEntity;
import learnme.hibernate.dto.DTOFieldsCount;
import learnme.hibernate.entities.user.Student;
import learnme.hibernate.entities.user.Tutor;
import learnme.hibernate.enums.EActivityType;
import learnme.hibernate.enums.ECurrency;
import learnme.hibernate.enums.ENotificationTarget;
import learnme.hibernate.enums.ENotificationType;
import learnme.hibernate.interfaces.IActivated;
import learnme.hibernate.interfaces.ICoursable;
import learnme.hibernate.interfaces.ICreated;
import learnme.hibernate.interfaces.IPostable;
import learnme.hibernate.utils.BasicLoader;

@Entity
@Table(name = "course")
public class Course extends BasicEntity implements IActivated, ICreated, IPostable {

	private static final String DEFAULT_COVER = "img/course_cover.jpg";
	private static final ECurrency DEFAULT_CURRENCY = ECurrency.EUR;
	
	public static final String MESSAGE_COURSE_ADDED = "$s followed by you released %s course. Wouldn't you mind to see it?";
	public static final String MESSAGE_STUDENT_ADDED = "I have just enrolled %s course!";
	public static final String MESSAGE_STUDENT_REMOVED = "Sorry, I have just disenrolled %s course.";

	public Course() {
		super();
	}
	
	public Course(User owner) {
		super();
		
		this.owner = owner;
		makeNotifications();
	}
	
    public Course(User owner, String name, String description) {
		super();
		
		this.owner = owner;
		this.name = name;
		this.description = description;
		
		makeNotifications();
	}

    
    private void makeNotifications() {
        if (owner != null) {
			for (User follower : owner.getFollowers()) {
				follower.addNotification(new Notification() {
					{
						setDescription(String.format(MESSAGE_COURSE_ADDED, owner.getNameAndSurname(), name));
						setSender(owner);
					}
				});
			}
		}
    }
    
	@Id
    @GeneratedValue
    private int id;

    @ManyToOne
    private User owner;

    @ColumnDefault("0")
    private boolean active;
    
    private String name;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @Column(length = 300)
    private String description;

    @Temporal(TemporalType.TIMESTAMP)
    private Date since;

    @Temporal(TemporalType.TIMESTAMP)
    private Date until;
    
    private boolean presence;

    @ColumnDefault("0")
    private boolean autoacceptance;

    @ColumnDefault("10")
    private int capacity;

    @OneToMany(mappedBy = "course", cascade = CascadeType.PERSIST)
    private Set<Tutor> tutors = new HashSet<Tutor>();

    @OneToMany(mappedBy = "course", cascade = CascadeType.PERSIST)
    private Set<Student> students = new HashSet<Student>();
    
    @ManyToMany(cascade = CascadeType.PERSIST)
    private Set<Tag> tags = new HashSet<Tag>();

    @OneToMany(mappedBy = "course", cascade = CascadeType.PERSIST)
    private Set<Activity> activities = new HashSet<Activity>();
    
    @OneToMany(mappedBy = "course", cascade = CascadeType.PERSIST)
    private Set<Assignment> assignments = new HashSet<Assignment>();
    
    @OneToMany(mappedBy = "course", cascade = CascadeType.PERSIST)
    private Set<Conversation> posts = new HashSet<Conversation>();
    
    @OneToMany(mappedBy = "course", cascade = CascadeType.PERSIST)
    private Set<Chapter> chapters = new HashSet<Chapter>();
    
    @OneToMany(mappedBy = "course", cascade = CascadeType.PERSIST)
    private Set<Review> reviews = new HashSet<Review>();
    
    private int fee;
    
    private String cover;

    @Enumerated
    private ECurrency currency;

    @PrePersist
    public void onCreate() {
        created = new Date();
    }
    
    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
    	if (this.owner != owner) {
    		this.owner = owner;
    		owner.addActivity(new Activity(EActivityType.COURSE_AS_OWNER_ADDED) {
    			{
    				setCourse(Course.this);
    			}
    		});
        }
    }
    
    public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
    
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getSince() {
        return since;
    }

    public void setSince(Date since) {
        this.since = since;
    }

    public Date getUntil() {
        return until;
    }

    public void setUntil(Date until) {
        this.until = until;
    }

    public boolean isAutoacceptance() {
        return autoacceptance;
    }

    public boolean isPresence() {
        return presence;
    }

    public void setPresence(boolean presence) {
        this.presence = presence;
    }

    public void setAutoacceptance(boolean autoacceptance) {
        this.autoacceptance = autoacceptance;
    }

    public int getMaxCapacity() {
        return capacity;
    }
    
    public int getCapacity() {
    	return getMaxCapacity() - getFreeCapacity();
    }
    
    public int getFreeCapacity() {
    	int realCapacity = capacity;
    	for(Student student : getStudentsResults()) {
    		if (student.isActive()) {
    			realCapacity--;
    		}
    	}
        return realCapacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getFee() {
        return fee;
    }

    public void setFee(int fee) {
        this.fee = fee;
    }

    public ECurrency getCurrency() {
    	if (currency == null) {
    		return DEFAULT_CURRENCY;
    	}
        return currency;
    }

    public void setCurrency(ECurrency currency) {
        this.currency = currency;
    }
    
	public String getCover() {
		if (cover != null) {
			return cover;
		} else {
			return DEFAULT_COVER;
		}
	}

	public void setCover(String cover) {
		this.cover = cover;
	}


    public int getId() {
        return id;
    }

    public Set<Tutor> getTutors() {
        return tutors;
    }
    
    public void addTutor(User user) {
    	if (!user.getCoursesAsTutor().contains(this)) {
    		Tutor tutor = new Tutor(user);
    		
    		tutor.setCourse(this);
    		
    		tutors.add(tutor);
    		user.addCourseAsTutor(tutor);
    		
    		user.addActivity(new Activity(EActivityType.COURSE_AS_TUTOR_ADDED) {
    			{
    				setCourse(Course.this);
    			}
    		});
    	}
    }
    
    public void addTutor(Tutor tutor) {
    	tutor.setCourse(this);
    	getTutors().add(tutor);
    }

    public Set<Student> getStudentsResults() {
        return students;
    } 

    public void addStudent(User user) {
    	addStudent(user, true);
    }
    
    public void addStudent(User user, boolean notificate) {
    	if (!user.getCoursesAsStudent().contains(this)) {
    		Student student = new Student(user);
    		student.setCourse(this);
    		user.addCourseAsStudent(student);

    		if (notificate) {
//    			TODO: Owner or Tutors?
    			for (Tutor tutor : getTutors()) {
    				Notification notification = new Notification(ENotificationType.INSERT, ENotificationTarget.COURSE_STUDENT);
    				
    				notification.setUser(tutor.getUser());
    				notification.setSender(user);
    				notification.setDescription(String.format(MESSAGE_STUDENT_ADDED, getName()));
    				try {
    					notification.persist();
					} catch (Exception e) {
						tutor.getUser().addNotification(notification);
					}
    			}
    		}
    	}
    }
    
    public void addStudent(Student result) {;
    	result.setCourse(this);
    	getStudentsResults().add(result);
    }
    
    public boolean isParticipant(User user) {
    	for (Tutor tutor : getTutors()) {
    		if (tutor.getUser().equals(user) && tutor.isActive()) {
    			return true;
    		}
    	}
    	
    	for (Student student : getStudentsResults()) {
    		if (student.getUser().equals(user) && student.isActive()) {
    			return true;
    		}
    	}
    	
    	return false;
    }
    
    public Set<Activity> getActivities() {
        return activities;
    }
    
    public void addActivity(Activity activity) {
    	activity.setCourse(this);
    	getActivities().add(activity);
    }
    
    public void addTag(Tag tag) {
    	if (!getTags().contains(tag)) {
    		getTags().add(tag);
    	}
    }
    
    public Set<Tag> getTags() {
        return tags;
    }

    
    public void addAssignment(Assignment assignment) {
    	assignment.setCourse(this);
    	assignment.notificateStudents();
    	getAssignments().add(assignment);
    }
    
    public Set<Assignment> getAssignments() {
		return assignments;
	}

	public void addPost(Conversation conversation) {
    	conversation.setCourse(this);
    	getPosts().add(conversation);
    }
    
    public Set<Conversation> getPosts() {
        return posts;
    }
    
    public void addChapter(Chapter chapter) {
    	chapter.setCourse(this);
    	getChapters().add(chapter);
    }
    
    public Set<Chapter> getChapters() {
		return chapters;
	}
    
    public void addReview(Review review) {
    	review.setCourse(this);
    	getReviews().add(review);
    }
    
    public Set<Review> getReviews() {
		return reviews;
	}
    
    public static Criteria getInterestingCourses(User user) {
    	return getInterestingCourses(user, true);
    }
    
    public static Criteria getInterestingCourses(User user, boolean removemy) {
    	Criteria coursesCriteria = getSession().createCriteria(Course.class);
     	
		coursesCriteria.add(Restrictions.eq("active", true));
//		TODO: real capacity (proc doporucovat plne, ze...) - coursesCriteria.add(Restrictions.eq("capacity"));
		
     	if (user != null) {
     		// REMOVE MY COURSES
     		if (removemy) {
     			for (Student student : user.getCoursesAsStudent()) {
     				coursesCriteria.add(Restrictions.not(Restrictions.eq("id", student.getCourse().getId())));
     			}
     			for (Tutor tutor : user.getCoursesAsTutor()) {
     				coursesCriteria.add(Restrictions.not(Restrictions.eq("id", tutor.getCourse().getId())));
     			}
     		}

 			// ADD COURSES BY POPULAR TAGS
     		BasicLoader<DTOFieldsCount> loader = new BasicLoader<DTOFieldsCount>(Field.getPopularFields(user, Student.class, Tutor.class));
     		List<DTOFieldsCount> fields = new ArrayList<DTOFieldsCount>(loader.loadNext());
     		
//     		TODO: FIELDS USER FOLLOWERS
         	if (fields != null && !fields.isEmpty()) {
         		coursesCriteria.createAlias("tags", "tagsA");
         		
         		List<Field> fieldsS = new ArrayList<Field>();
         		for (DTOFieldsCount field : fields) {
         			fieldsS.add(field.field);
         		}
         		
         		coursesCriteria.add(Restrictions.or(Restrictions.in("tagsA.field", fieldsS)));
         	}
     	}
     	
     	return coursesCriteria;
    }

    public static Criteria getCourses(User user, boolean active, boolean completed, boolean notcompleted) {
    	Criteria coursesCriteria = getSession().createCriteria(Course.class);
     	
    	if (active) {
    		coursesCriteria.add(Restrictions.eq("active", true));
    	} else {
    		coursesCriteria.add(Restrictions.eq("active", false));
    	}
    	
     	if (user != null) {
     		coursesCriteria.createAlias("students", "studentA");
     		coursesCriteria.add(Restrictions.eq("studentA.user", user));
     		if (active) {
     			coursesCriteria.add(Restrictions.eq("studentA.active", true));
     		} else {
     			coursesCriteria.add(Restrictions.eq("studentA.active", false));
     		}

     		if (completed) {
     			coursesCriteria.add(Restrictions.isNotNull("studentA.achieved"));
     		}
     		if (notcompleted) {
     			coursesCriteria.add(Restrictions.isNull("studentA.achieved"));
     		}
     	}
     	
     	return coursesCriteria;
    }
    
	public static Criteria getCoursesByField(Set<Field> fields, User user, Class<? extends ICoursable>... clazzez) {
     	Criteria coursesCriteria = getSession().createCriteria(Course.class);
     	coursesCriteria.add(Restrictions.eq("active", true));
     	
     	if (user != null) {
     		for (Class<? extends ICoursable> clazz : clazzez){
     			String clazzString = clazz.getSimpleName().toLowerCase() + "s";
     			
     			/* TODO: HERE is problem - dva aliasy bych potreboval zORovat - bud najit Hibernate reseni, nebo podobne reseni foreachem jako v Field */
	     		coursesCriteria.createAlias(clazzString, clazzString + "A");
	     		coursesCriteria.add(Restrictions.eq(clazzString + "A.user", user));
	     		coursesCriteria.add(Restrictions.eq(clazzString + "A.active", true));
     		}
     	}
     	
     	if (fields != null && !fields.isEmpty()) {
     		coursesCriteria.createAlias("tags", "tagsA");
     		coursesCriteria.add(Restrictions.in("tagsA.field", fields));
     	}
     	coursesCriteria.addOrder(Order.desc("created")).addOrder(Order.desc("id"));
		return coursesCriteria;
    }
    
	public Criteria getLastPosts() {

		Criteria postsCriteria = getSession().createCriteria(Conversation.class);
		postsCriteria.add(Restrictions.eq("course", this));
		postsCriteria.addOrder(Order.asc("created"));

		return postsCriteria;
	}
}
