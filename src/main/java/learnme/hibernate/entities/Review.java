package learnme.hibernate.entities;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import learnme.hibernate.BasicEntity;

@Entity
@Table(name = "review")
public class Review extends BasicEntity {
    
    @Id
    @GeneratedValue
    private int id;

    // Pro zrychleni klice User a Course i v Review, jinak by stacilo jit pres Course.Result
    @ManyToOne(cascade = CascadeType.PERSIST)
    private User user;
    
    @ManyToOne(cascade = CascadeType.PERSIST)
    private Course course;
    
    private int mark;
    
    private String description;

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }
    
    public int getId() {
        return id;
    }
}
