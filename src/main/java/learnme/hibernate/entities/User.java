package learnme.hibernate.entities;

import static learnme.hibernate.Hibernate.getSession;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.wicket.Component;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import learnme.application.AppConfig;
import learnme.hibernate.BasicEntity;
import learnme.hibernate.entities.user.Student;
import learnme.hibernate.entities.user.Tutor;
import learnme.hibernate.enums.EActivityType;
import learnme.hibernate.enums.ENotificationTarget;
import learnme.hibernate.interfaces.IActivated;
import learnme.hibernate.interfaces.ICoursable;
import learnme.hibernate.interfaces.IMessagable;
import learnme.hibernate.interfaces.IPostable;
import learnme.hibernate.test.TestingData;
import learnme.hibernate.utils.EntityLoader;

@Entity
@Table(name = "user")
public class User extends BasicEntity implements IActivated, IMessagable, IPostable {

	private static final String DEFAULT_IMAGE = "img/avatar.png";
	private static final String DEFAULT_COVER = "img/avatar_cover.jpg";

	private static final String MESSAGE_WELLCOME = "Wellcome in LearnMe! \n We hope that you will find this educational social network usefull and it brings you a lot of joy. \n With best, Creators.";
	private static final String MESSAGE_NEW_FOLLOWER = "Since now, I am your new follower!";

	public User() {
		super();
	}

	public User(String name, String surname, String email) {
		super();
		this.name = name;
		this.surname = surname;
		this.email = email;

		addNotification(new Notification(MESSAGE_WELLCOME) {
			{
				setTarget(ENotificationTarget.SYSTEM_WELLCOME);
				setSender(User.this);
			}
		});
	}

	@Id
	@GeneratedValue
	private int id;

	private boolean active;

	private String name;

	private String surname;

	private String email;

	private String password;

	private String image;

	private String cover;

	@Temporal(TemporalType.TIMESTAMP)
	private Date registered;

	@Temporal(TemporalType.TIMESTAMP)
	private Date logged;

	@Temporal(TemporalType.DATE)
	private Date birthday;

	@ManyToOne
	private Place place;

	@OneToMany(mappedBy = "user", cascade = CascadeType.PERSIST)
	private Set<Student> student = new HashSet<Student>();

	@OneToMany(mappedBy = "user", cascade = CascadeType.PERSIST)
	private Set<Tutor> tutor = new HashSet<Tutor>();

	@OneToMany(mappedBy = "user", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	private Set<Activity> activities = new HashSet<Activity>();

	@ManyToMany(mappedBy = "users", cascade = CascadeType.PERSIST)
	private Set<Conversation> posts = new HashSet<Conversation>();

	@OneToMany(mappedBy = "user", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	private Set<Notification> notifications = new HashSet<Notification>();

	@ManyToMany(cascade = CascadeType.PERSIST)
	@JoinTable(name = "user_followers", joinColumns = { @JoinColumn(name = "followers") }, inverseJoinColumns = {
			@JoinColumn(name = "following") })
	private Set<User> following = new HashSet<User>();

	@ManyToMany(mappedBy = "following", cascade = CascadeType.PERSIST)
	private Set<User> followers = new HashSet<User>();

	@OneToOne(mappedBy = "user", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	private Significancy significancy;

	public int getId() {
		return id;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getImage() {
		if (image != null) {
			return image;
		} else {
			return DEFAULT_IMAGE;
		}
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getCover() {
		if (cover != null) {
			return cover;
		} else {
			return DEFAULT_COVER;
		}
	}

	public void setCover(String cover) {
		this.cover = cover;
	}

	public Date getRegistered() {
		return registered;
	}

	public void setRegistered(Date registered) {
		this.registered = registered;
	}

	public Date getLogged() {
		return logged;
	}

	public void setLogged(Date logged) {
		this.logged = logged;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public Place getPlace() {
		return place;
	}

	public void setPlace(Place place) {
		this.place = place;
	}

	public Set<Student> getCoursesAsStudent() {
		return student;
	}

	public void addCourseAsStudent(Course course) {
		for (Student student : getCoursesAsStudent()) {
			if (student.getCourse().equals(course)) {
				return;
			}
		}

		course.addStudent(this);
	}

	public void addCourseAsStudent(Student student) {
		student.setUser(this);
		getCoursesAsStudent().add(student);
	}

	public Set<Tutor> getCoursesAsTutor() {
		return tutor;
	}

	public void addCourseAsTutor(Course course) {
		for (Tutor tutor : getCoursesAsTutor()) {
			if (tutor.getCourse().equals(course)) {
				return;
			}
		}

		course.addTutor(this);
	}

	public void addCourseAsTutor(Tutor tutorResult) {
		tutorResult.setUser(this);
		getCoursesAsTutor().add(tutorResult);
	}

	public Set<? extends ICoursable> getCoursesAs(Class<? extends ICoursable> clazz) {
		if (clazz.equals(Student.class)) {
			return getCoursesAsStudent();
		}
		if (clazz.equals(Tutor.class)) {
			return getCoursesAsTutor();
		}
		return null;
	}

	public Set<Activity> getActivities() {
		return activities;
	}

	public void addActivity(Activity activity) {
		activity.setUser(this);
		getActivities().add(activity);
	}

	public Set<Conversation> getPosts() {
		return posts;
	}

	public void addPost(Conversation post) {
		post.addUser(this);
		getPosts().add(post);
	}

	public void addConversation(Conversation post) {
		post.setMessage(true);
		getPosts().add(post);
	}

	public Set<Notification> getNotifications() {
		return notifications;
	}

	public void addNotification(Notification notification) {
		notification.setUser(this);
		getNotifications().add(notification);
	}

	public Significancy getSignificancy() {
		return significancy;
	}

	public Set<User> getFollowers() {
		return followers;
	}

	public Set<User> getFollowing() {
		return following;
	}

	public void addToFollow(User user) {
		addToFollow(user, true);
	}

	public void addToFollow(User user, boolean notificate) {
		if (!getFollowing().contains(user)) {
			getFollowing().add(user);

			if (notificate) {
				user.addNotification(new Notification(ENotificationTarget.USER_FOLLOW) {
					{
						setSender(User.this);
						setDescription(MESSAGE_NEW_FOLLOWER);
					}
				});
			}

			user.addActivity(new Activity(EActivityType.USER_IS_FOLLOWED));
		}
	}

	public void removeFromFollow(User user) {
		if (getFollowing().contains(user)) {
			getFollowing().remove(user);
			user.addActivity(new Activity(EActivityType.USER_IS_UNFOLLOWED));
		}
	}

	@PrePersist
	public void onCreate() {
		registered = new Date();

		significancy = new Significancy();
		significancy.setUser(this);

		// addActivity(new Activity(EActivityType.USER_REGISTER));
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null) {
			String email2 = ((User) obj).getEmail();

			if (email != null && email2 != null) {
				return email.equals(email2);
			}
		}
		return super.equals(obj);
	}

	@Override
	public int hashCode() {
		if (email == null) {
			return id;
		}
		return email.hashCode();
	}

	public static User getUserByEmail(String email) {
		Session session = getSession();

		Criteria userCriteria = session.createCriteria(User.class);
		userCriteria.add(Restrictions.eq("email", email));
		User user = (User) userCriteria.uniqueResult();

		session.close();

		return user;
	}

	public static User getTestUser() {
		String name = AppConfig.getTestName();
		String surname = AppConfig.getTestSurname();
		String email = AppConfig.getTestEmail();
		User user;

		Session session;

		try {
			session = getSession();

			Criteria userCriteria = session.createCriteria(User.class);
			userCriteria.add(Restrictions.eq("email", email));
			user = (User) userCriteria.uniqueResult();

			if (user == null) {
				user = TestingData.generateTestUser(name, surname, email, session);
				user.persist(session);
			}
			session.close();

			// SIGNIFICANCY
			session = getSession();
			userCriteria = session.createCriteria(User.class);

			for (User userS : new ArrayList<User>(userCriteria.list())) {
				Significancy.refreshTotalMark(userS);
				session.save(userS.getSignificancy());
			}
			session.getTransaction().commit();
			session.close();

		} catch (Exception e) {
			e.printStackTrace();
			user = null;
		}

		return user;
	}

	public String getNameAndSurname() {
		String wholename = "";
		if (getName() != null) {
			wholename += getName();
		}
		if (getSurname() != null) {
			wholename += " " + getSurname();
		}
		if (wholename.length() > 0) {
			return wholename;
		}
		return null;
	}

	public Criteria getLastNotifications() {

		Criteria userCriteria = getSession().createCriteria(Notification.class);
		userCriteria.add(Restrictions.eq("user", this));
		userCriteria.addOrder(Order.desc("created")).addOrder(Order.desc("id"));

		return userCriteria;
	}

	// TODO: Here gets users for UsersPage
	public static Criteria getInterestingPeople(User user) {
		return getInterestingPeople(user, false);
	}

	public static Criteria getInterestingPeople(User user, boolean notfollower) {

		Criteria userCriteria = getSession().createCriteria(User.class);

		if (user != null) {
			userCriteria.add(Restrictions.not(Restrictions.eq("id", user.getId())));
		}

		if (notfollower) {
			userCriteria.add(Restrictions.not(Restrictions.in("following", user.getFollowing())));
		}

		userCriteria.createAlias("significancy", "significancyA");
		userCriteria.addOrder(Order.desc("significancyA.totalMark"));

		return userCriteria;
	}

	public Criteria getLastPosts() {

		Criteria postsCriteria = getSession().createCriteria(Conversation.class);
		postsCriteria.createAlias("users", "user");
		postsCriteria.add(Restrictions.eq("message", false));
		postsCriteria.add(Restrictions.eq("user.id", this.getId()));
		postsCriteria.addOrder(Order.desc("created")).addOrder(Order.asc("id"));

		return postsCriteria;
	}

	public Criteria getLastPostsFollowers() {
		return getLastPostsFollowers(false);
	}

	public Criteria getLastPostsFollowers(boolean extractMe) {
		Criteria postsCriteria = getSession().createCriteria(Conversation.class);

		// FIRST - Interesting people
		Set<User> people = (new EntityLoader<User>(getInterestingPeople(this)) {

			{
				setLimit(10);
			}

			@Override
			public Component renderComponent(LinkedHashSet<User> list) {
				return null;
			}
		}).loadNext();

		// SECOND - Following people
		HashSet<User> following = new HashSet<User>(getFollowing());
		if (following.size() > 0) {
			people.addAll(following);
		}

		// THIRD - Me
		if (!extractMe) {
			people.add(this);
		}

		postsCriteria.createAlias("users", "user");
		Set<Integer> ids = new HashSet();
		for (User user : people) {
			ids.add(user.getId());
		}

		postsCriteria.add(Restrictions.eq("message", false));
		postsCriteria.add(Restrictions.in("user.id", ids));
		postsCriteria.addOrder(Order.desc("created")).addOrder(Order.desc("id"));

		return postsCriteria;
	}

	@Override
	public Criteria getLastMessages() {
		Criteria conversationCriteria = getSession().createCriteria(Conversation.class);

		conversationCriteria.add(Restrictions.eq("message", true));
		conversationCriteria.createAlias("users", "user");
		conversationCriteria.add(Restrictions.eq("user.id", this.getId()));
		conversationCriteria.addOrder(Order.desc("created")).addOrder(Order.desc("id"));

		return conversationCriteria;
	}
}
