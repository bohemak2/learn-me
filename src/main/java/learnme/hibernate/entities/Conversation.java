package learnme.hibernate.entities;

import static learnme.hibernate.Hibernate.getSession;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.Criteria;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import learnme.hibernate.BasicEntity;
import learnme.hibernate.enums.ENotificationTarget;
import learnme.hibernate.interfaces.ICreated;

@Entity
@Table(name = "conversation")
public class Conversation extends BasicEntity implements ICreated {
	
    public Conversation() {
		super();
	}
    
    public Conversation(boolean message) {
    	super();
    	
    	this.message = message;
    }

	@Id
    @GeneratedValue
    private int id;
    
    String name;

    @Temporal(TemporalType.TIMESTAMP)
    private Date created;
    
    @ColumnDefault("0")
    private boolean message;
    
    @ColumnDefault("1")
    private boolean replies = true;
    
    @OneToMany(cascade = CascadeType.ALL)
    private Set<Message> messages = new HashSet<Message>();
    
    /* Start conversation threads (comments) */
    @ManyToOne
    private Course course;
    
    @ManyToMany(cascade = CascadeType.PERSIST)
    @JoinTable(name = "conversation_user", joinColumns = { @JoinColumn(name = "conversation") }, inverseJoinColumns = {
			@JoinColumn(name = "user") })
    private Set<User> users = new HashSet<User>();
    /* End */
    
    @PrePersist
    public void onCreate() {
        created = new Date();
    }
    
    public boolean isMessage() {
		return message;
	}

	public void setMessage(boolean message) {
		this.message = message;
	}

	public String getFullName() {
		return getName(null, true);
	}
	
	public String getFullName(User me) {
		return getName(me, true);
	}

	public String getName() {
		return getName(null);
	}
	
	public String getName(User me) {
		return getName(me, false);
	}
	
	
	public String getName(User me, boolean fullname) {
		if (name == null && me != null) {
			String result = "";
			for (User user : users) {
				if(!user.equals(me)) {
					result += (fullname ? user.getNameAndSurname() : user.getName()) + ", ";
				}
			}
			return result.substring(0, result.length() - 2);
		}
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Message> getMessages() {
        return messages;
    }
    
    public int getCommentSize() {
        return messages.size() - 1;
    }    
    
    public void addMessage(Message message) {
    	addMessage(message, true);
    }
    
    public void addMessage(Message message, boolean notificate) {
    	message.setConversation(this);
    	getMessages().add(message);
    	
    	if (notificate) {
    		Conversation conversation = message.getConversation();
    		User me = message.getUser();
    		
			for(User user : conversation.getUsers()) {
	    		if(!user.equals(me)){
	    			Notification notification = new Notification(ENotificationTarget.POST_MESSAGE) {
	    				{
	    					setMessage(message);
	    					setUser(user);
	    					setSender(me);
	    				}
	    			};
	    			message.addNotification(notification);
	    		}
	    	}
		}
    	
    }
    
    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void addUser(User user) {
    	if (!users.contains(user)) {
    		users.add(user);
    	}
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public boolean isReplies() {
        return replies;
    }

    public void setReplies(boolean replies) {
        this.replies = replies;
    }
    
    public int getId() {
        return id;
    }

    public Message getConversationFirstMessage(boolean ascending) {

		Criteria messageCriteria = getSession().createCriteria(Message.class);
		messageCriteria.add(Restrictions.eq("conversation", this));
		if (ascending) {
			messageCriteria.addOrder(Order.asc("sent")).addOrder(Order.asc("id"));			
		} else {
			messageCriteria.addOrder(Order.desc("sent")).addOrder(Order.desc("id"));
		}
		messageCriteria.setMaxResults(1);

		return (Message)messageCriteria.uniqueResult();
	}

    
    public Criteria getConversationMessages() {
    	return getConversationMessages(null);
    }
    
	public Criteria getConversationMessages(Message excludeMessage) {

		Criteria messageCriteria = getSession().createCriteria(Message.class);
		messageCriteria.add(Restrictions.eq("conversation", this));
		messageCriteria.addOrder(Order.desc("sent")).addOrder(Order.desc("id"));
		if (excludeMessage != null) {
			messageCriteria.add(Restrictions.not(Restrictions.in("id", excludeMessage.getId())));
		}

		return messageCriteria;
	}

}
