function onScroll(markupId, foreground, isNextResult, fn){
	if (isNextResult){
		
		markupId = $(markupId);
		parent = markupId.parent() || $(document);
		result = null;
		handling = false;
		call = Loader.getCall(markupId.attr('id'));
		
		if(call == undefined){
			call = Loader.addCall(markupId.attr('id'), null);
		}
		
		if (!foreground){
			Loader.AjaxInForeground = false;
		}
		
		while(parent != undefined && !parent.is("body")){
			if (parent.height() < markupId.height()){
				break;
			}
			parent = parent.parent();
		}
		
		if(parent.is("body")){
			parent = $(document);
		}
		
		parent.on("scroll", function(e){
			if (!handling){
				handling = true;
				setTimeout(function(){
					if (parent.is(e.target.activemarkupId) || parent.is(document)){
						if((parent.outerHeight() * .75) < parent.scrollTop() + $(window).outerHeight()){
							if (call != undefined && call.callable != false && Loader.AjaxFree)
							{
								fn();												
							}
						}
					}
					handling = false;
				}, 100);
			}
		});
		
	//	TODO: load more since if statement in 'scroll' could be true (executed by user) - beware of no results!
		if((parent.outerHeight() * .75) < parent.scrollTop() + $(window).outerHeight()){		
			parent.scroll();
		}
	}
}
