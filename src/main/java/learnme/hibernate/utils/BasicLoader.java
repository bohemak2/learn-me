package learnme.hibernate.utils;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Set;

import org.hibernate.Criteria;

public class BasicLoader<T> implements Serializable {

	private Criteria criteria;
    private int limit = 20;
 
    
	public BasicLoader(Criteria criteria) {
		super();
		this.criteria = criteria;
	}

	public BasicLoader(Criteria criteria, int limit) {
		super();
		this.criteria = criteria;
		this.limit = limit;
	}

	public Set<T> loadNext(){
		return new LinkedHashSet<T>(criteria.list());
	}	
}
