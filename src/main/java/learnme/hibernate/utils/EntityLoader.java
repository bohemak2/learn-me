package learnme.hibernate.utils;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AbstractDefaultAjaxBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptReferenceHeaderItem;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.request.resource.JavaScriptResourceReference;
import org.hibernate.Criteria;
import org.hibernate.Session;

import learnme.hibernate.BasicEntity;
import learnme.hibernate.Hibernate;
import learnme.hibernate.enums.EEntityLoaderType;


   
public abstract class EntityLoader<T extends BasicEntity> extends AbstractDefaultAjaxBehavior implements IRenderLoader<T>{
	
    private final static JavaScriptResourceReference JS_SCROLL = new JavaScriptResourceReference(EntityLoader.class, "EntityLoaderScroll.js");
    private final static JavaScriptResourceReference JS_CLICK = new JavaScriptResourceReference(EntityLoader.class, "EntityLoaderClick.js");
    
    private Criteria criteria;
    private boolean append;
    private boolean foreground = true;
    private boolean isList;
    
    private EEntityLoaderType loaderType = EEntityLoaderType.SCROLL;
    private String loaderString;
    private boolean loaderStringBefore;
    
    private LinkedHashSet<T> result;
    private boolean nextResult;
    private boolean reverseResult;
    
    private int limit = 20;
    private int offset = 0;
    private int callback = -1;

    public EntityLoader(Set<T> list) {
    	this(list, false);
    }
    
    public EntityLoader(Set<T> list, boolean append) {
    	this.result = new LinkedHashSet<T>(list);
    	this.append = append;
    	this.isList = true;
    }
    
    public EntityLoader(Criteria criteria) {
        this(criteria, false);
    }
    
    public EntityLoader(Criteria criteria, boolean append) {
        this(criteria, append, 0);
    }
    
    public EntityLoader(Criteria criteria, boolean append, int limit) {
        super();
        
        this.criteria = criteria;
        this.append = append;
        
        if (limit > 0) {
        	this.limit = limit;
        }
    }


    @Override
    public LinkedHashSet<T> loadNext(){
    	// END of results - no SQL
    	if (result != null && result.size() == 0){
    		return result;
    	}
    	
    	callback++;
    	
    	if (isList) {
    		if (append) {
    			LinkedHashSet<T> res = new LinkedHashSet<T>();
	    		if(append && callback != 0){
	    			offset += getLimit();
		        }
	    		
	    		int iterator = offset;
	    		for(T entity: result) {
	    			if(offset >= iterator &&  iterator < limit) {
	    				res.add(entity);
	    			}
	    			iterator++;
	    		}
	    		return res;
    		}else {
    			return result;
    		}
    	} else {
	        Session session = Hibernate.getSession();
	
	        // Add 'result++' for loadNext?
	        criteria.setMaxResults(limit + 1);
	        
	        if(append && callback != 0){
	            criteria.setFirstResult(offset += limit);
	        }
	        
	        
	        ArrayList<T> list = new ArrayList<T>(criteria.list());
	        
	        if (list.size() > limit) {
	        	nextResult = true;
	        	
	        	// Remove 'result++' helper for loadNext
	        	list.remove(limit);	        		
	        } else {
	        	nextResult = false;
	        }
	        
	        session.getTransaction().commit();
	        session.close();
	        
	        return new LinkedHashSet<T>(list);
        }
    }
    
    @Override
    protected void respond(AjaxRequestTarget target) {
    	Component cp = renderComponent(loadNext());
    	if (cp != null) {
    		target.add(cp);
        }
    }
    
    @Override
    public void renderHead(Component component, IHeaderResponse response) {
    	super.renderHead(component, response);
    	
    	if (isNextResult()) {
	    	switch (loaderType) {
				case SCROLL:
					response.render(JavaScriptReferenceHeaderItem.forReference(JS_SCROLL));
					response.render(OnDomReadyHeaderItem.forScript("onScroll("+ getComponent().getMarkupId()+ "," + isForeground() + "," + isNextResult() + "," + getCallbackFunction() +");"));			
					break;
				case CLICK:
					response.render(JavaScriptReferenceHeaderItem.forReference(JS_CLICK));
					response.render(OnDomReadyHeaderItem.forScript("onClick("+ getComponent().getMarkupId()+ "," + isForeground() + "," + isNextResult() + "," + getLoaderString() + "," + isLoaderStringBefore() + "," + getCallbackFunction() +");"));
				default:
					break;
			}
    	}
    }
    
	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	
	public void setOffset(int offset) {
		this.offset = offset;
	}

	public boolean isNextResult() {
		return nextResult;
	}
	
	public boolean isAppend() {
		return append;
	}

	public void setAppend(boolean append) {
		this.append = append;
	}

	public boolean isForeground() {
		return foreground;
	}

	public void setForeground(boolean foreground) {
		this.foreground = foreground;
	}

	public EEntityLoaderType getLoaderType() {
		return loaderType;
	}

	public void setLoaderType(EEntityLoaderType loaderType) {
		this.loaderType = loaderType;
	}

	public String getLoaderString() {
		return loaderString;
	}

	public void setLoaderString(String loaderString) {
		this.loaderString = loaderString;
	}

	public boolean isLoaderStringBefore() {
		return loaderStringBefore;
	}

	public void setLoaderStringBefore(boolean loaderStringBefore) {
		this.loaderStringBefore = loaderStringBefore;
		
//		setReverseResult(true);
	}

	public boolean isReverseResult() {
		return reverseResult;
	}

	public void setReverseResult(boolean reverseResult) {
		this.reverseResult = reverseResult;
	}
}
