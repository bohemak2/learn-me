
function onClick(markupId, foreground, isNextResult, model, isModelBefore, fn){
	if(isNextResult){
		markupId = $(markupId);
		model = model || "Load more ...";
		call = Loader.getCall(markupId.attr('id'));
		
		if(call == undefined){
			call = Loader.addCall(markupId.attr('id'), null);
		}
		
		if (!foreground){
			Loader.AjaxInForeground = false;
		}
		
		target = $('<a href="#!">'+ model +'</a>');
		target.on("click", function(){
			if (Loader.AjaxFree){
				fn();			
			}
		});
		
		if (isModelBefore){
			markupId.prepend(target);
		} else {
			markupId.append(target);			
		}
	}
}
