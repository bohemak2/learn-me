package learnme.hibernate.utils;

import java.util.LinkedHashSet;

import learnme.hibernate.BasicEntity;
import org.apache.wicket.Component;

public interface IRenderLoader<T extends BasicEntity> {
	LinkedHashSet<T> loadNext();
    Component renderComponent(LinkedHashSet<T> list);
}
