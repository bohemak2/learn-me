package learnme.application;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;


public class AppConfig {
    public static boolean DEBUG = false;
	public static final boolean SHOW_DESCRIPTION = true;
    public static boolean SHOW_ROBOTS = false;
    // BEWARE! FLAGS ICON CONNECTED VIA INDEX - more in EFlags.getFlagByLocale
    public static List<Locale> LOCALES = Arrays.asList(Locale.ENGLISH, Locale.forLanguageTag("cs"));
    
    public static final String SESSION_USER_ID = "user"; 
    
    private final static String TEST_USER_FIRSTNAME = "Niles";
    private final static String TEST_USER_SURNAME = "Crane";
    private final static String TEST_USER_DOMAIN = "learnme.com";

    private final static String SYSTEM_USER_FIRSTNAME = "Guide";
    private final static String SYSTEM_USER_SURNAME = "LearnMe";
    private final static String SYSTEM_USER_DOMAIN = "learnme.com";
    
    public static String getTestName(){
        return AppConfig.TEST_USER_FIRSTNAME;
    }
    
    public static String getTestSurname(){
        return AppConfig.TEST_USER_SURNAME;
    }
    
    public static String getTestEmail(){
        return AppConfig.TEST_USER_FIRSTNAME.toLowerCase() + "." + AppConfig.TEST_USER_SURNAME.toLowerCase() + "@" + AppConfig.TEST_USER_DOMAIN;
    }

    public static String getSystemName(){
        return AppConfig.SYSTEM_USER_FIRSTNAME;
    }
    
    public static String getSystemSurname(){
        return AppConfig.SYSTEM_USER_SURNAME;
    }
    
    public static String getSystemEmail(){
        return AppConfig.SYSTEM_USER_FIRSTNAME.toLowerCase() + "." + AppConfig.SYSTEM_USER_SURNAME.toLowerCase() + "@" + AppConfig.SYSTEM_USER_DOMAIN;
    }
    
}
