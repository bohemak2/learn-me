package learnme.wicket;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.protocol.http.WebApplication;

import learnme.wicket.pages.application.ApplicationPage;
import learnme.wicket.pages.application.calendar.CalendarPage;
import learnme.wicket.pages.application.courses.CoursePage;
import learnme.wicket.pages.application.courses.CoursesPage;
import learnme.wicket.pages.application.dashboard.DashboardPage;
import learnme.wicket.pages.application.market.MarketPage;
import learnme.wicket.pages.application.messages.MessagesPage;
import learnme.wicket.pages.application.notifications.NotificationsDetailPage;
import learnme.wicket.pages.application.notifications.NotificationsPage;
import learnme.wicket.pages.application.posts.PostPage;
import learnme.wicket.pages.application.posts.PostsPage;
import learnme.wicket.pages.application.settings.SettingsPage;
import learnme.wicket.pages.application.statistics.StatisticsPage;
import learnme.wicket.pages.application.users.UserPage;
import learnme.wicket.pages.application.users.UsersPage;
import learnme.wicket.pages.general.about.AboutPage;
import learnme.wicket.pages.general.contact.ContactPage;
import learnme.wicket.pages.general.exceptions.Page4xx;
import learnme.wicket.pages.general.exceptions.Page5xx;
import learnme.wicket.pages.general.home.HomePage;
import learnme.wicket.pages.general.login.LoginPage;

/**
 * Application object for your web application. If you want to run this
 * application without deploying, run the Start class.
 *
 * @see learnme.Start#main(String[])
 */
public class WicketApplication extends WebApplication {

    private static String message;
    
    /**
     * @see org.apache.wicket.Application#getHomePage()
     */
    @Override
    public Class<? extends WebPage> getHomePage() {
        return HomePage.class;
    }

    /**
     * @see org.apache.wicket.Application#init()
     */
    @Override
    public void init() {
        super.init();
        getMarkupSettings().setStripWicketTags(true);
        
        mountPage("/404", Page4xx.class);
        mountPage("/500", Page5xx.class);
        
        mountPage("/login", LoginPage.class);
        mountPage("/about", AboutPage.class);
        mountPage("/contact", ContactPage.class);
        
        mountPage("/app", ApplicationPage.class);
        mountPage("/app/dashboard", DashboardPage.class);
        mountPage("/app/calendar", CalendarPage.class);
        mountPage("/app/courses", CoursesPage.class);
        mountPage("/app/course", CoursePage.class);
        mountPage("/app/market", MarketPage.class);
        mountPage("/app/messages", MessagesPage.class);
        mountPage("/app/notifications", NotificationsPage.class);
        mountPage("/app/notifications/detail", NotificationsDetailPage.class);
        mountPage("/app/posts", PostsPage.class);
        mountPage("/app/posts/detail", PostPage.class);
        mountPage("/app/settings", SettingsPage.class);
        mountPage("/app/statistics", StatisticsPage.class);
        mountPage("/app/user", UserPage.class);
        mountPage("/app/users", UsersPage.class);
    }

    public static String getMessage() {
        return message;
    }

    public static void setMessage(String message) {
        WicketApplication.message = message;
    }
}
