
package learnme.wicket.interfaces;

import learnme.wicket.pages.BasicPage;

public interface IWebPage extends IMarkupClasses{
    String getTitle();
    String getDescription();
    
    boolean isDescribted();
    boolean isIndexed();
    
    Class getLogoLinkPage();
    
    Class<? extends BasicPage>getPageParent();
}