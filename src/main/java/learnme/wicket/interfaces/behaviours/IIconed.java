package learnme.wicket.interfaces.behaviours;

import java.io.Serializable;

import learnme.wicket.interfaces.IIcon;

public interface IIconed extends Serializable{
	public void setIcon(IIcon icon);
	public IIcon getIcon();
}
