package learnme.wicket.interfaces.behaviours;

import java.io.Serializable;
import java.util.Optional;

import org.apache.wicket.ajax.AjaxRequestTarget;

public interface IClickable extends Serializable{
	void onClick(Optional<AjaxRequestTarget> target);
	boolean isDisabled();
}
