package learnme.wicket.interfaces.behaviours;

import java.io.Serializable;

public interface IAjaxable extends Serializable{
	public boolean isAjax();
}
