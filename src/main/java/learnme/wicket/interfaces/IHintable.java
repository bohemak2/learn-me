package learnme.wicket.interfaces;

public interface IHintable {
	String getHint();
}
