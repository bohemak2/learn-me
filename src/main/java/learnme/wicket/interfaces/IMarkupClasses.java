package learnme.wicket.interfaces;

import java.io.Serializable;

public interface IMarkupClasses extends Serializable{
    String getMarkupClass();
}