package learnme.wicket.interfaces;

import learnme.wicket.frontend.definition.EAligment;
import learnme.wicket.frontend.definition.EColors;

public interface IBasicComponent extends IHintable {
    IBasicComponent addClass(String addedClass);
    IBasicComponent removeClass(String addedClass);
    
    IBasicComponent setAlignment(EAligment alignment);
    
    IBasicComponent setBackgroundColor(EColors color);
    IBasicComponent setTextColor(EColors color);
}
