package learnme.wicket.interfaces;

import learnme.wicket.components.layout.Layout;
import org.apache.wicket.Component;

public interface ILayout {
    Layout add(Component component);
    String newId();
    Layout getLayout();
    void setLayoutTag(String layoutTagName);
    void setLayoutItemsTag(String layoutItemsTagName);
    IMarkupClasses reverseClass();
}
