package learnme.wicket.components.button;

import java.util.Optional;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;

import learnme.wicket.components.BasicComponent;
import learnme.wicket.components.link.Link;
import learnme.wicket.frontend.Utils;
import learnme.wicket.frontend.definition.EColors;
import learnme.wicket.interfaces.IBasicComponent;
import learnme.wicket.interfaces.IIcon;
import learnme.wicket.interfaces.behaviours.IAjaxable;
import learnme.wicket.interfaces.behaviours.IClickable;
import learnme.wicket.interfaces.behaviours.IIconed;


public abstract class Button extends BasicComponent implements IAjaxable, IClickable, IIconed {
	private final static String ID_BUTTON = "button";
    
    public static String CSS_SELECTOR = "btn";
    public static String CSS_REVERSED = "outline";

    private boolean ajax;
    private boolean disabled, reversed;
    private IModel<?> model;
    private Link link;
    private EColors originalBackgroundColor;
    private IIcon icon;
    private	IModel<IIcon> iconModel;
    
    public Button(String id, IIcon icon) {
    	this(id, null, icon);
    }
    
    public Button(String id, IModel<?> model) {
    	this(id, model, false);
    }
    
    public Button(String id, IModel<?> model, boolean ajax) {
    	this(id, model, null, ajax);
    }
    
    public Button(String id, IModel<?> model, IIcon icon) {
    	this(id, model, icon, false);
    }
    
    public Button(String id, IModel<?> model, IIcon icon, boolean ajax) {
        super(id);
        
        this.model = model;
        this.ajax = ajax;
        this.icon = icon;
        
        setBackgroundColor(EColors.PRIMARY);
        setTextColor(EColors.WHITE);
        addClass(CSS_SELECTOR);
    }
    
    public void setModel(IModel<?> model) {
		this.model = model;
		
		link.setModel(model);
	}

	public IModel<?> getModel() {
		return model;
	}
    
    @Override
    protected void onInitialize() {
        super.onInitialize();

        add(link = new Link(ID_BUTTON, model, isAjax()) {
			
			@Override
			protected void onInitialize() {
				
				setTextColor(Button.this.getTextColor());
				
				if (Button.this.iconModel != null) {
					link.setIcon(Button.this.iconModel);
				} else {
					if (Button.this.icon != null) {
						link.setIcon(new PropertyModel<IIcon>(Button.this, "icon"));
					}
				}
				
				super.onInitialize();
			}
			
			@Override
			public void onClick(Optional<AjaxRequestTarget> target) {
				if(!isDisabled()) {
					Button.this.onClick(target);
				}
			}
		});
    }
    
    @Override
    protected void onComponentTag(ComponentTag tag) {
    	EColors color = getBackgroundColor();

    	if(isDisabled()) {
    		if(originalBackgroundColor == null && color != null) {
    			originalBackgroundColor = color;
    		}
    		
    		this.setBackgroundColor(EColors.DEFAULT);
    	} else {   		
    		if (originalBackgroundColor != null) {
    			this.setBackgroundColor(originalBackgroundColor);
    		}
    	}
    	
    	super.onComponentTag(tag);
    }	

    @Override
    public IBasicComponent setBackgroundColor(EColors color) {
    	return super.setBackgroundColor(color, CSS_SELECTOR + "-");
    }
    
    
    public boolean isAjax() {
		return ajax;
	}

	public void setAjax(boolean ajax) {
		this.ajax = ajax;
	}
    
    public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	public boolean isReversed() {
		return reversed;
	}

	public void setReversed(boolean reversed) {
		if (reversed) {
			addClass(CSS_SELECTOR + "-" + CSS_REVERSED + "-" + getBackgroundColor().getMarkupClass());
		} else {
			removeClass(CSS_SELECTOR + "-" + CSS_REVERSED + "-" + getBackgroundColor().getMarkupClass());
		}
		this.reversed = reversed;
	}

	public IIcon getIcon() {
		return icon;
	}

	public void setIcon(IModel<IIcon> iconModel) {
		this.iconModel = iconModel;
	}
	
	public void setIcon(IIcon icon) {
		this.icon = icon;
	}
	
	public abstract void onClick(Optional<AjaxRequestTarget> target);
   
}
