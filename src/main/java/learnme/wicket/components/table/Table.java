package learnme.wicket.components.table;

import java.util.ArrayList;
import java.util.List;

import org.apache.wicket.Component;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.ResourceModel;

import learnme.wicket.components.BasicComponent;
import learnme.wicket.components.EmptyComponent;
import learnme.wicket.components.layout.Layout;

public class Table extends BasicComponent {
	
	public static final String ID_COMPONENT = TableColumn.ID_COMPONENT; 

	private static final String ID_TABLE = "table";
	
	private static final String ID_TABLE_HEAD = "table-head";
	private static final String ID_TABLE_HEAD_TR = "table-head-tr";
	
	private static final String ID_TABLE_BODY = "table-body";
	private static final String ID_TABLE_BODY_TR = "table-body-tr";
		
	private static final String CSS_TABLE_BORDERS = "no-borders";
	
	private TableRow.InnerRow head;
	private List<TableRow.InnerRow> rows = new ArrayList<>();
	private Layout layout = new Layout(ID_TABLE_BODY_TR);
	
	private boolean headInBody;
	private boolean borders;
	
	WebMarkupContainer tableLayout, bodyLayout, headLayout; // TODO: not necessary - TransparentWebMarkupContainer

	private IModel<String> noRecordsModel = new ResourceModel("NoRecords");
	
	public Table(String id) {
		this(id, false);
	}
	
	public Table(String id, boolean headInBody) {
		super(id);
		
		this.headInBody = headInBody;
	}

	@Override
	protected void onRender() {
		
		if (borders) {
			removeClass(CSS_TABLE_BORDERS);
		} else {
			addClass(CSS_TABLE_BORDERS);
		}

		super.onRender();
	}
	
	@Override
	protected void onInitialize() {
		super.onInitialize();

		add(tableLayout = new WebMarkupContainer(ID_TABLE));
		
		if (headInBody || head == null) {
			tableLayout.add(new EmptyComponent(ID_TABLE_HEAD));
			
			
		} else {
			tableLayout.add(headLayout = new WebMarkupContainer(ID_TABLE_HEAD));
			
			TableRow tableRow = new TableRow(ID_TABLE_HEAD_TR, head);
			for (TableColumn.InnerColumn td: head.getCells()) {
				tableRow.getLayout().add(new TableColumn(tableRow.getLayout().newChildId(), td));
			}
			headLayout.add(tableRow);
		}
		
		if (rows != null) {
			bodyLayout = new WebMarkupContainer(ID_TABLE_BODY);
			bodyLayout.add(layout);
			tableLayout.add(bodyLayout);
			
			if (rows.isEmpty()) {
				addRow(noRecordsModel);
			}			
		} else {
			tableLayout.add(new EmptyComponent(ID_TABLE_BODY));
		}
		
	}
	
	@Override
	protected void onBeforeRender() {
		super.onBeforeRender();
				
		if (rows != null) {			
			layout.removeAll();

			int i = 0;
			for (TableRow.InnerRow tr : rows) {
				TableRow tableRow = new TableRow(layout.newChildId(), tr);
				
				if (headInBody) {
					tableRow.getLayout().add(new TableColumn(tableRow.getLayout().newChildId(), head.getCells().get(i)));
				}

				for (TableColumn.InnerColumn td : tr.getCells()) {
					tableRow.getLayout().add(new TableColumn(tableRow.getLayout().newChildId(), td));				
				}
				
				layout.add(tableRow);
				i++;
			}
		}
	}


	public void setHeadRow(Component... components) {
		head = new TableRow.InnerRow(true, components);
	}
	
	public void setHeadRow(IModel<?>... models) {
		head = new TableRow.InnerRow(true, models);
	}

	public void addRow(Component... components) {
		rows.add(new TableRow.InnerRow(components));
	}
	
	public void addRow(IModel<?>... models) {
		rows.add(new TableRow.InnerRow(models));
	}

	public boolean isHeadInBody() {
		return headInBody;
	}

	public boolean isBorders() {
		return borders;
	}

	public void setBorders(boolean borders) {
		this.borders = borders;
	}

	public IModel<String> getNoRecordsModel() {
		return noRecordsModel;
	}

	public void setNoRecordsModel(IModel<String> noRecordsModel) {
		this.noRecordsModel = noRecordsModel;
	}	
}
