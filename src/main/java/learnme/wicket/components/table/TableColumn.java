package learnme.wicket.components.table;

import java.io.Serializable;

import org.apache.wicket.Component;
import org.apache.wicket.markup.ComponentTag;

import learnme.wicket.components.BasicComponent;

public class TableColumn extends BasicComponent {

	public static final String ID_COMPONENT = "table-body-td";
	
	InnerColumn inner;
	
	public TableColumn(String id, InnerColumn inner) {
		super(id);

		this.inner = inner;
		add(inner.getComponent());
	}

	public InnerColumn getInner() {
		return inner;
	}
	
	@Override
	protected void onComponentTag(ComponentTag tag) {
		super.onComponentTag(tag);
		
		if (inner.isHead()) {
			tag.setName("th");
		} else {
			tag.setName("td");
		}

	}

	public static class InnerColumn implements Serializable{
		
		private Component component;
		
		private boolean head;

		public InnerColumn(boolean head, Component component) {
			super();
			this.head = head;
			this.component = component;
		}

		public Component getComponent() {
			return component;
		}

		public boolean isHead() {
			return head;
		}
	}
}
