package learnme.wicket.components.table;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.wicket.Component;
import org.apache.wicket.model.IModel;

import learnme.wicket.components.BasicComponent;
import learnme.wicket.components.label.Label;
import learnme.wicket.components.layout.Layout;

public class TableRow extends BasicComponent {

	private static final String ID_ROW = "row";

	private InnerRow inner;
	private Layout layout = new Layout(ID_ROW);
	
	public TableRow(String id, InnerRow inner) {
		super(id);
		
		this.inner = inner;
		
		add(layout);
	}	
	
	public InnerRow getInner() {
		return inner;
	}

	public Layout getLayout() {
		return layout;
	}



	public static class InnerRow implements Serializable{
		
		private List<TableColumn.InnerColumn> cells = new ArrayList<TableColumn.InnerColumn>();
		
		public InnerRow(IModel<?>... models) {
			this(false, models);
		}
		
		public InnerRow(boolean head, IModel<?>... models) {
			this(head, makeComponents(models));
		}
		
		public InnerRow(Component... components) {
			this(false, components);
		}
		
		public InnerRow(boolean head, Component... components) {			
			if (components.length > 0) {
				for(Component cell : components){
					cells.add(new TableColumn.InnerColumn(head, cell));
				}
			}
		}
		
		public List<TableColumn.InnerColumn> getCells() {
			return cells;
		}
		
		private static Component[] makeComponents(IModel<?>... models) {
			Component[] components = new Component[models.length];
			for(int i = 0; models.length > i ; i++) {
				components[i] = new Label(TableColumn.ID_COMPONENT, models[i]);
			}
			return components;
		}
	}
}
