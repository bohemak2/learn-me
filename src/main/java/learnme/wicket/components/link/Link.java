package learnme.wicket.components.link;

import java.util.Optional;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxFallbackLink;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;

import learnme.wicket.components.BasicComponent;
import learnme.wicket.components.EmptyComponent;
import learnme.wicket.components.icon.Icon;
import learnme.wicket.components.label.Label;
import learnme.wicket.frontend.definition.EColors;
import learnme.wicket.interfaces.IIcon;
import learnme.wicket.interfaces.behaviours.IAjaxable;
import learnme.wicket.interfaces.behaviours.IClickable;
import learnme.wicket.interfaces.behaviours.IIconed;

public abstract class Link extends BasicComponent implements IAjaxable, IClickable, IIconed {
	
	private final static String ID_LINK = "link";
	private final static String ID_LINK_ICON = "link-icon";
	public final static String ID_LINK_CONTENT = "link-content";
	
	private boolean ajax;
	private boolean disabled;
	private Component content;
	private IModel<?> model;
	private IIcon icon;
	private	IModel iconModel; 
	
	private org.apache.wicket.markup.html.link.Link<Component> link;

	public Link(String id, IModel<?> model) {
		this(id, model, false);
	}

	public Link(String id, IModel<?> model, boolean ajax) {
		this(id, new Label(ID_LINK_CONTENT, model) {
			@Override
			public IModel<?> getModel() {
				return getModel();
			}
		}, ajax);
		
		this.model = model;
	}
	
	public Link(String id, Component content) {
		this(id, content, false);
	}
	
	public Link(String id, Component content, boolean ajax) {
		super(id);
		this.ajax = ajax;
		this.content = content;

		this.content.setRenderBodyOnly(true);
		setRenderBodyOnly(true);
	}
	
	@Override
	protected void onInitialize() {
		super.onInitialize();
		
		if(isDisabled()) {
			setTextColor(EColors.DISABLED);
		}
		
		if (isAjax()) {        
			add(link = new AjaxFallbackLink<Component>(ID_LINK) {
				
				@Override
				protected void onComponentTag(ComponentTag tag) {
					super.onComponentTag(tag);
					Link.this.addClassesToTag(tag);
				}
				
				@Override	
				public void onClick(Optional<AjaxRequestTarget> target) {
					if (!isDisabled()) {
						Link.this.onClick(target);
					}					
				}
			});
		} else {
			add(link = new org.apache.wicket.markup.html.link.Link<Component>(ID_LINK) {
				
				@Override
				protected void onComponentTag(ComponentTag tag) {
					super.onComponentTag(tag);
					Link.this.addClassesToTag(tag);
				}
				
				@Override
				public void onClick() {
					if (!isDisabled()) {
						Link.this.onClick(null);
					}							
				}
			});
		}
		
		link.add(content);
		if (content == null) {
			content.setVisible(false);
		}
		
		if (icon != null || iconModel != null) {
			if (iconModel != null) {
				link.add(new Icon(ID_LINK_ICON, iconModel));
			} else {
				link.add(new Icon(ID_LINK_ICON, new PropertyModel(this, "icon"))); 
			}
		} else {
			link.add(new EmptyComponent(ID_LINK_ICON));
		}
	}
	
	public void setAjax(boolean ajax) {
		this.ajax = ajax;
	}

	@Override
	public boolean isAjax(){
		return ajax;
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	public IModel<?> getModel() {
		return model;
	}

	public void setModel(IModel<?> model) {
		this.model = model;
	}

	public IIcon getIcon() {
		return icon;
	}

	public void setIcon(IIcon icon) {
		this.icon = icon;
	}
	
	public void setIcon(IModel iconModel) {
		this.iconModel = iconModel;
	}

	
//	@Override
//	public MarkupContainer add(Component... component) {
//		for (Component c : component) {
//			if (c.equals(link)) {
//				super.add(c);
//			}else {
//				link.add(c);
//			}
//		}
//		return null;
//	}
}
