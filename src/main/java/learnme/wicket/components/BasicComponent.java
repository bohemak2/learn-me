package learnme.wicket.components;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.wicket.markup.ComponentTag;

import learnme.wicket.frontend.Template;
import learnme.wicket.frontend.Utils;
import learnme.wicket.frontend.definition.EAligment;
import learnme.wicket.frontend.definition.EColors;
import learnme.wicket.frontend.definition.ECursors;
import learnme.wicket.frontend.definition.EDirection;
import learnme.wicket.frontend.definition.EFontSize;
import learnme.wicket.frontend.definition.EGrid;
import learnme.wicket.frontend.definition.ESpacing;
import learnme.wicket.frontend.definition.ETransform;
import learnme.wicket.interfaces.IBasicComponent;
import learnme.wicket.panels.BasicPanel;


/**
 * @author lukas.richtrmoc
 *
 */
abstract public class BasicComponent extends BasicPanel implements IBasicComponent{
    
	private static final String BACKGROUND_PREFIX = "bg-";
	private static final String TEXT_PREFIX = "text-";
	
	String classes;
    String hint;
	
	EFontSize fontSize;
    EAligment alignment;
    ETransform transform;
    
    EColors backgroundColor;
    EColors textColor;
    
    ECursors cursor;
    
    List<ESpacing> spacing;
    
    HashMap<EGrid, Boolean> responsivness = new HashMap<EGrid, Boolean>();

    public BasicComponent(String id) {
        super(id);
        
        spacing = new LinkedList<>();
    }

    @Override
    protected void onComponentTag(ComponentTag tag) {
        super.onComponentTag(tag);
        
        if (classes == null) {
        	classes = tag.getAttribute("class");
        }
        
        if (getAlignment() != null){
            addClass(getAligmentClass());
        }
        
        if (getTransform()!= null){
            addClass(getTransformClass());
        }
        
        if (getCursor() != null) {
        	addClass(getCursorClass());
        }
        
        addClass(getResponsivityClasses());
        
        addClassesToTag(tag);
        
		if (hint != null) {
			String result = hint;
			try {
				result = getLocalizer().getString(hint, this);
			} catch(Exception e){
				
			} finally {
				tag.put("title", result);
			}
		}
		
    }

    @Override
    public IBasicComponent addClass(String addedClass) {
        classes = Utils.addClass(classes, addedClass);
        return this;
    }
    
    @Override
    public IBasicComponent removeClass(String classToRemove) {
    	classes = Utils.removeClass(classes, classToRemove);
    	return this;
    }

    protected void addClassesToTag(ComponentTag tag) {
    	tag.put("class", getClasses());
    }
    
    public IBasicComponent addSpacing(ESpacing spacing, int column){
        classes = Utils.addClass(classes, spacing.getMarkupClass(column));
        return this;
    }
    
    public IBasicComponent addSpacing(ESpacing spacing, int column, EDirection direction){
        classes = Utils.addClass(classes, spacing.getMarkupClass(column, direction));
        return this;
    }
    
    
    /* JUST SET - one possibility */
    public IBasicComponent setBackgroundColor(EColors color, String PREFIX) {
    	if (backgroundColor != null) {
    		classes = Utils.replaceClass(classes, PREFIX + getBackgroundColor().getMarkupClass(), PREFIX + color.getMarkupClass());
    	} else {
    		classes = Utils.addClass(classes, PREFIX + color.getMarkupClass());
    	}
    	this.backgroundColor = color;
        return this;
    }
    
    @Override
    public IBasicComponent setBackgroundColor(EColors color) {
        return setBackgroundColor(color, BACKGROUND_PREFIX);
    }
    
    @Override
    public IBasicComponent setTextColor(EColors color) {
    	if (textColor != null) {
    		classes = Utils.replaceClass(classes, getTextColorClass(), TEXT_PREFIX + color.getMarkupClass());    		
    	} else {
    		classes = Utils.addClass(classes, TEXT_PREFIX + color.getMarkupClass());
    	}
    	
        this.textColor = color;
        return this;
    }

    public IBasicComponent setFontSize(EFontSize fontSize) {
    	if (backgroundColor != null) {
    		classes = Utils.replaceClass(classes, getBackgroundColor().getMarkupClass(), fontSize.getMarkupClass());
    	} else {
    		classes = Utils.addClass(classes, fontSize.getMarkupClass());
    	}
    	this.fontSize = fontSize;
        return this;
    }
    
    public EFontSize getFontSize() {
		return fontSize;
	}

	@Override
    public IBasicComponent setAlignment(EAligment alignment) {
        this.alignment = alignment;
        return this;
    }
    
    protected String getClasses() {
    	return classes;
    }

    public EColors getBackgroundColor() {
        return backgroundColor;
    }
    
    protected String getBackgroundColorClass() {
        return BACKGROUND_PREFIX + getBackgroundColor().getMarkupClass();
    }
    
    public EColors getTextColor() {
        return textColor;
    }
    
    protected String getTextColorClass(){
        return TEXT_PREFIX + getTextColor().getMarkupClass();
    }

    public EAligment getAlignment() {
        return alignment;
    }
    
    protected String getAligmentClass(){
        return getAlignment().getMarkupClass();
    }

    public ETransform getTransform() {
        return transform;
    }

    public void setTransform(ETransform transform) {
        this.transform = transform;
    }
    
    protected String getTransformClass(){
        return getTransform().getMarkupClass();
    }

	public ECursors getCursor() {
		return cursor;
	}

	public void setCursor(ECursors cursor) {
    	if (this.cursor != null) {
    		classes = Utils.replaceClass(classes, this.cursor.getMarkupClass(), cursor.getMarkupClass());
    	} else {
    		classes = Utils.addClass(classes, cursor.getMarkupClass());
    	}
    	
		this.cursor = cursor;
	}
	
    protected String getCursorClass(){
        return getCursor().getMarkupClass();
    }
    
    private String getResponsivityClasses() {
    	String result = "";
    	for(Map.Entry<EGrid, Boolean> entry : responsivness.entrySet()){
    		if (entry.getValue()) {
    			result += " " + Template.RESPONSIVITY_CLASS + "-" + entry.getKey().getMarkupClass();
    		}
		}
    	return result;
    }
    
    public void setResponsive(EGrid grid) {
    	setResponsivness(grid, true);
    }
    
    public void setResponsivness(EGrid grid, boolean responsive){
    	responsivness.put(grid, responsive);
    }
    
    @Override
    public String getHint() {
    	return hint;
    }
    
    public void setHint(String hintKey) {
		this.hint = hintKey;
	}
    
}
