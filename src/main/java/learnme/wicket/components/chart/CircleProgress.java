package learnme.wicket.components.chart;

import org.apache.wicket.Component;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import learnme.wicket.components.BasicComponent;
import learnme.wicket.components.EmptyComponent;
import learnme.wicket.components.icon.Icon;
import learnme.wicket.components.label.LabelHeader;
import learnme.wicket.interfaces.IIcon;

public class CircleProgress extends BasicComponent {

	private static final String ID_PROGRESS = "circle-progress";
	private static final String ID_PROGRESS_ICON = "circle-progress-icon";	

	private static final String ID_VALUE = "circle-value";
	public static final String ID_COMPONENT_LABEL = "circle-label";
	
	private static final String CSS = "counter";
	
	private Component header;
	private IIcon icon;

	private IModel<Integer> total;
	private IModel<Integer> value;
	
	private ECircleProgressType type = ECircleProgressType.SUCCESS;
	private boolean percentage;
	
	public CircleProgress(String id, IModel<Integer> value, IIcon icon) {
		this(id, null, value, null, icon);
	}

	public CircleProgress(String id, IModel<String> header, IModel<Integer> value, IIcon icon) {
		this(id, new LabelHeader(ID_COMPONENT_LABEL, header, 5), value, null, icon);
	}
	
	public CircleProgress(String id, IModel<String> header, IModel<Integer> value, IIcon icon, ECircleProgressType type) {
		this(id, new LabelHeader(ID_COMPONENT_LABEL, header, 5), value, null, icon);
		setType(type);
	}
	
	public CircleProgress(String id, Component header, IModel<Integer> value, IIcon icon, ECircleProgressType type) {
		this(id, header, value, null, icon, false);
		setType(type);
	}
	
	public CircleProgress(String id, Component header, IModel<Integer> value, IModel<Integer> total, IIcon icon) {
		this(id, header, value, total, icon, false);
	}

	public CircleProgress(String id, Component header, IModel<Integer> value, IModel<Integer> total, IIcon icon, boolean percentage) {
		super(id);
	
		this.header = header;
		this.icon = icon;

		this.value = value;
		this.total = total;
		
		this.percentage = percentage;
	}

	

	@Override
	protected void onInitialize() {
		super.onInitialize();
		
		addClass(CSS + "-card-" + type.getMarkupClass());
		
		if (header != null) {
			add(header);			
		} else {
			add(new EmptyComponent(ID_COMPONENT_LABEL));
		}
		
		IModel<String> model;
		if (percentage && total != null) {
			model = new Model<String>((total.getObject() / value.getObject() * 100f) + "%");
		} else if (total != null) {
			model = new Model<String>((total.getObject() + "/" + value.getObject()));
		} else {
			model = new Model<String>(value.getObject() + "x");
		}
		
		add(new LabelHeader(ID_VALUE, model, 4));		

		Wrapper wrapper = new Wrapper(ID_PROGRESS);
		wrapper.add(new Icon(ID_PROGRESS_ICON, icon));
		add(wrapper);
	}
	
	public ECircleProgressType getType() {
		return type;
	}

	public void setType(ECircleProgressType type) {
		this.type = type;
	}


	private class Wrapper extends WebMarkupContainer{

		public Wrapper(String id) {
			super(id);
		}
		
		@Override
		protected void onInitialize() {
			super.onInitialize();
			
			
			if (percentage && total != null) {
				float prcg = value.getObject() / total.getObject() * 100f;
				
				if (prcg > 50 && prcg <= 75) {
					addClass(CSS + "-75");
				} else if (prcg > 25 && prcg <= 50) {
					addClass(CSS + "-50");
				} else if (prcg <= 25 ){
					addClass(CSS + "-25");
				}
			}
		}
	}
}
