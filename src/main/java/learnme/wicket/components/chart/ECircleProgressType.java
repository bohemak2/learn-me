package learnme.wicket.components.chart;

import learnme.wicket.interfaces.IMarkupClasses;

public enum ECircleProgressType implements IMarkupClasses{
	SUCCESS, DANGER, WARNING;

	@Override
	public String getMarkupClass() {
		switch (this) {
		case SUCCESS:
			return "1";
		case DANGER: 
			return "2";
		case WARNING: 
			return "3";

		default:
			break;
		}
		return null;
	}
}
