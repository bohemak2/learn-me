package learnme.wicket.components.icon;

import learnme.wicket.components.BasicComponent;
import learnme.wicket.interfaces.IIcon;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;


public class Icon extends BasicComponent{
    private final String ID = "icon"; 
    
    public Icon(String id, IIcon icon){
        this(id, new Model<>(icon));
    }
    
    public Icon(String id, IModel<? extends IIcon> icon) {
        super(id);
        
        add(new WebMarkupContainer(ID){
            @Override
            protected void onComponentTag(ComponentTag tag) {
                super.onComponentTag(tag);
                tag.put("class", icon.getObject().getMarkupClass());
            }
        });
    }
}
