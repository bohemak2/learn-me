// REQUIRED Loader.js

function newCalendar(markupId, localeID, date, fileURL, callbackURL){
	
	$(function() {
		if (Loader.Srcs.Calendar == undefined || !Loader.Srcs.Calendar){
			$.getScript( fileURL )
			  .done(function( script, textStatus ) {
				 Loader.Srcs.Calendar = true;
				 init();
			 })
		} else {
			init();
		}
	});
	
	function init(){		
		$(markupId).fullCalendar({
			locale: localeID,
			header: { 
				left: 'prev title next',
				center: false,
				right: false,
			},
			defaultDate: date || (new Date()).toISOString(),
			eventLimit: true,
			aspectRatio: 2.25,
			navLinks: false,
			events: {
		        url: callbackURL,
		        beforeSend: function() {
		        	Loader.AjaxFree = false;
		        	Loader.showLoader();
		        },
		        complete: function(){
		        	Loader.AjaxFree = true;
		        	Loader.hideLoader();
		        },
	        }
		})
	}
}