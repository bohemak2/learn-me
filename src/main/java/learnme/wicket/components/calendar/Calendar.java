package learnme.wicket.components.calendar;

import java.awt.Color;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.wicket.ajax.AbstractDefaultAjaxBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.head.CssReferenceHeaderItem;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptReferenceHeaderItem;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.request.IRequestParameters;
import org.apache.wicket.request.handler.TextRequestHandler;
import org.apache.wicket.request.resource.CssResourceReference;
import org.apache.wicket.request.resource.JavaScriptResourceReference;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import learnme.hibernate.dto.DTOCalendarEvent;
import learnme.hibernate.entities.Assignment;
import learnme.hibernate.entities.User;
import learnme.hibernate.entities.assignment.Event;
import learnme.hibernate.utils.BasicLoader;
import learnme.wicket.components.BasicComponent;
import learnme.wicket.frontend.Template;
import learnme.wicket.pages.application.ApplicationPage;

public class Calendar extends BasicComponent {
	
	private final static JavaScriptResourceReference JS_MOMENT = new JavaScriptResourceReference(Calendar.class, "moment.min.js");
	private final static JavaScriptResourceReference JS_SRC = new JavaScriptResourceReference(Calendar.class, "fullcalendar.min.js");
	private final static JavaScriptResourceReference JS = new JavaScriptResourceReference(Calendar.class, "Calendar.js");
	
	private final static JavaScriptResourceReference JS_LANG_EN = new JavaScriptResourceReference(Calendar.class, "locale/en.js");
	private final static JavaScriptResourceReference JS_LANG_CS = new JavaScriptResourceReference(Calendar.class, "locale/cs.js");
    
    private final static CssResourceReference CSS = new CssResourceReference(Calendar.class, "fullcalendar.min.css");
    private final static CssResourceReference CSS_PRINT = new CssResourceReference(Calendar.class, "fullcalendar.print.css");
    
    private AbstractDefaultAjaxBehavior ajax;
    private Date date;
    
    private boolean print;
    
    public Calendar(String id) {
    	this(id, null);
    }
    
	public Calendar(String id, Date date) {
		super(id);
		
		this.date = date;
		
		add(ajax = new AbstractDefaultAjaxBehavior() {
			
			@Override
			protected void respond(AjaxRequestTarget target) {
				IRequestParameters parameters = getRequest().getRequestParameters();
				List<DTOCalendarEvent> results = new ArrayList<>();
				User me = null; 
				
				if (getPage() instanceof ApplicationPage) {
					me = ((ApplicationPage)getPage()).getUser();
				}
				
				if (parameters != null) {
					try {
						SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
						Date start = format.parse(parameters.getParameterValue("start").toString());
						Date end = format.parse(parameters.getParameterValue("end").toString());
						
						for (Assignment assignment : (new BasicLoader<Assignment>(Event.getEvents(start, end, me))).loadNext()) {
							Date since = assignment.getSince();
							Date until = assignment.getUntil();
							
							if (until == null) {
								until = since;
							}
							
							if (since.getDate() != until.getDate()) {
								Random random = new Random();
								Color color = new Color(random.nextFloat(), random.nextFloat(), random.nextFloat());
								
								DTOCalendarEvent event1 = new DTOCalendarEvent();
								event1.start = assignment.getSince().toGMTString();
								event1.title = assignment.getName() + " ->";
								event1.color = color.toString();
								
								DTOCalendarEvent event2 = new DTOCalendarEvent();
								event2.start = assignment.getUntil().toGMTString();
								event2.title = assignment.getName() + " <-";
								event2.color = color.toString();
								
								results.add(event1);
								results.add(event2);
							} else {	
								DTOCalendarEvent event = new DTOCalendarEvent();
								
								event.title = assignment.getName();
								event.start = assignment.getSince().toGMTString();
								if (since != until) {
									event.end = assignment.getUntil().toGMTString();									
								}
								event.title = assignment.getName();
								
								results.add(event);
							}
						}
						
					} catch(Exception e){
						
					} finally {
						Gson builder = (new GsonBuilder()).create();
						getRequestCycle().replaceAllRequestHandlers(new TextRequestHandler("application/json", "UTF-8", builder.toJson(results)));
					}
				}
			}
		});
	}

	@Override
	public void renderHead(IHeaderResponse response) {
		super.renderHead(response);
		
		response.render(CssReferenceHeaderItem.forReference(CSS));
		
		if (print) {
			response.render(CssReferenceHeaderItem.forReference(CSS_PRINT));			
		}
		

		response.render(JavaScriptReferenceHeaderItem.forReference(JS_MOMENT));
		response.render(JavaScriptReferenceHeaderItem.forReference(JS));
		response.render(JavaScriptReferenceHeaderItem.forReference(getLocaleResource()));

		
		response.render(JavaScriptReferenceHeaderItem.forReference(new JavaScriptResourceReference(Template.class, Template.JS_LOADER_PATH)));
		response.render(OnDomReadyHeaderItem.forScript(renderFunction(getMarkupId())));
	}
	
	private String renderFunction(String markupId) {		
		return "newCalendar(" + markupId + ", '" + getLocale().getLanguage() + "', " +  (date != null ? date.toGMTString() : "undefined") + ", '" + urlFor(JS_SRC, getPage().getPageParameters()) + "', '"+ ajax.getCallbackUrl() +"');";
	}
	
	private JavaScriptResourceReference getLocaleResource() {
		switch (getLocale().getLanguage()) {
			case "cs":
				return JS_LANG_CS;
	
			default:
				return JS_LANG_EN;
		}
	}
	
}
