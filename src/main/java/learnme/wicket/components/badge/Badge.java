package learnme.wicket.components.badge;

import learnme.wicket.components.BasicComponent;
import learnme.wicket.components.label.Label;
import learnme.wicket.frontend.definition.EBadge;
import learnme.wicket.interfaces.IHintable;

import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.html.WebMarkupContainer;

import org.apache.wicket.model.IModel;


public class Badge extends BasicComponent implements IHintable{
    public static final String ID_BADGE_WRAPPER = "badge-wrapper";
    public static final String ID_BADGE_CONTENT = "badge-content";
    
    public static String CSS_SELECTOR = "badge";
    
    private EBadge type = EBadge.DEFAULT;
    private boolean inverse;

    private Label content;
    
    public Badge(String id, IModel model) {
        this(id, model, null, null);
    }
    
    public Badge(String id, IModel model, boolean inverse) {
        this(id, model, null, null, inverse);
    }
    
    public Badge(String id, IModel model, String hint) {
        this(id, model, hint, null);
    }
    
    public Badge(String id, IModel model, EBadge badge) {
    	this(id, model, null, badge);
    }
    
    public Badge(String id, IModel model, EBadge badge, boolean inverse) {
    	this(id, model, null, badge, inverse);
    }
    
    public Badge(String id, IModel model, String hint, EBadge badge) {
    	this(id, model, hint, badge, false);
    }
    
    public Badge(String id, IModel model, String hint, EBadge badge, boolean inverse) {
        super(id);
        
        if(badge != null){
            this.type = badge;
        }
        this.inverse = inverse;
        
        setHint(hint);
        content = new Label(ID_BADGE_CONTENT, model);
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        
        add(new WebMarkupContainer(ID_BADGE_WRAPPER){
            {
                add(content);
                
                addClass(CSS_SELECTOR);
                addClass(CSS_SELECTOR + "-" + type.getMarkupClass(inverse));
            }
        });
    }

    public EBadge getType() {
        return type;
    }

    public void setType(EBadge type) {
        this.type = type;
    }

    public boolean isInverse() {
        return inverse;
    }

    public void setInverse(boolean inverse) {
        this.inverse = inverse;
    }

    public Label getContent() {
        return content;
    }

    public void setContent(Label content) {
        this.content = content;
    }
    
}
