package learnme.wicket.components.layout;

import learnme.wicket.frontend.definition.EGrid;
import learnme.wicket.frontend.definition.ETransform;
import learnme.wicket.interfaces.IMarkupClasses;


public class HorizontalLayout extends BasicLayout{
	
    public HorizontalLayout(String id) {
        super(id);
        
        setGrid(EGrid.SMALL, 0);
    }

	@Override
	public IMarkupClasses reverseClass() {
		return ETransform.REVERSE_HORIZONTAL;
	}
}