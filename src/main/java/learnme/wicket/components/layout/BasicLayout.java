package learnme.wicket.components.layout;

import learnme.wicket.components.BasicComponent;
import learnme.wicket.frontend.Utils;
import learnme.wicket.frontend.definition.EAligment;
import learnme.wicket.frontend.definition.EGrid;
import learnme.wicket.interfaces.ILayout;
import learnme.wicket.interfaces.IMarkupClasses;

import java.util.HashMap;
import java.util.Map;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.Component;
import org.apache.wicket.MarkupContainer;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.html.TransparentWebMarkupContainer;
import org.apache.wicket.markup.html.WebMarkupContainer;

/* AWARE - have to insert HTML content manually -> child/extend doesnt work*/
public class BasicLayout extends BasicComponent implements ILayout{
    
	public static final String ID_CONTENT_WRAPPER = "layout-wrapper";
	public static final String ID_CONTENT = "layout-content";
	
    Layout layout;
    String layoutTagName;
    String itemClasses;
    
    HashMap<EGrid, Integer> grid = new HashMap<EGrid, Integer>();
    
    boolean reversed;
    boolean floated;
    
    public BasicLayout(String id) {
        super(id);       
        
        super.add(layout = new Layout("layout"));
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        
        if (isReversed()) {
        	add(new AttributeAppender("class", reverseClass().getMarkupClass()));
        }
        
    }
    
    @Override
    public Layout getLayout() {
        return layout;
    }

    @Override
    public Layout add(Component component) {
        Wrapper layoutContent = new Wrapper(getLayout().newChildId()){
            {
            	
//              TODO: Revison - pristup se zmenil -> pres Utils...
                
            	add(component);
            }
            
            @Override
        	protected void onComponentTag(ComponentTag tag) {
        		super.onComponentTag(tag);
        		
        		String tagName = BasicLayout.this.layoutTagName;
        		if (tagName != null) {
        			tag.setName(tagName);
        		}
        		
        		classes = Utils.addClass("", tag.getAttribute("class"));
        		classes = Utils.addClass(classes, getGridClasses());
        		
        		if (itemClasses != null) {
        			classes = Utils.addClass(classes, itemClasses);        			
        		}

        		if (floated) {
        			classes = Utils.addClass(classes, EAligment.FLOAT_LEFT.getMarkupClass());
        		}
            	
            	if (!classes.isEmpty()) {
            		tag.put("class", classes);            		
            	}
            	
        	}
            
        };
        
        getLayout().add(layoutContent);
        return getLayout();
    }
    
    @Override
    public String newId() {
        return ID_CONTENT;
    }
    
    private class Wrapper extends WebMarkupContainer{
    	
    	String classes;
    
        public Wrapper(String id) {
            super(id);
        }
    
    }
    
    private String getGridClasses() {
    	String result = "";
    	for(Map.Entry<EGrid, Integer> entry : grid.entrySet()){
    		result += " " + entry.getKey().getMarkupClass(entry.getValue());
    	}
    	return result;
    }
	
	public void setGrid(EGrid egrid) {
		setGrid(egrid, 12);
	}
	
	public void setGrid(EGrid egrid, int columns) {
		grid.put(egrid, columns);
	}

	public boolean isReversed() {
		return reversed;
	}

	public void setReversed(boolean reversed) {
		this.reversed = reversed;
	}

	public void setLayoutTag(String layoutTagName) {
		this.layoutTagName = layoutTagName;
	}

	@Override
	public void setLayoutItemsTag(String layoutTagName) {
		this.layoutTagName = layoutTagName;
	}

	public String getItemClasses() {
		return itemClasses;
	}

	public void addItemClasses(String itemClasses) {
		this.itemClasses = Utils.addClass( this.itemClasses, itemClasses);
	}

	@Override
	public MarkupContainer removeAll() {
		return layout.removeAll();
	}
	
	@Override
	public IMarkupClasses reverseClass() {
		return null;
	}

	public boolean isFloated() {
		return floated;
	}

	public void setFloated(boolean floated) {
		this.floated = floated;
	}
	
	
}
