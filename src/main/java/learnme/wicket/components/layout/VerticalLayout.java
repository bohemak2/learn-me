package learnme.wicket.components.layout;

import learnme.wicket.frontend.definition.EGrid;
import learnme.wicket.frontend.definition.ETransform;
import learnme.wicket.interfaces.IMarkupClasses;


public class VerticalLayout extends BasicLayout{
    
    public VerticalLayout(String id) {
        super(id);
        
        setGrid(EGrid.SMALL, 12);
    }
    
    @Override
    public IMarkupClasses reverseClass() {
    	return ETransform.REVERSE_VERTICAL;
    }
}