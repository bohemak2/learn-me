package learnme.wicket.components.nonbasic;

import java.util.List;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.IModel;




abstract public class DropDown<T> extends ListView<T>{

    private T selected;
    
    public DropDown(String id, List<T> list) {
        super(id, list);
    }

    public DropDown(String id, IModel<? extends List<T>> model) {
        super(id, model);
    }

    public T getSelected() {
        return selected;
    }

    public void setSelected(T selected) {
        this.selected = selected;
    }
}
