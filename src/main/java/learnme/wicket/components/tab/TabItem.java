package learnme.wicket.components.tab;

import org.apache.wicket.Component;
import org.apache.wicket.model.IModel;

import learnme.wicket.components.BasicComponent;
import learnme.wicket.components.label.Label;
import learnme.wicket.frontend.definition.ECursors;

public class TabItem extends BasicComponent {

	private final static String ID_LINK = "tab-item-link";
	
	private static final String CSS_LINK = "nav-link";
	
	private BasicComponent tabTitle;
	private Component tabContent;
	
	private boolean disabled;
	
	public TabItem(String id, IModel tabTitle, Component tabContent) {
		super(id);
		
		this.tabTitle = new Label(ID_LINK, tabTitle) {
			{
				setCursor(ECursors.POINTER);
				addClass(CSS_LINK);
			}
		};
		this.tabContent = tabContent;
	}
	
	@Override
	protected void onInitialize() {
		super.onInitialize();
		
		add(tabTitle);
	}
	
	public BasicComponent getTabTitle() {
		return tabTitle;
	}

	public Component getTabContent() {
		return tabContent;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	public boolean isDisabled() {
		return disabled;
	}
}
