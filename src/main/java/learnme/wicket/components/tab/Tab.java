package learnme.wicket.components.tab;


import java.util.LinkedHashSet;
import java.util.Set;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxEventBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.html.TransparentWebMarkupContainer;
import org.apache.wicket.model.IModel;

import learnme.wicket.components.BasicComponent;
import learnme.wicket.components.EmptyComponent;
import learnme.wicket.components.label.Label;
import learnme.wicket.components.layout.BasicLayout;

public class Tab extends BasicComponent {

	private static final String ID_TITLE = "tab-title";
	private static final String ID_MENU_ITEMS = "tab-menu-items";
	private static final String ID_CONTENT = "tab-contents";
	private static final String ID_CONTENT_ITEMS = "tab-contents-items";
		
	private static final String CSS_CONTENT = "tab-content";
	private static final String CSS_CONTENT_ITEMS = "nav-item";
	private static final String CSS_MENU = "nav nav-tabs md-tabs";
	
	private static final String CSS_HORIZONTAL_LEFT_MENU = "tabs-left";
	private static final String CSS_HORIZONTAL_LEFT_CONTENT = "tabs-left-content";
	
//	TODO: Integrate bellow CSS properties
	private static final String CSS_MENU_MATERIAL = "md-tabs";

	private Component header;
	private Set<TabItem> items;
	
	private BasicLayout tabs;
	private BasicLayout contents;
	
	private TabItem selected;
	
	public Tab(String id) {
		this(id, null, false);
	}
	
	public Tab(String id, boolean vertical) {
		this(id, null, vertical);
	}
	
	public Tab(String id, IModel header) {
		this(id, header != null ? new Label(ID_TITLE, header) : null, true);
	}

	public Tab(String id, Component header, boolean vertical) {
		super(id);
		
		this.items = new LinkedHashSet<TabItem>();
		this.header = header;
		this.tabs = new BasicLayout(ID_MENU_ITEMS) {
			{
				setLayoutTag("li");
				addItemClasses(CSS_CONTENT_ITEMS);

				addClass(CSS_MENU);
				
				if (vertical) {
					addClass(CSS_HORIZONTAL_LEFT_MENU);
				}
			}			
		};
		this.contents = new BasicLayout(ID_CONTENT_ITEMS);
		
		add(new TransparentWebMarkupContainer(ID_CONTENT) {
			
			@Override
			protected void onComponentTag(ComponentTag tag) {
				super.onComponentTag(tag);
				
				if (vertical) {
					tag.put("class", CSS_CONTENT + " " + CSS_HORIZONTAL_LEFT_CONTENT);
				}
			}
		});
	}
	
	@Override
	protected void onInitialize() {
		
		if (header != null) {
			add(header);
		} else {
			add(new EmptyComponent(ID_TITLE));
		}
		
		int i = 0;
		for(TabItem item : items) {
			item.getTabTitle().add(new AjaxEventBehavior("click") {
				
				@Override
				protected void onEvent(AjaxRequestTarget target) {
					Tab.this.setSelected(item);
					
					target.add(tabs);
					target.add(contents);
				}
			});	
			
			tabs.add(item);
			contents.add(item.getTabContent());
			
			if (i != 0) {
				item.getTabContent().setVisible(false);
			} else {
				setSelected(item);
			}
			
			i++;
		}		
		
		add(tabs);
		add(contents);
		super.onInitialize();
	}
	
	private void setSelected(TabItem item) {
		if (selected == null) {
			selected = item;
			selected.getTabTitle().addClass("active");
		}
		
		if (!selected.equals(item)) {
			selected.getTabTitle().removeClass("active");
			selected.getTabContent().setVisible(false);
			selected = item;
			selected.getTabTitle().addClass("active");
			selected.getTabContent().setVisible(true);
		}
	}
	
	public void addTabItem(TabItem item){		
		items.add(item);
	}	
	
	public String subTitleId() {
		return ID_TITLE;
	}
	
    public String newTabId(){
        return tabs.newId();
    }
    
    public String newContentId() {
    	return contents.newId();
    }
}
