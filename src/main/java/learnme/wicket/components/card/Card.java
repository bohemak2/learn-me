package learnme.wicket.components.card;

import learnme.wicket.components.BasicComponent;
import learnme.wicket.components.EmptyComponent;
import learnme.wicket.frontend.definition.ECursors;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxEventBehavior;
import org.apache.wicket.markup.html.image.Image;

public class Card extends BasicComponent{
    public static final String ID_HEADER = "header";
    public static final String ID_IMAGE = "image";
    public static final String ID_CONTENT = "content";
    
    Image image;

    public Card(String id, Component content){
        this(id, content, null);
    }
    
    public Card(String id, Component content, Component header){
        this(id, content, header, null, null);
    }
    
    public Card(String id, Component content, Component header, AjaxEventBehavior event) {
    	this(id, content, header, null, event);
    }
    
    public Card(String id, Component content, Image image){
        this(id, content, null, image, null);
    }
    
    public Card(String id, Component content, Component header, Image image, AjaxEventBehavior event) {
        super(id);
        
        if(image == null){
            add(new EmptyComponent(ID_IMAGE));
        }else{
            add(image);
        }
        
        if(header == null){
            header = new EmptyComponent(ID_HEADER);
        }
        
        if(event != null){
        	add(event);
        	setCursor(ECursors.POINTER);
        }
        
        add(header);
        add(content);
    }
}
