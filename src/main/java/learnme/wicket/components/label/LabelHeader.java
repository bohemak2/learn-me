package learnme.wicket.components.label;

import org.apache.wicket.model.IModel;


public class LabelHeader extends Label {

    public LabelHeader(String id, IModel<?> model, int header) {
        super(id, model);
        setHeader(header);
    }
    
}
