package learnme.wicket.components.label;

import learnme.wicket.components.BasicComponent;
import learnme.wicket.frontend.Template;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.model.IModel;


public class Label extends BasicComponent{
    
    private org.apache.wicket.markup.html.basic.Label label;
    private int header = 0;
    private boolean paragraph;
    private boolean onlyBody;
    private IModel<?> model;
    
    public Label(String id, IModel<?> model) {
        super(id);
        
        this.model = model;
        
        label = new org.apache.wicket.markup.html.basic.Label("label", model){
        	
            @Override
            protected void onComponentTag(ComponentTag tag) {
                super.onComponentTag(tag);
                if(isParagraph()){
                    tag.setName("p");
                }
                
                if(header != 0){
                    tag.setName("h" + header);
                }
            }
        };
    }
    
    @Override
    protected void onInitialize() {
        super.onInitialize();
        
        label.setEscapeModelStrings(false);
        label.setRenderBodyOnly(isRenderBodyOnlyLabel());
        add(label);
    }

    protected int getHeader() {
        return header;
    }

    protected void setHeader(int header) {
        if (header > 0 && header < Template.HEADERS_COUNT){
            this.header = header;
        }
    }

    public boolean isParagraph() {
        return paragraph;
    }

    public void setParagraph(boolean paragraph) {
        this.paragraph = paragraph;
    }

    public boolean isRenderBodyOnlyLabel() {
        return onlyBody;
    }

    public void setRenderBodyOnlyLabel(boolean onlyBody) {
        this.onlyBody = onlyBody;
    }

	public IModel<?> getModel() {
		return model;
	}

	public void setModel(IModel<String> model) {
		this.model = model;
	}
}
