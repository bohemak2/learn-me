package learnme.wicket.components;

import org.apache.wicket.Component;


public class EmptyComponent extends Component{

    public EmptyComponent(String id) {
        super(id);
        setVisible(false);
    }

    @Override
    protected void onRender() {}

}
