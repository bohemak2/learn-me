package learnme.wicket.components.menu;

import java.util.Optional;

import org.apache.wicket.MarkupContainer;
import org.apache.wicket.Page;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;

import learnme.wicket.components.BasicComponent;
import learnme.wicket.components.EmptyComponent;
import learnme.wicket.components.button.Button;
import learnme.wicket.components.icon.Icon;
import learnme.wicket.components.layout.Layout;
import learnme.wicket.frontend.definition.ESymbols;
import learnme.wicket.interfaces.IIcon;
import learnme.wicket.interfaces.behaviours.IClickable;
import learnme.wicket.interfaces.behaviours.IIconed;
import learnme.wicket.pages.BasicPage;


public class MenuItem extends BasicComponent implements IClickable, IIconed {
	private static final long serialVersionUID = 1L;
	
	public static final String ID_LINK = "menu-item-link";
    private static final String ID_ICON = "menu-item-icon";
    private static final String ID_MODEL = "menu-item-model";
    private static final String ID_SUBITEMS = "menu-item-subitems";
    
    private Layout subitems;
    private IModel<?> model;
    private IIcon icon;
    private Class<? extends Page> page;
    private MarkupContainer link;
    private boolean button;
    private Button buttonComponent;
    
    public MenuItem(String id, IModel<?> model) {
        this(id, model, ESymbols.NULL);
    }
    
    public MenuItem(String id, IModel<?> model, IIcon icon) {
        this(id, model, icon, null);
    }
    
    public MenuItem(String id, IModel<?> model, IIcon icon, Class<? extends Page> page) {
        super(id);
        this.model = model;
        this.icon = icon;
        this.page = page;
        this.subitems = new Layout(ID_SUBITEMS);
    }
    
    @Override
    protected void onInitialize() {
        super.onInitialize();
               	
        
        if(isButton()) {
	    	add(link = new WebMarkupContainer(ID_LINK) {
	    		@Override
	    		protected void onComponentTag(ComponentTag tag) {
	    			super.onComponentTag(tag);
	    			tag.setName("div");
	    		}
	    	});
        } else {
	    	add(link = new Link<Object>(ID_LINK) {
	    		
	    		@Override
	    		public void onClick() {
	    			if (page != null){
	    				setResponsePage(page);
	    			}
	    			MenuItem.this.onClick(null);
	    		}
	    	});
    	}
        
        
    	if(isButton()) {
    		link.add(buttonComponent = new Button(ID_MODEL, model) {
    			
    			{
    				setIcon(new PropertyModel(MenuItem.this, "icon"));
    			}
    			
    			
        		@Override
        		public void onClick(Optional<AjaxRequestTarget> target) {
        			if (page != null){
        				setResponsePage(page);
        			}
        			MenuItem.this.onClick(null);
        		}
        		
        	});
    	} else {
    		if (model != null){
    			link.add(new Label(ID_MODEL, model));
    		} else {
    			link.add(new EmptyComponent(ID_MODEL));
    		}     		        		
    	}
        
        
        if (icon != null){
        	if(isButton()) {
        		link.add(new EmptyComponent(ID_ICON));
    		} else {
    			link.add(new Icon(ID_ICON, new PropertyModel<IIcon>(this, "icon")));    			
    		}
        }
        
        if (subitems != null && subitems.size() > 0){
            add(subitems);
        } else{
            add(new EmptyComponent(ID_SUBITEMS));
        }
        
        Page actualpage = getPage();
        
        if (page != null && (page.equals(actualpage) || (actualpage instanceof BasicPage && page.equals(((BasicPage)actualpage).getPageParent())))){
        	addClass("active");
        }

    }
    
    public Layout getSubitems() {
        return subitems;
    }

	public IIcon getIcon() {
		return icon;
	}

	public void setIcon(IIcon icon) {
		this.icon = icon;
	}

	@Override
	public void onClick(Optional<AjaxRequestTarget> target) {}

	@Override
	public boolean isDisabled() {
		return false;
	}

	public boolean isButton() {
		return button;
	}

	public void setButton(boolean button) {
		this.button = button;
	};
	
	public <Optional>Button getButton() {
		return buttonComponent;
	}
}
