package learnme.wicket.components.menu;

import learnme.wicket.components.BasicComponent;
import learnme.wicket.components.EmptyComponent;
import learnme.wicket.components.layout.Layout;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.IModel;


public class MenuContainer extends BasicComponent{
    private static final String ID_MENU_CONTAINER_HEADER = "menu-container-header";
    private static final String ID_MENU_CONTAINER_ITEMS = "menu-container-items";
        
    Layout items;
    
    public MenuContainer(String id) {
        this(id, null);
    }
    
    public MenuContainer(String id, IModel header) {
        super(id);
        
        if(header != null){
            add(new Label(ID_MENU_CONTAINER_HEADER, header));
        }else{
            add(new EmptyComponent(ID_MENU_CONTAINER_HEADER));
        }
        add(items = new Layout(ID_MENU_CONTAINER_ITEMS));
    }
    
    public void addMenuItem(MenuItem item){
        items.add(item);
    }
    
    public String newItemId(){
        return items.newChildId();
    }
}
