package learnme.wicket.components.menu;

import learnme.wicket.components.BasicComponent;
import learnme.wicket.components.layout.Layout;


public class Menu extends BasicComponent{
    private static final String ID_MENU_CONTAINERS = "menu-containers";
    
    Layout containers;
    MenuItem selected;
    
    public Menu(String id) {
        super(id);
        add(containers = new Layout(ID_MENU_CONTAINERS));
    }
    
    public void addMenuContainer(MenuContainer item){
        containers.add(item);
    }
    
    public String newItemId(){
        return containers.newChildId();
    }

    public MenuItem getSelected() {
        return selected;
    }

    public void setSelected(MenuItem selected) {
        this.selected = selected;
    }
}
