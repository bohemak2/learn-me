package learnme.wicket.pages.application.posts;

import java.util.HashSet;
import java.util.LinkedHashSet;

import org.apache.wicket.Component;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import learnme.hibernate.entities.Conversation;
import learnme.hibernate.utils.EntityLoader;
import learnme.wicket.components.layout.VerticalLayout;
import learnme.wicket.frontend.definition.EGrid;
import learnme.wicket.pages.application.ApplicationPage;
import learnme.wicket.panels.application.conversation.ConversationCardPanel;


public class PostsPage extends ApplicationPage{

	private VerticalLayout list;
	private EntityLoader<Conversation> loader;
	
	
    public PostsPage(PageParameters parameters) {
        super(parameters);
        
		list = new VerticalLayout(getContent().ID_BODY) {
			{
				setGrid(EGrid.MEDIUM, 4);
			}
		};
		list.add(loader = new EntityLoader<Conversation>(getUser().getLastPostsFollowers(), true) {
			
			@Override
			public Component renderComponent(LinkedHashSet<Conversation> conversations) {
				if(conversations.size() > 0) {
					fillConversations(conversations);
					return list;
				}
				return null;
			}			
		});
    }
    
    @Override
    protected void onInitialize() {
    	super.onInitialize();
    	
    	fillConversations(loader.loadNext());
    	getContent().setBody(list);
    	getContent().setHeader(new ResourceModel("PostsHeader"));
    }
    
	private void fillConversations(HashSet<Conversation> conversations) {
		for(Conversation conversation: conversations) {
			list.add(new ConversationCardPanel(list.newId(), conversation, true, false));
		}
	}
}
