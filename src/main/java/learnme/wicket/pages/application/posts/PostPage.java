package learnme.wicket.pages.application.posts;

import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import learnme.hibernate.entities.Conversation;
import learnme.wicket.pages.BasicPage;
import learnme.wicket.pages.application.ApplicationPage;
import learnme.wicket.pages.application.messages.MessagesPage;
import learnme.wicket.panels.application.conversation.ConversationCardPanel;

public class PostPage extends ApplicationPage {

	Conversation conversation;
	
	public PostPage(PageParameters parameters) {
		super(parameters);
		
		setResponsePage(new PostsPage(parameters));
	}
	
	public PostPage(PageParameters parameters, Conversation conversation) {
		super(parameters);
		this.conversation = conversation;
	}

	@Override
	protected void onInitialize() {
		super.onInitialize();
		
		if (conversation != null) {
			if (conversation.isMessage()) {
				getContent().setHeader(new Model<String>(conversation.getFullName(getUser())));
			} else {
				getContent().clearHeader();
			}
			getContent().setBody(new ConversationCardPanel(getContent().ID_BODY, conversation, true, true, 10));
		}
	}
	
	@Override
	public Class<? extends BasicPage> getPageParent() {
		if (conversation.isMessage()) {
			return MessagesPage.class;
		}
		return PostsPage.class;
	}
}
