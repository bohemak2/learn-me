package learnme.wicket.pages.application.statistics;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import learnme.hibernate.dto.DTOActivitiesCount;
import learnme.hibernate.entities.Activity;
import learnme.hibernate.utils.BasicLoader;
import learnme.wicket.components.card.Card;
import learnme.wicket.components.chart.CircleProgress;
import learnme.wicket.components.chart.ECircleProgressType;
import learnme.wicket.components.layout.VerticalLayout;
import learnme.wicket.frontend.definition.EGrid;
import learnme.wicket.frontend.definition.ESymbols;
import learnme.wicket.interfaces.IIcon;
import learnme.wicket.pages.application.ApplicationPage;


public class StatisticsPage extends ApplicationPage{

    public StatisticsPage(PageParameters parameters) {
        super(parameters);
        
        Calendar cal = new GregorianCalendar();
        cal.setTime(new Date());
        cal.add(Calendar.DAY_OF_MONTH, -30);
        
        BasicLoader<DTOActivitiesCount> statistics = new BasicLoader<>(Activity.getActivities(getUser(), null, null, null, cal.getTime(), null, true));
        
        getContent().setHeader(new ResourceModel("StatisticsHeader"));
        getContent().setBody(new VerticalLayout(getContent().ID_BODY) {
        	{
        		setGrid(EGrid.XLARGE, 3);
        		setGrid(EGrid.LARGE, 4);
        		setGrid(EGrid.MEDIUM, 6);
        		setGrid(EGrid.SMALL, 12);
        		
        		for (DTOActivitiesCount field : statistics.loadNext()) {
        			
        			ResourceModel model = null;
        			IIcon icon = null;
        			ECircleProgressType type = ECircleProgressType.SUCCESS;
        			
        			switch (field.type) {
						case USER_LOGIN:
							model = new ResourceModel("StatisticsLogin");
							icon = ESymbols.UNLOCK;
							type = ECircleProgressType.WARNING;
							break;
						case COURSE_SIGNED:
							model = new ResourceModel("StatisticsCourseSigned");
							icon = ESymbols.BLACKBOARD;
							type = ECircleProgressType.WARNING;
							break;
						case COURSE_COMPLETED:
							model = new ResourceModel("StatisticsCourseCompleted");
							icon = ESymbols.MEDALL;
							type = ECircleProgressType.SUCCESS;
							break;
						case COURSE_AS_TUTOR_ADDED:
							model = new ResourceModel("StatisticsCourseTutorAdded");
							icon = ESymbols.RULER_PENCIL;
							type = ECircleProgressType.WARNING;
							break;
						case USER_IS_FOLLOWED:
							model = new ResourceModel("StatisticsCourseFollowed");
							icon = ESymbols.ID_BADGE;
							type = ECircleProgressType.SUCCESS;
							break;
						case USER_IS_UNFOLLOWED:
							model = new ResourceModel("StatisticsCourseUnFollowed");
							icon = ESymbols.ID_BADGE;
							type = ECircleProgressType.DANGER;
						default:
							break;
					}
        			
        			
        			if (model != null && icon != null) {
        				add(new Card(newId(), new CircleProgress(Card.ID_CONTENT, model, new PropertyModel<Integer>(field, "count"), icon, type)));        				
        			}
        		}
        	}
        });
    }

}
