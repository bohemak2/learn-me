package learnme.wicket.pages.application.courses;

import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import learnme.hibernate.entities.User;
import learnme.wicket.pages.application.ApplicationPage;
import learnme.wicket.panels.application.course.CoursesCentralPanel;


public class CoursesPage extends ApplicationPage{

	private User user;
	
	public CoursesPage(PageParameters parameters) {
		this(parameters, null);
	}
	
    public CoursesPage(PageParameters parameters, User user) {
        super(parameters);
    }

    @Override
    protected void onInitialize() {
    	super.onInitialize();
    	
    	getContent().setHeader(new ResourceModel("CoursesHeader"));
    	
    	if (user == null) {
    		user = getUser();
    	}
    	
    	getContent().setBody(new CoursesCentralPanel(getContent().ID_BODY, user));
    }
}
