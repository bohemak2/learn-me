package learnme.wicket.pages.application.courses;

import org.apache.wicket.model.Model;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import learnme.hibernate.entities.Course;
import learnme.hibernate.entities.User;
import learnme.hibernate.entities.user.Student;
import learnme.hibernate.entities.user.Tutor;
import learnme.wicket.components.layout.VerticalLayout;
import learnme.wicket.components.tab.Tab;
import learnme.wicket.components.tab.TabItem;
import learnme.wicket.pages.BasicPage;
import learnme.wicket.pages.application.ApplicationPage;
import learnme.wicket.pages.application.market.MarketPage;
import learnme.wicket.panels.application.assignment.AssignmentsListPanel;
import learnme.wicket.panels.application.conversation.ConversationListPanel;
import learnme.wicket.panels.application.course.CourseChapterPanel;
import learnme.wicket.panels.application.course.CourseDescriptionPanel;
import learnme.wicket.panels.application.course.CoursePeoplePanel;
import learnme.wicket.panels.application.course.CourseProfilePanel;

public class CoursePage extends ApplicationPage {
	
	private static final String CSS_COURSE_HEADER = "course-header-items";
	
	private Course course;
	
	public CoursePage(PageParameters parameters) {
		super(parameters);
		
		setResponsePage(MarketPage.class);
	}
	
	public CoursePage(PageParameters parameters, Course course) {
		super(parameters);
		
		this.course = course;
		
		getContent().clearHeader();
	}
	
	@Override
	protected void onInitialize() {
		super.onInitialize();
			
		
		VerticalLayout layout = new VerticalLayout(getContent().ID_BODY);
		
		layout.add(new CourseProfilePanel(layout.newId(), getUser(), course));
		
		layout.add(new Tab(layout.newId()) {
			{
				Model<Course> courseModel = new Model(course); 

				addTabItem(new TabItem(newTabId(), new ResourceModel("CourseTabDescription"), new CourseDescriptionPanel(newContentId(), course)));
				addTabItem(new TabItem(newTabId(), new ResourceModel("CourseTabPost"), new Tab(newContentId(), true) {
					{
						addTabItem(new TabItem(newTabId(), new StringResourceModel("CourseTabPosts", courseModel), new ConversationListPanel(newContentId(), course)));
						addTabItem(new TabItem(newTabId(), new StringResourceModel("CourseTabReview", courseModel), new ConversationListPanel(newContentId(), course)));
					}
				}));
				
				addTabItem(new TabItem(newTabId(), new ResourceModel("CourseTabPeople"), new CoursePeoplePanel(newContentId(), course)));
				addTabItem(new TabItem(newTabId(), new StringResourceModel("CourseTabChapters", courseModel), new CourseChapterPanel(newContentId(), course)));
				
				User me = getUser();
				if (course.isParticipant(me)) {
					addTabItem(new TabItem(newTabId(), new StringResourceModel("CourseTabAssigments", courseModel), new AssignmentsListPanel(newContentId(), null, course)));
				}
			}
		});
		
		getContent().setBody(layout);
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}
	
	@Override
	public Class<? extends BasicPage> getPageParent() {
		User me = getUser();
		
		for (Student student : me.getCoursesAsStudent()) {
			if (student.getCourse().equals(course)) {
				return CoursesPage.class; 
			}
		}
		
		for (Tutor tutor : me.getCoursesAsTutor()) {
			if (tutor.getCourse().equals(course)) {
				return CoursesPage.class; 
			}
		}
		
		return MarketPage.class;
	}
}
