package learnme.wicket.pages.application.users;

import org.apache.wicket.model.Model;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import learnme.hibernate.entities.User;
import learnme.hibernate.entities.user.Student;
import learnme.hibernate.entities.user.Tutor;
import learnme.hibernate.interfaces.IPostable;
import learnme.wicket.components.layout.VerticalLayout;
import learnme.wicket.components.tab.Tab;
import learnme.wicket.components.tab.TabItem;
import learnme.wicket.pages.BasicPage;
import learnme.wicket.pages.application.ApplicationPage;
import learnme.wicket.panels.application.conversation.ConversationListPanel;
import learnme.wicket.panels.application.course.CoursesListPanel;
import learnme.wicket.panels.application.user.UserPeoplePanel;
import learnme.wicket.panels.application.user.UserProfilePanel;

public class UserPage extends ApplicationPage{
	
	private User user;
	
	public UserPage(PageParameters parameters) {
		this(parameters, null);
	}
	
	public UserPage(PageParameters parameters, User user) {
		super(parameters);
		
		if (user == null) {
			user = getUser();
		}
		this.user = user;
	}
	
	@Override
	protected void onInitialize() {
		super.onInitialize();
		
		getContent().clearHeader();
		
		getContent().setBody(new VerticalLayout(getContent().ID_BODY) {
			
			{
				add(new UserProfilePanel(newId(), user));
				
				add(new Tab(newId()) {
					{
						addTabItem(new TabItem(newTabId(), new ResourceModel("UserTabPost"), new ConversationListPanel(newContentId(), (IPostable)user)));
						addTabItem(new TabItem(newTabId(), new ResourceModel("UserTabPeople"), new UserPeoplePanel(newContentId(), user)));
						addTabItem(new TabItem(newTabId(), new ResourceModel("UserCourseList"), new Tab(newContentId(), true) {
							{
								Model<User> model = new Model<>(user);
								addTabItem(new TabItem(newId(), new StringResourceModel("UserTutor", model) , new CoursesListPanel(newId(), user, Tutor.class)));
								addTabItem(new TabItem(newId(), new StringResourceModel("UserStudent", model) , new CoursesListPanel(newId(), user, Student.class)));
							}
						}));
					}
				});
			}
			
		});
	}
	
	@Override
	public Class<? extends BasicPage> getPageParent() {
		return UsersPage.class;
	}
}

