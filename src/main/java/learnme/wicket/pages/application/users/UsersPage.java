package learnme.wicket.pages.application.users;

import java.util.LinkedHashSet;
import java.util.Set;

import org.apache.wicket.Component;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import learnme.hibernate.entities.User;
import learnme.hibernate.utils.EntityLoader;
import learnme.wicket.components.card.Card;
import learnme.wicket.components.layout.VerticalLayout;
import learnme.wicket.frontend.definition.EGrid;
import learnme.wicket.pages.application.ApplicationPage;
import learnme.wicket.panels.application.user.UserLittlePreviewPanel;
import learnme.wicket.panels.application.user.UserPreviewPanel;


public class UsersPage extends ApplicationPage{
    
    private EntityLoader<User> loader;
    private VerticalLayout list;

    public UsersPage(PageParameters parameters) {
        super(parameters);
        

        list = new VerticalLayout(getContent().ID_BODY);
        list.setGrid(EGrid.SMALL, 6);
        list.setGrid(EGrid.MEDIUM, 4);
        list.setGrid(EGrid.LARGE, 3);
        list.add(loader = new EntityLoader<User>(User.getInterestingPeople(getUser(), false), true) {
        	
            @Override
            public Component renderComponent(LinkedHashSet<User> users) {
            	if (!users.isEmpty()) {
            		fillUsers(users);
            		return list;
                }
            	return null;
            }
            
        });
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        
        getContent().setHeader(new ResourceModel("UsersHeader"));
        
        fillUsers(loader.loadNext());
        
        getContent().setBody(list);
    }
    
    private void fillUsers(Set<User> users){
        for(User user : users){        	
            list.add(new Card(list.newId(), new UserPreviewPanel(Card.ID_CONTENT, user), new VerticalLayout(Card.ID_HEADER) {
            	{
            		add(new UserLittlePreviewPanel(newId(), user, true, true, true, false));
            	}	
            }));
        }
    }
}
