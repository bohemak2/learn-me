package learnme.wicket.pages.application.calendar;

import org.apache.wicket.request.mapper.parameter.PageParameters;

import learnme.wicket.components.button.Button;
import learnme.wicket.components.calendar.Calendar;
import learnme.wicket.components.card.Card;
import learnme.wicket.pages.application.ApplicationPage;


public class CalendarPage extends ApplicationPage{
	Button btn;
	
    public CalendarPage(PageParameters parameters) {
        super(parameters);
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        
        getContent().clearHeader();
        getContent().setBody(new Card(getContent().ID_BODY, new Calendar(Card.ID_CONTENT)));
    }
}
	