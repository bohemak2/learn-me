package learnme.wicket.pages.application;

import org.apache.wicket.RestartResponseAtInterceptPageException;
import org.apache.wicket.Session;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import learnme.application.AppConfig;
import learnme.hibernate.entities.Activity;
import learnme.hibernate.entities.User;
import learnme.hibernate.enums.EActivityType;
import learnme.wicket.WicketApplication;
import learnme.wicket.components.menu.Menu;
import learnme.wicket.components.menu.MenuContainer;
import learnme.wicket.components.menu.MenuItem;
import learnme.wicket.frontend.definition.EColors;
import learnme.wicket.frontend.definition.EDirection;
import learnme.wicket.frontend.definition.ESpacing;
import learnme.wicket.frontend.definition.ESymbols;
import learnme.wicket.pages.BasicPage;
import learnme.wicket.pages.application.calendar.CalendarPage;
import learnme.wicket.pages.application.courses.CoursesPage;
import learnme.wicket.pages.application.dashboard.DashboardPage;
import learnme.wicket.pages.application.market.MarketPage;
import learnme.wicket.pages.application.messages.MessagesPage;
import learnme.wicket.pages.application.notifications.NotificationsPage;
import learnme.wicket.pages.application.posts.PostsPage;
import learnme.wicket.pages.application.statistics.StatisticsPage;
import learnme.wicket.pages.application.users.UsersPage;
import learnme.wicket.pages.general.login.LoginPage;
import learnme.wicket.panels.application.content.ContentPanel;
import learnme.wicket.panels.application.header.HeaderPanel;
import learnme.wicket.panels.application.sidemenu.SideMenu;


public class ApplicationPage extends BasicPage{
	private static final String ID_CONTENT = "content";
    private static final String ID_HEADER = "header";
    private static final String ID_SIDE_MENU = "side-menu";
    
    User user;
    ContentPanel content;

    public ApplicationPage(PageParameters parameters) {
        super(parameters);
        
        user = (User)getSession().getAttribute(AppConfig.SESSION_USER_ID);
        
        if(user == null){
            throw new RestartResponseAtInterceptPageException(LoginPage.class);
        }
        
        add(new HeaderPanel(ID_HEADER));
        add(content = new ContentPanel(ID_CONTENT){
            {
                if (user.getNameAndSurname() != null){
                    setHeader(new StringResourceModel("ContentWelcome", this, new Model<User>(user) {	
                    }));
                }
            }
        });
        add(new SideMenu(ID_SIDE_MENU){

			{
                setMenu(new Menu(ID_MENU){
                {
                    addMenuContainer(new MenuContainer(newItemId(), null){
                        {
                            addMenuItem(new MenuItem(newItemId(), new ResourceModel("SideMenuItemOverview"), ESymbols.LAYERS, DashboardPage.class));
                            addSpacing(ESpacing.MARGIN, 1, EDirection.VERTICAL);
                        }
                    });
                    addMenuContainer(new MenuContainer(newItemId(), new ResourceModel("SideMenuHeaderPersonal")){
                        {
                            addMenuItem(new MenuItem(newItemId(), new ResourceModel("SideMenuItemCalendar"), ESymbols.CALENDAR, CalendarPage.class));
                            addMenuItem(new MenuItem(newItemId(), new ResourceModel("SideMenuItemMessages"), ESymbols.COMMENTS	, MessagesPage.class));
                            addMenuItem(new MenuItem(newItemId(), new ResourceModel("SideMenuItemNotifications"), ESymbols.CHECK_BOX, NotificationsPage.class));
                        }
                    });
                    addMenuContainer(new MenuContainer(newItemId(), new ResourceModel("SideMenuHeaderNetwork")){
                      {
                          addMenuItem(new MenuItem(newItemId(), new ResourceModel("SideMenuItemPosts"), ESymbols.ALIGN_CENTER, PostsPage.class));
                          addMenuItem(new MenuItem(newItemId(), new ResourceModel("SideMenuItemUsers"), ESymbols.ID_BADGE, UsersPage.class));
                      }
                    });
                    addMenuContainer(new MenuContainer(newItemId(), new ResourceModel("SideMenuHeaderCourses")){
                        {
                            addMenuItem(new MenuItem(newItemId(), new ResourceModel("SideMenuItemMarket"), ESymbols.BAG, MarketPage.class));
                            addMenuItem(new MenuItem(newItemId(), new ResourceModel("SideMenuItemCourses"), ESymbols.BLACKBOARD, CoursesPage.class){
                            	{
                            		setBackgroundColor(EColors.PRIMARY_DARK);
                            	}
                            });
                        }
                    });

                    addMenuContainer(new MenuContainer(newItemId(), new ResourceModel("SideMenuHeaderOthers")){
                      {
                          addMenuItem(new MenuItem(newItemId(), new ResourceModel("SideMenuItemStatistics"), ESymbols.BAR_CHART, StatisticsPage.class));
                      }
                    });
                }
            });
            }
        });
    }

    @Override
    protected void onConfigure() {
    	super.onConfigure();
    	
    	if(getClass() == ApplicationPage.class) {
        	throw new RestartResponseAtInterceptPageException(DashboardPage.class);
        }
    }
    
    @Override
    public Class<ApplicationPage> getLogoLinkPage() {
        return ApplicationPage.class;
    }
    
    protected void setUser(User user) {
    	if (!this.user.equals(user)) {
    		this.user = user;    			

    		getSession().setAttribute(AppConfig.SESSION_USER_ID, this.user);
    	}
    }
    
    public User getUser() {
        return user;
    }

    public ContentPanel getContent() {
        return content;
    }

    @Override
    public String getMarkupClass() {
        return "page-application";
    }
    
    public static boolean logUserIn(Session session, String email, String password){
        User user;
        
        if(email.equals(AppConfig.getTestEmail())){
        	
            user = User.getTestUser();
            session.setAttribute(AppConfig.SESSION_USER_ID, user);
            
            Activity activity = new Activity(EActivityType.USER_LOGIN);
            activity.setUser(user);
            activity.persist();
            
            return true;
        }else{
            user = User.getUserByEmail(email);
            if (user != null){
                if (password.equals(user.getPassword())){
                    session.setAttribute(AppConfig.SESSION_USER_ID, user);
                    
                    Activity activity = new Activity(EActivityType.USER_LOGIN);
                    activity.setUser(user);
                    activity.persist();
                    
                    return true;
                }
                WicketApplication.setMessage("Wrong password");
            }else{
                WicketApplication.setMessage("Wrong email");
            }
        }
        
        return false;
    }
    
    public static boolean logUserOut(Session session){
        try{
            session.removeAttribute(AppConfig.SESSION_USER_ID);
            return true;
        } catch(Exception e){
            return false;
        }
    }
}
