package learnme.wicket.pages.application.dashboard;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import learnme.hibernate.entities.Conversation;
import learnme.hibernate.entities.Course;
import learnme.hibernate.entities.Message;
import learnme.hibernate.entities.Notification;
import learnme.hibernate.entities.User;
import learnme.hibernate.entities.assignment.Event;
import learnme.hibernate.utils.BasicLoader;
import learnme.wicket.components.card.Card;
import learnme.wicket.components.chart.CircleProgress;
import learnme.wicket.components.chart.ECircleProgressType;
import learnme.wicket.components.label.Label;
import learnme.wicket.components.label.LabelHeader;
import learnme.wicket.components.layout.VerticalLayout;
import learnme.wicket.components.link.Link;
import learnme.wicket.frontend.definition.EColors;
import learnme.wicket.frontend.definition.EGrid;
import learnme.wicket.frontend.definition.ESymbols;
import learnme.wicket.pages.application.ApplicationPage;
import learnme.wicket.pages.application.calendar.CalendarPage;
import learnme.wicket.pages.application.courses.CoursePage;
import learnme.wicket.pages.application.courses.CoursesPage;
import learnme.wicket.pages.application.notifications.NotificationsPage;
import learnme.wicket.pages.application.posts.PostPage;
import learnme.wicket.panels.application.course.CoursePreview;
import learnme.wicket.panels.application.user.UserLittlePreviewPanel;
import learnme.wicket.panels.application.user.UserPreviewPanel;

public class DashboardPage extends ApplicationPage {

	private final static String CSS = "dashboard";
	
	public DashboardPage(PageParameters parameters) {
		super(parameters);
		
		User me = getUser();
		
		getContent().setBody(new VerticalLayout(getContent().ID_BODY) {
			{
				
				add(new VerticalLayout(newId()) {
					{
						setGrid(EGrid.SMALL, 6);
						setGrid(EGrid.MEDIUM, 4);
						setGrid(EGrid.LARGE, 3);

						Calendar nextSevenDay = new GregorianCalendar();
						nextSevenDay.setTime(new Date());
						nextSevenDay.add(Calendar.DAY_OF_MONTH, +7);

						add(new Card(newId(), new CircleProgress(Card.ID_CONTENT, new Link(CircleProgress.ID_COMPONENT_LABEL, new LabelHeader(Link.ID_LINK_CONTENT, new ResourceModel("DashboardCalendar"), 5)) {
							
							@Override
							public void onClick(Optional<AjaxRequestTarget> target) {
								setResponsePage(new CalendarPage(getPageParameters()));
							}
							
						}, new Model<Integer>((new BasicLoader<>(Event.getEvents(new Date(), nextSevenDay.getTime(), me))).loadNext().size()), ESymbols.CALENDAR, ECircleProgressType.SUCCESS)));
						
						add(new Card(newId(), new CircleProgress(Card.ID_CONTENT, new Link(CircleProgress.ID_COMPONENT_LABEL, new LabelHeader(Link.ID_LINK_CONTENT, new ResourceModel("DashboardNotifications"), 5)) {

							@Override
							public void onClick(Optional<AjaxRequestTarget> target) {
								setResponsePage(new NotificationsPage(getPageParameters()));
							}
							
						}, new Model<Integer>((new BasicLoader<>(Notification.getNotifications(me, null, false, true))).loadNext().size()), ESymbols.BELL, ECircleProgressType.DANGER)));
						
						add(new Card(newId(), new CircleProgress(Card.ID_CONTENT, new Link(CircleProgress.ID_COMPONENT_LABEL, new LabelHeader(Link.ID_LINK_CONTENT, new ResourceModel("DashboardCoursesStarted"), 5)) {
							
							@Override
							public void onClick(Optional<AjaxRequestTarget> target) {
								setResponsePage(new CoursesPage(getPageParameters(), me));
							}
							
						}, new Model<Integer>((new BasicLoader<>(Course.getCourses(me, true, false, true))).loadNext().size()), ESymbols.BLACKBOARD, ECircleProgressType.WARNING)));
						
						
					}
				});
				
				add(new VerticalLayout(newId()) {
					{
						setGrid(EGrid.SMALL, 6);
						setGrid(EGrid.MEDIUM, 4);
						setGrid(EGrid.LARGE, 3);
						
						int size, rand;
						
						List<User> people = new ArrayList<User>((new BasicLoader<User>(User.getInterestingPeople(me))).loadNext());
						size = people.size();
						
						if (size > 0) {
							rand = (new Random()).nextInt(size);
							User user = people.get(rand == size ? rand - 1 : rand);
							
							add(new Card(newId(), new VerticalLayout(Card.ID_CONTENT) {
								{
									add(new UserLittlePreviewPanel(newId(), user, true, true, false, false));
									add(new UserPreviewPanel(newId(), user));
									addClass(CSS + "-recommend-user");
								}
							}, new LabelHeader(Card.ID_HEADER, new ResourceModel("DashboardSomeoneToFollow"), 5)));
						}
						
						List<Course> courses = new ArrayList<Course>((new BasicLoader<Course>(Course.getInterestingCourses(me))).loadNext());
						size = courses.size();
						if (size > 0) {
							rand = (new Random()).nextInt(size);
							Course course = courses.get(rand == size ? rand - 1 : rand);
							
							add(new Card(newId(),new VerticalLayout(Card.ID_CONTENT) {
								{
									add(new Link(newId(), new LabelHeader(Link.ID_LINK_CONTENT, new PropertyModel<String>(course, "getName"), 3)) {
										
										{
											setTextColor(EColors.DARK);
										}
										
										@Override
										public void onClick(Optional<AjaxRequestTarget> target) {
											setResponsePage(new CoursePage(getPage().getPageParameters(), course));
										}
									});
									
									add(new CoursePreview(newId(), course, false));
								}
							}, new LabelHeader(Card.ID_HEADER, new ResourceModel("DashboardCourseToSign"), 5)));
						}
					
					
						List<Conversation> conversations = new ArrayList<Conversation>((new BasicLoader<Conversation>(me.getLastPostsFollowers(true))).loadNext());
						size = conversations.size();
						if (size > 0) {
							rand = (new Random()).nextInt(size);
							Conversation conversation = conversations.get(rand == size ? rand - 1 : rand);
							Message message = conversation.getConversationFirstMessage(true);
							
							add(new Card(newId(),new VerticalLayout(Card.ID_CONTENT) {
								{
									add(new Link(newId(), new LabelHeader(Link.ID_LINK_CONTENT, new PropertyModel<String>(message.getUser(), "getNameAndSurname"), 3)) {
										
										{
											setTextColor(EColors.DARK);
										}
										
										@Override
										public void onClick(Optional<AjaxRequestTarget> target) {
											setResponsePage(new PostPage(getPage().getPageParameters(), conversation));
										}
									});
									
									add(new Label(newId(), new PropertyModel<String>(message, "getText")));
								}
							}, new LabelHeader(Card.ID_HEADER, new ResourceModel("DashboardPostToSee"), 5)));
						}
					}
				});
			}
		});
	}

}
