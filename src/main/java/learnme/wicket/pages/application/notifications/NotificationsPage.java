package learnme.wicket.pages.application.notifications;

import java.util.LinkedHashSet;
import java.util.Optional;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import learnme.hibernate.entities.Notification;
import learnme.hibernate.utils.EntityLoader;
import learnme.wicket.components.card.Card;
import learnme.wicket.components.icon.Icon;
import learnme.wicket.components.label.Label;
import learnme.wicket.components.link.Link;
import learnme.wicket.components.table.Table;
import learnme.wicket.frontend.definition.EColors;
import learnme.wicket.frontend.definition.ESymbols;
import learnme.wicket.pages.application.ApplicationPage;
import learnme.wicket.panels.application.notification.NotificationBadgesPanel;
import learnme.wicket.panels.application.user.UserLittlePreviewPanel;


public class NotificationsPage extends ApplicationPage{
    
	private static final String CSS_TABLE_WRAPPER = "notifications-page-table";
	
	private Card card;
	private Table table;
	
	private EntityLoader<Notification> entities;
	
    public NotificationsPage(PageParameters parameters) {
        super(parameters);
        
        table = new Table(Card.ID_CONTENT){
        	{
        		setHeadRow(new ResourceModel("NotificationsSeen"), new ResourceModel("NotificationsType"), new ResourceModel("NotificationsSender"), new ResourceModel("NotificationsContent"), new ResourceModel("NotificationsDate"));
        		setBorders(true);
        	}
        };
        
        table.addClass(CSS_TABLE_WRAPPER);
        table.add(entities = new EntityLoader<Notification>(getUser().getLastNotifications(), true) {
        	
        	@Override
        	public Component renderComponent(LinkedHashSet<Notification> list) {
        		if (!list.isEmpty()) {
        			fillNotifications(list);        			
        			return table;
        		}
        		return null;
        	}

        });
    }
    
    @Override
    protected void onInitialize() {
        super.onInitialize();
        
        fillNotifications(entities.loadNext());
        
        getContent().setHeader(new ResourceModel("NotificationsHeader"));
        getContent().setBody(new Card(getContent().ID_BODY, table));      
    }
    
    private void fillNotifications(LinkedHashSet<Notification> list) {
    	 for(Notification notification : list){
    		
    		table.addRow(new Icon(Table.ID_COMPONENT, ESymbols.BELL) {
    			{
    				if (notification.getSeen() == null) {
    					setTextColor(EColors.DARK);
    				} else {
    					setVisible(false);
    				}
    			}
    		}, new NotificationBadgesPanel(Table.ID_COMPONENT, notification), new UserLittlePreviewPanel(Table.ID_COMPONENT, notification.getSender(), true), new Link(Table.ID_COMPONENT, new PropertyModel<String>(notification, "getDescription")) {
				
				@Override
				public void onClick(Optional<AjaxRequestTarget> target) {
					setResponsePage(new NotificationsDetailPage(getPageParameters(), notification));
				}
			}, new Label(Table.ID_COMPONENT, new PropertyModel<String>(notification, "getCreated().toGMTString()")));
    	}    	
    }
}
