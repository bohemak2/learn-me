package learnme.wicket.pages.application.notifications;

import java.util.Date;
import learnme.hibernate.entities.Notification;
import learnme.wicket.pages.BasicPage;
import learnme.wicket.pages.application.ApplicationPage;
import learnme.wicket.panels.application.content.ContentPanel;
import learnme.wicket.panels.application.notification.NotificationDetail;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;


public class NotificationsDetailPage extends ApplicationPage{
      
    private Notification notification;

    public NotificationsDetailPage(PageParameters parameters){
    	super(parameters);
    	
    	setResponsePage(new NotificationsPage(parameters));
    }
    
    public NotificationsDetailPage(PageParameters parameters, Notification notification){
        super(parameters);
        
        if (notification.getSeen() == null){
            notification.setSeen(new Date());
            notification.persist();
        }
        
        this.notification = notification;
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        
        getContent().clearHeader();   
        getContent().setBody(new NotificationDetail(ContentPanel.ID_BODY, notification));
    }
    
    @Override
    public Class<? extends BasicPage> getPageParent() {
    	return NotificationsPage.class;
    }
}
