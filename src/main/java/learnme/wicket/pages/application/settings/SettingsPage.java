package learnme.wicket.pages.application.settings;

import java.util.LinkedHashSet;
import java.util.Optional;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import learnme.hibernate.entities.Activity;
import learnme.hibernate.entities.User;
import learnme.hibernate.enums.EActivityType;
import learnme.hibernate.utils.EntityLoader;
import learnme.wicket.components.card.Card;
import learnme.wicket.components.label.LabelHeader;
import learnme.wicket.components.link.Link;
import learnme.wicket.components.tab.Tab;
import learnme.wicket.components.tab.TabItem;
import learnme.wicket.components.table.Table;
import learnme.wicket.pages.application.ApplicationPage;
import learnme.wicket.panels.application.user.UserLittlePreviewPanel;


public class SettingsPage extends ApplicationPage{

    public SettingsPage(PageParameters parameters) {
        super(parameters);
        
        User me = getUser();
        
//		TODO: JUST FOR TESTING
        EntityLoader<User> loader = new EntityLoader<User>(User.getInterestingPeople(me)) {

			@Override
			public Component renderComponent(LinkedHashSet<User> list) {
				return null;
			}
        	
        };
//      END  
        
        getContent().setHeader(new ResourceModel("SettingsHeader"));
        getContent().setBody(
        		new Tab(getContent().ID_BODY) {
        			{
        				addTabItem(new TabItem(newTabId(), new ResourceModel("SettingsInfo"), new Card(newContentId(), 
        					new Table(Card.ID_CONTENT, true) {
	        					{
	        						setHeadRow(new ResourceModel("SettingsInfoName"), new ResourceModel("SettingsInfoSurname"), new ResourceModel("SettingsInfoEmail"), new ResourceModel("SettingsInfoBirthday"), new ResourceModel("SettingsInfoCity"), new ResourceModel("SettingsInfoRegistered"));
	        						addRow(new PropertyModel<String>(me, "getName"));
	        						addRow(new PropertyModel<String>(me, "getSurname"));
	        						addRow(new PropertyModel<String>(me, "getEmail"));
	        						addRow(new PropertyModel<String>(me, "getBirthday"));
	        						addRow(new PropertyModel<String>(me, "getPlace().getAddress"));
	        						addRow(new PropertyModel<String>(me, "getRegistered().toGMTString"));
	        					}
							})
        				));
        				
//        				TODO: JUST FOR TESTING
        				addTabItem(new TabItem(newTabId(), new ResourceModel("SettingsAccount"), new Card(newContentId(), 
            					new Table(Card.ID_CONTENT) {
    	        					{    	        						
    	        						for (User user : loader.loadNext()) {
    	        							addRow(new Link(Table.ID_COMPONENT, new UserLittlePreviewPanel(Link.ID_LINK_CONTENT, user, true, true, false, true)) {
												
												@Override
												public void onClick(Optional<AjaxRequestTarget> target) {
													setUser(user);

										            Activity activity = new Activity(EActivityType.USER_LOGIN);
										            activity.setUser(user);
										            activity.persist();
										            
													setResponsePage(SettingsPage.class);
												}
											});
	        							}
    	        					}
    							}, new LabelHeader(Card.ID_HEADER, new ResourceModel("SettingsAccountChange"), 5)
        					)
						));
//        				END
        			}
        		}        		
        );
    }
}
