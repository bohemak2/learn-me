package learnme.wicket.pages.application.messages;

import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import learnme.hibernate.entities.User;
import learnme.hibernate.interfaces.IMessagable;
import learnme.wicket.pages.application.ApplicationPage;
import learnme.wicket.panels.application.conversation.ConversationListPanel;

public class MessagesPage extends ApplicationPage {

	public MessagesPage(PageParameters parameters) {
		this(parameters, null);
	}
	
	public MessagesPage(PageParameters parameters, User user) {
		super(parameters);
		
		if(user == null) {
			user = getUser();
		}
		
		getContent().setHeader(new ResourceModel("MessagesHeader"));		
		getContent().setBody(new ConversationListPanel(getContent().ID_BODY, (IMessagable)user));
	}
	
}
