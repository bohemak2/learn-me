package learnme.wicket.pages.application.market;

import learnme.wicket.pages.application.ApplicationPage;
import learnme.wicket.panels.application.course.CoursesSelectableListPanel;

import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;


public class MarketPage extends ApplicationPage{

    public MarketPage(PageParameters parameters) {
        super(parameters);
    }
    
    @Override
    protected void onInitialize() {
    	super.onInitialize();
    	
    	getContent().setHeader(new ResourceModel("ContentTitle"));
    	getContent().setBody(new CoursesSelectableListPanel(getContent().ID_BODY));
    }

}
