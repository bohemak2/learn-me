package learnme.wicket.pages;

import learnme.application.AppConfig;
import learnme.wicket.WicketApplication;
import learnme.wicket.frontend.Template;
import learnme.wicket.interfaces.IWebPage;
import learnme.wicket.pages.general.home.HomePage;

import org.apache.wicket.Page;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptContentHeaderItem;
import org.apache.wicket.markup.head.JavaScriptReferenceHeaderItem;
import org.apache.wicket.markup.html.TransparentWebMarkupContainer;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.request.resource.JavaScriptResourceReference;

public abstract class BasicPage extends WebPage implements IWebPage{
    
    private static final String TITLE_PREFIX = "LearnMe";
    private static final String TITLE_DIVIDER = " | ";
    
    public static final String BODY_ID = "body";
    

    public BasicPage(PageParameters parameters) {
        super(parameters);
        
        add(new Label("title", new Model<String>() {
            @Override
            public String getObject() {
                String title = TITLE_PREFIX;
        
                if (getTitle() != null){
                    title += TITLE_DIVIDER + getTitle();
                }
                return title;
            }          
        }));
        
        add(new WebMarkupContainer("robots"){
			@Override
            protected void onComponentTag(ComponentTag tag) {
                super.onComponentTag(tag);
                tag.put("content", isIndexed() ? "index, follow" : "noindex, nofollow");
            }
        });
        
        add(new WebMarkupContainer("description"){
            @Override
            protected void onComponentTag(ComponentTag tag) {
                super.onComponentTag(tag);
                tag.put("content", getDescription());
            }
        });
        
        add(new TransparentWebMarkupContainer(BODY_ID){
            @Override
            protected void onComponentTag(ComponentTag tag) {
                String pageClass = getMarkupClass();
                if(pageClass != null){
                    tag.put("class", getMarkupClass());
                }
            }
        });
        
        add(new WebMarkupContainer("loader"){
            @Override
            protected void onComponentTag(ComponentTag tag) {
                tag.put("class", Template.LOADER_CLASS);
            }
            
        });
        
        add(new Label("message", WicketApplication.getMessage()));
    }

    @Override
    public String getDescription(){
        try{
            return getString("PageDescription");
        } catch(Exception e){
            return null;
        }
    };
    
    @Override
    public String getTitle() {
        try{
            return getString("PageTitle");
        } catch(Exception e){
            return null;
        }
    }

    @Override
    public boolean isDescribted(){
        return AppConfig.SHOW_DESCRIPTION;
    }

    @Override
    public boolean isIndexed(){
        return AppConfig.SHOW_ROBOTS;
    }

    @Override
    public Class<? extends Page> getLogoLinkPage(){
        return HomePage.class;
    };

    @Override
    public String getMarkupClass() {
        return "page-basic";
    }
    
    @Override
    public void renderHead(IHeaderResponse response) {
    	super.renderHead(response);	
    	response.render(JavaScriptReferenceHeaderItem.forReference(new JavaScriptResourceReference(Template.class, Template.JS_LOADER_PATH)));
    	response.render(new JavaScriptContentHeaderItem(Template.JS_LOADER_INITILIZE, null, null));
    }
    
    @Override
    public Class<? extends BasicPage> getPageParent() {
    	return this.getClass();
    }
}
