package learnme.wicket.pages.general;

import learnme.wicket.pages.BasicPage;
import learnme.wicket.pages.general.home.HomePage;
import learnme.wicket.panels.general.footer.FooterPanel;
import learnme.wicket.panels.general.header.HeaderPanel;
import org.apache.wicket.request.mapper.parameter.PageParameters;


public class GeneralPage extends BasicPage{

    public GeneralPage(PageParameters parameters) {
        super(parameters);
               
        add(new HeaderPanel("header"));
        add(new FooterPanel("footer"));
    }

    @Override
    public boolean isIndexed() {
        return true;
    }

    @Override
    public Class<HomePage> getLogoLinkPage() {
        return HomePage.class;
    }

    @Override
    public String getMarkupClass() {
        return "page-general";
    }
}
