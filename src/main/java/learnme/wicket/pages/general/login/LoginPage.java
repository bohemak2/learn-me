package learnme.wicket.pages.general.login;

import java.util.Optional;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import learnme.application.AppConfig;
import learnme.wicket.components.button.Button;
import learnme.wicket.components.card.Card;
import learnme.wicket.components.layout.HorizontalLayout;
import learnme.wicket.frontend.definition.EColors;
import learnme.wicket.pages.application.ApplicationPage;
import learnme.wicket.pages.application.dashboard.DashboardPage;
import learnme.wicket.pages.general.GeneralPage;


public class LoginPage extends GeneralPage {

    public LoginPage(PageParameters parameters) {
        super(parameters);
        
        if(getSession().getAttribute(AppConfig.SESSION_USER_ID) != null){
            setResponsePage(DashboardPage.class);
        }
        
        add(new Card("login-card", new Label(Card.ID_HEADER, new ResourceModel("ChooseEnter")), new HorizontalLayout(Card.ID_CONTENT) {
			{
        		add(new Button(newId(), new ResourceModel("LoginButton"), true){
        			
        			{
        				setDisabled(true);
        			}
        			
        			@Override
        			public void onClick(Optional<AjaxRequestTarget> target) {
        			}
        		});       		
        	
        		add(new Button(newId(), new ResourceModel("RegisterButton"), true){
        			
        			{
        				setDisabled(true);
        			}
        			
        			@Override
        			public void onClick(Optional<AjaxRequestTarget> target) {
        			}
        		});
        		
        		add(new Button(newId(), new ResourceModel("TestButton"), true){
        			{
        				setBackgroundColor(EColors.INFO);
        			}

        			@Override
        			public void onClick(Optional<AjaxRequestTarget> target) {
        				if (ApplicationPage.logUserIn(getSession(), AppConfig.getTestEmail(), null)){
        					setResponsePage(DashboardPage.class);
        				}else{
        					setResponsePage(LoginPage.class);
        				}
        			}
        		});
			}
    	}));
	}
}
