package learnme.wicket.pages.general.home;

import java.util.Optional;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import learnme.wicket.components.button.Button;
import learnme.wicket.components.label.LabelHeader;
import learnme.wicket.components.layout.VerticalLayout;
import learnme.wicket.frontend.definition.EColors;
import learnme.wicket.frontend.definition.EDirection;
import learnme.wicket.frontend.definition.ESpacing;
import learnme.wicket.pages.general.GeneralPage;
import learnme.wicket.pages.general.login.LoginPage;

public class HomePage extends GeneralPage {
        
	public HomePage(final PageParameters parameters) {
            super(parameters);
                
            VerticalLayout vl = new VerticalLayout("home-content");
            
            vl.add(new LabelHeader(vl.newId(), new ResourceModel("PageTitle"), 1));
            vl.add(new Button(vl.newId(), new ResourceModel("EnterButton")){

                {
                    setBackgroundColor(EColors.WARNING);
                    addSpacing(ESpacing.MARGIN, 4, EDirection.TOP);
                }
                
                @Override
            	public void onClick(Optional<AjaxRequestTarget> target) {
                	setResponsePage(LoginPage.class);
                }                
            });       
            add(vl);
        }

    @Override
    public boolean isIndexed() {
        return true;
    }
}