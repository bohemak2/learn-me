package learnme.wicket.pages.general.exceptions;

import learnme.wicket.WicketApplication;
import learnme.wicket.pages.general.GeneralPage;
import org.apache.wicket.request.mapper.parameter.PageParameters;


public abstract class ErrorPage extends GeneralPage{

    public ErrorPage(PageParameters parameters) {
        super(parameters);

        // TODO: Connect with Exception
        WicketApplication.setMessage(getTitle());
    }

    @Override
    public boolean isVersioned() {
        return false;
    }

    @Override
    public boolean isErrorPage() {
        return true;
    }
}
