package learnme.wicket.pages.general.exceptions;

import org.apache.wicket.request.http.WebResponse;
import org.apache.wicket.request.mapper.parameter.PageParameters;


public class Page4xx extends ErrorPage{

    public Page4xx(PageParameters parameters) {
        super(parameters);
    }

    @Override
    protected void configureResponse(WebResponse response) {
        super.configureResponse(response);
        response.setStatus(404);
    }


    @Override
    public String getTitle() {
        return "Not Found";
    }
}
