package learnme.wicket.pages.general.exceptions;

import org.apache.wicket.request.http.WebResponse;
import org.apache.wicket.request.mapper.parameter.PageParameters;


public class Page5xx extends ErrorPage{

    public Page5xx(PageParameters parameters) {
        super(parameters);
    }

    @Override
    protected void configureResponse(WebResponse response) {
        super.configureResponse(response);
        response.setStatus(500);
    }
    
    @Override
    public String getTitle() {
        return "Error";
    }

}
