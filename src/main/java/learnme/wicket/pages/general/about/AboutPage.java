package learnme.wicket.pages.general.about;

import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import learnme.wicket.components.card.Card;
import learnme.wicket.components.label.Label;
import learnme.wicket.components.label.LabelHeader;
import learnme.wicket.pages.general.GeneralPage;


public class AboutPage extends GeneralPage{

	private static final String ID_CONTENT = "about-content";
	
    public AboutPage(PageParameters parameters) {
        super(parameters);
    }
    
    @Override
    protected void onInitialize() {
    	super.onInitialize();
    	
    	add(new Card(ID_CONTENT, new Label(Card.ID_CONTENT, new ResourceModel("PageText")), new LabelHeader(Card.ID_HEADER, new ResourceModel("PageTitle"), 1)));	 
	}
}
