package learnme.wicket.frontend;

public class Template {
	public static final String LOADER_CLASS = "theme-loader";
	public static final String RESPONSIVITY_CLASS = "responsive";
	
	public static final String JS_LOADER_PATH = "Loader.js";
	public static final String JS_LOADER_INITILIZE = "Loader.onInitialize(\"."+ LOADER_CLASS +"\")";
	 
    public static final int GRID_COUNT = 12;
    public static final int SPACING_COUNT = 5;
    public static final int HEADERS_COUNT = 6;
}
