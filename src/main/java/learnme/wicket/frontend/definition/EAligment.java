package learnme.wicket.frontend.definition;

import learnme.wicket.interfaces.IMarkupClasses;


public enum EAligment implements IMarkupClasses{
    TEXT_LEFT, TEXT_CENTER, TEXT_RIGHT,
    FLOAT_LEFT, FLOAT_RIGHT,
    BLOCK_CENTER;

    @Override
    public String getMarkupClass() {
        switch(this){
            case TEXT_LEFT:
                return "text-left";
            case TEXT_CENTER:
                return "text-center";
            case TEXT_RIGHT:
                return "text-right";
            case FLOAT_LEFT:
            	return "float-left";
            case FLOAT_RIGHT:
            	return "float-right";
            case BLOCK_CENTER:
                return "mx-auto";
            default:
                return null;
        }
    }
}