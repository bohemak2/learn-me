package learnme.wicket.frontend.definition;

import learnme.wicket.interfaces.IMarkupClasses;

public enum ETransform implements IMarkupClasses{
    CAPITALIZE, LOWERCASE, UPPERCASE,
    REVERSE_VERTICAL, REVERSE_HORIZONTAL;

    @Override
    public String getMarkupClass() {
         switch(this){
            case CAPITALIZE:
                return "text-capitalize";
            case LOWERCASE:
                return "text-lowercase";
            case UPPERCASE:
                return "text-uppercase";
            case REVERSE_VERTICAL:
            	return "reverse-vertical";
            case REVERSE_HORIZONTAL:
            	return "reverse-horizontal";
            default:
                return null;
        }
    }
}
