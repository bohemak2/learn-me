package learnme.wicket.frontend.definition;

import learnme.wicket.frontend.Template;
import learnme.wicket.interfaces.IMarkupClasses;

public enum EGrid implements IMarkupClasses{
    SMALL, MEDIUM, LARGE, XLARGE;

    @Override
    public String getMarkupClass() {
        switch(this){
            case SMALL:
                return "col-sm";
            case MEDIUM:
                return "col-md";
            case LARGE:
                return "col-lg";
            case XLARGE:
                return "col-xl";
            default:
                return null;
        }
    }
        
    public String getMarkupClass(int column) {
        if(column > 0 && column <= Template.GRID_COUNT){
            return getMarkupClass() +  "-" + column;
        }
        return getMarkupClass();
    }
}
