package learnme.wicket.frontend.definition;

import learnme.wicket.interfaces.IMarkupClasses;

public enum EColors implements IMarkupClasses{
    DEFAULT, PRIMARY, PRIMARY_DARK, SECONDARY, SUCCESS, INFO, WARNING, DANGER, DISABLED,
    WHITE, DARK, MUTED;

    @Override
    public String getMarkupClass() {
        switch(this){
            case PRIMARY:
                return "primary";
            case PRIMARY_DARK:
                return "primary-dark";
            case SECONDARY:
                return "secondary";
            case SUCCESS:
                return "success";
            case INFO:
                return "info";
            case WARNING:
                return "warning";
            case DANGER:
                return "danger";
            case DISABLED:
                return "disabled";
            case WHITE:
                return "white";
            case DARK:
                return "dark";
            case MUTED:
                return "muted";
            default:
                return "default";
        }
    }
}
