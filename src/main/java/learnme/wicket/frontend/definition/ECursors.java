package learnme.wicket.frontend.definition;

import learnme.wicket.interfaces.IMarkupClasses;

public enum ECursors implements IMarkupClasses{
	DEFAULT, POINTER;
	
	@Override
	public String getMarkupClass() {
		 switch(this){
	         case DEFAULT:
	             return "c-default";
	         case POINTER:
	             return "c-pointer";
	         default:
	             return null;
	     }
	}

}
