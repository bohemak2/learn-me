package learnme.wicket.frontend.definition;

import learnme.wicket.interfaces.IMarkupClasses;


public enum EDirection implements IMarkupClasses{
    TOP, RIGHT, BOTTOM, LEFT,
    VERTICAL, HORIZONTAL;

    @Override
    public String getMarkupClass() {
        switch(this){    
            case TOP:
                return "t";
            case RIGHT:
                return "r";
            case BOTTOM:
                return "b";
            case LEFT:
                return "l";
            case VERTICAL:
                return "y";
            case HORIZONTAL:
                return "x";
            default:
                return null;
        }
    }
    
}
