package learnme.wicket.frontend.definition;

import learnme.wicket.frontend.Template;
import learnme.wicket.interfaces.IMarkupClasses;


public enum ESpacing implements IMarkupClasses{
    PADDING, MARGIN;

    @Override
    public String getMarkupClass() {
        switch(this){
            case PADDING:
                return "p";
            case MARGIN:
                return "m";
            default:
                return null;
        }
    }
        
    public String getMarkupClass(int column) {
        if(column > 0 && column <= Template.SPACING_COUNT){
            return getMarkupClass() +  "-" + column;
        }
        return null;
    }

    public String getMarkupClass(int column, EDirection direction) {
        if(column > 0 && column <= Template.SPACING_COUNT){
            return getMarkupClass() + direction.getMarkupClass() +  "-" + column;
        }
        return null;
    }
}
