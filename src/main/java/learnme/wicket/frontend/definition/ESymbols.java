package learnme.wicket.frontend.definition;

import learnme.wicket.interfaces.IIcon;

public enum ESymbols implements IIcon {
    AGENDA,
    ALARM_CLOCK,
    ALERT,
    ALIGN_CENTER,
    ALIGN_JUSTIFY,
    ALIGN_LEFT,
    ALIGN_RIGHT,
    ANDROID,
    ANGLE_DOUBLE_DOWN,
    ANGLE_DOUBLE_LEFT,
    ANGLE_DOUBLE_RIGHT,
    ANGLE_DOUBLE_UP,
    ANGLE_DOWN,
    ANGLE_LEFT,
    ANGLE_RIGHT,
    ANGLE_UP,
    ANCHOR,
    ANNOUNCEMENT,
    APPLE,
    ARCHIVE,
    ARROW_CIRCLE_DOWN,
    ARROW_CIRCLE_LEFT,
    ARROW_CIRCLE_RIGHT,
    ARROW_CIRCLE_UP,
    ARROW_DOWN,
    ARROW_LEFT,
    ARROW_RIGHT,
    ARROW_TOP_LEFT,
    ARROW_TOP_RIGHT,
    ARROW_UP,
    ARROWS_CORNER,
    ARROWS_HORIZONTAL,
    ARROWS_VERTICAL,
    BACK_LEFT,
    BACK_RIGHT,
    BAG,
    BAR_CHART_ALT,
    BAR_CHART,
    BASKETBALL,
    BELL,
    BLACKBOARD,
    BOLT_ALT,
    BOLT,
    BOOK,
    BOOKMARK_ALT,
    BOOKMARK,
    BRIEFCASE,
    BRUSH_ALT,
    BRUSH,
    CALENDAR,
    CAMERA,
    CAR,
    CLIP,
    CLIPBOARD,
    CLOSE,
    CLOUD_DOWN,
    CLOUD_UP,
    CLOUD,
    COMMENT_ALT,
    COMMENT,
    COMMENTS_SMILEY,
    COMMENTS,
    CONTROL_BACKWARD,
    CONTROL_EJECT,
    CONTROL_FORWARD,
    CONTROL_PAUSE,
    CONTROL_PLAY,
    CONTROL_RECORD,
    CONTROL_SHUFFLE,
    CONTROL_SKIP_BACKWARD,
    CONTROL_SKIP_FORWARD,
    CONTROL_STOP,
    CREDIT_CARD,
    CROWN,
    CSS3,
    CUP,
    CUT,
    DASHBOARD,
    DESKTOP,
    DIRECTION_ALT,
    DIRECTION,
    DOWNLOAD,
    DRIBBBLE,
    DROPBOX_ALT,
    DROPBOX,
    DRUPAL,
    EMAIL,
    ENVELOPE,
    ERASER,
    EXCHANGE_VERTICAL,
    EXPORT,
    EYE,
    FACE_SAD,
    FACE_SMILE,
    FACEBOOK,
    FILE,
    FILES,
    FILTER,
    FLAG_ALT_2,
    FLAG_ALT,
    FLAG,
    FLICKR_ALT,
    FLICKR,
    FOLDER,
    FULLSCREEN,
    GALLERY,
    GAME,
    GIFT,
    GITHUB,
    GOOGLE,
    HAND_DRAG,
    HAND_OPEN,
    HAND_POINT_DOWN,
    HAND_POINT_LEFT,
    HAND_POINT_RIGHT,
    HAND_POINT_UP,
    HAND_STOP,
    HARDDRIVE,
    HARDDRIVES,
    HEADPHONE_ALT,
    HEADPHONE,
    HEART_BROKEN,
    HEART,
    HELP_ALT,
    HELP,
    HOME,
    HTML5,
    HUMMER,
    CHECK_BOX,
    CHECK,
    ID_BADGE,
    IMAGE,
    IMPORT,
    INFINITE,
    INFO_ALT,
    INFO,
    INK_PEN,
    INSTAGRAM,
    ITALIC,
    JOOMLA,
    JSFIDDLE,
    KEY,
    LAYERS_ALT,
    LAYERS,
    LAYOUT_ACCORDION_LIST,
    LAYOUT_ACCORDION_MERGED,
    LAYOUT_ACCORDION_SEPARATED,
    LAYOUT_COLUMN2_ALT,
    LAYOUT_COLUMN2,
    LAYOUT_COLUMN3_ALT,
    LAYOUT_COLUMN3,
    LAYOUT_COLUMN4_ALT,
    LAYOUT_COLUMN4,
    LAYOUT_CTA_BTN_LEFT,
    LAYOUT_CTA_BTN_RIGHT,
    LAYOUT_CTA_CENTER,
    LAYOUT_CTA_LEFT,
    LAYOUT_CTA_RIGHT,
    LAYOUT_GRID2_ALT,
    LAYOUT_GRID2_THUMB,
    LAYOUT_GRID2,
    LAYOUT_GRID3_ALT,
    LAYOUT_GRID3,
    LAYOUT_GRID4_ALT,
    LAYOUT_GRID4,
    LAYOUT_LINE_SOLID,
    LAYOUT_LIST_LARGE_IMAGE,
    LAYOUT_LIST_POST,
    LAYOUT_LIST_THUMB_ALT,
    LAYOUT_LIST_THUMB,
    LAYOUT_MEDIA_CENTER_ALT,
    LAYOUT_MEDIA_CENTER,
    LAYOUT_MEDIA_LEFT_ALT,
    LAYOUT_MEDIA_LEFT,
    LAYOUT_MEDIA_OVERLAY_ALT_2,
    LAYOUT_MEDIA_OVERLAY_ALT,
    LAYOUT_MEDIA_OVERLAY,
    LAYOUT_MEDIA_RIGHT_ALT,
    LAYOUT_MEDIA_RIGHT,
    LAYOUT_MENU_FULL,
    LAYOUT_MENU_SEPARATED,
    LAYOUT_MENU_V,
    LAYOUT_MENU,
    LAYOUT_PLACEHOLDER,
    LAYOUT_SIDEBAR_2,
    LAYOUT_SIDEBAR_LEFT,
    LAYOUT_SIDEBAR_NONE,
    LAYOUT_SIDEBAR_RIGHT,
    LAYOUT_SLIDER_ALT,
    LAYOUT_SLIDER,
    LAYOUT_TAB_MIN,
    LAYOUT_TAB_V,
    LAYOUT_TAB_WINDOW,
    LAYOUT_TAB,
    LAYOUT_WIDTH_DEFAULT_ALT,
    LAYOUT_WIDTH_DEFAULT,
    LAYOUT_WIDTH_FULL,
    LAYOUT,
    LIGHT_BULB,
    LINE_DASHED,
    LINE_DOTTED,
    LINE_DOUBLE,
    LINK,
    LINKEDIN,
    LINUX,
    LIST_OL,
    LIST,
    LOCATION_ARROW,
    LOCATION_PIN,
    LOCK,
    LOOP,
    MAGNET,
    MAP_ALT,
    MAP,
    MARKER_ALT,
    MARKER,
    MEDALL_ALT,
    MEDALL,
    MENU_ALT,
    MENU,
    MICROPHONE_ALT,
    MICROPHONE,
    MICROSOFT_ALT,
    MICROSOFT,
    MINUS,
    MOBILE,
    MONEY,
    MORE_ALT,
    MORE,
    MOUSE_ALT,
    MOUSE,
    MOVE,
    MUSIC_ALT,
    MUSIC,
    NA,
    NEW_WINDOW,
    NOTEPAD,
    NULL,
    PACKAGE,
    PAINT_BUCKET,
    PAINT_ROLLER,
    PALETTE,
    PANEL,
    PARAGRAPH,
    PENCIL_ALT,
    PENCIL_ALT2,
    PENCIL,
    PIE_CHART,
    PIN_ALT,
    PIN,
    PIN2,
    PINTEREST_ALT,
    PINTEREST,
    PLUG,
    PLUS,
    POWER_OFF,
    PRINTER,
    PULSE,
    QUOTE_LEFT,
    QUOTE_RIGHT,
    RECEIPT,
    REDDIT,
    RELOAD,
    ROCKET,
    RSS_ALT,
    RSS,
    RULER_ALT_2,
    RULER_ALT,
    RULER_PENCIL,
    RULER,
    SAVE_ALT,
    SAVE,
    SEARCH,
    SERVER,
    SETTINGS,
    SHARE_ALT,
    SHARE,
    SHARETHIS_ALT,
    SHARETHIS,
    SHIELD,
    SHIFT_LEFT_ALT,
    SHIFT_LEFT,
    SHIFT_RIGHT_ALT,
    SHIFT_RIGHT,
    SHINE,
    SHOPPING_CART_FULL,
    SHOPPING_CART,
    SHORTCODE,
    SIGNAL,
    SKYPE,
    SLICE,
    SMALLCAP,
    SOUNDCLOUD,
    SPLIT_H,
    SPLIT_V_ALT,
    SPLIT_V,
    SPRAY,
    STACK_OVERFLOW,
    STAMP,
    STAR,
    STATS_DOWN,
    STATS_UP,
    SUPPORT,
    TABLET,
    TAG,
    TARGET,
    TEXT,
    THEMIFY_FAVICON_ALT,
    THEMIFY_FAVICON,
    THEMIFY_LOGO,
    THOUGHT,
    THUMB_DOWN,
    THUMB_UP,
    TICKET,
    TIME,
    TIMER,
    TRASH,
    TRELLO,
    TRUCK,
    TUMBLR_ALT,
    TUMBLR,
    TWITTER_ALT,
    TWITTER,
    UNDERLINE,
    UNLINK,
    UNLOCK,
    UPLOAD,
    UPPERCASE,
    USER,
    VECTOR,
    VIDEO_CAMERA,
    VIDEO_CLAPPER,
    VIEW_GRID,
    VIEW_LIST_ALT,
    VIEW_LIST,
    VIMEO_ALT,
    VIMEO,
    VOLUME,
    WALLET,
    WAND,
    WHEELCHAIR,
    WIDGET_ALT,
    WIDGET,
    WIDGETIZED,
    WORDPRESS,
    WORLD,
    WRITE,
    YAHOO,
    YOUTUBE,
    ZIP,
    ZOOM_IN,
    ZOOM_OUT;

    private static final String ICON_PREFIX = "ti-";
    
    @Override
    public String getMarkupClass() {
        return ICON_PREFIX + getIconName().toLowerCase();
    }
    
    private String getIconName (){
        switch (this){
            case AGENDA:
                    return "AGENDA";
            case ALARM_CLOCK:
                    return "ALARM-CLOCK";
            case ALERT:
                    return "ALERT";
            case ALIGN_CENTER:
                    return "ALIGN-CENTER";
            case ALIGN_JUSTIFY:
                    return "ALIGN-JUSTIFY";
            case ALIGN_LEFT:
                    return "ALIGN-LEFT";
            case ALIGN_RIGHT:
                    return "ALIGN-RIGHT";
            case ANDROID:
                    return "ANDROID";
            case ANGLE_DOUBLE_DOWN:
                    return "ANGLE-DOUBLE-DOWN";
            case ANGLE_DOUBLE_LEFT:
                    return "ANGLE-DOUBLE-LEFT";
            case ANGLE_DOUBLE_RIGHT:
                    return "ANGLE-DOUBLE-RIGHT";
            case ANGLE_DOUBLE_UP:
                    return "ANGLE-DOUBLE-UP";
            case ANGLE_DOWN:
                    return "ANGLE-DOWN";
            case ANGLE_LEFT:
                    return "ANGLE-LEFT";
            case ANGLE_RIGHT:
                    return "ANGLE-RIGHT";
            case ANGLE_UP:
                    return "ANGLE-UP";
            case ANCHOR:
                    return "ANCHOR";
            case ANNOUNCEMENT:
                    return "ANNOUNCEMENT";
            case APPLE:
                    return "APPLE";
            case ARCHIVE:
                    return "ARCHIVE";
            case ARROW_CIRCLE_DOWN:
                    return "ARROW-CIRCLE-DOWN";
            case ARROW_CIRCLE_LEFT:
                    return "ARROW-CIRCLE-LEFT";
            case ARROW_CIRCLE_RIGHT:
                    return "ARROW-CIRCLE-RIGHT";
            case ARROW_CIRCLE_UP:
                    return "ARROW-CIRCLE-UP";
            case ARROW_DOWN:
                    return "ARROW-DOWN";
            case ARROW_LEFT:
                    return "ARROW-LEFT";
            case ARROW_RIGHT:
                    return "ARROW-RIGHT";
            case ARROW_TOP_LEFT:
                    return "ARROW-TOP-LEFT";
            case ARROW_TOP_RIGHT:
                    return "ARROW-TOP-RIGHT";
            case ARROW_UP:
                    return "ARROW-UP";
            case ARROWS_CORNER:
                    return "ARROWS-CORNER";
            case ARROWS_HORIZONTAL:
                    return "ARROWS-HORIZONTAL";
            case ARROWS_VERTICAL:
                    return "ARROWS-VERTICAL";
            case BACK_LEFT:
                    return "BACK-LEFT";
            case BACK_RIGHT:
                    return "BACK-RIGHT";
            case BAG:
                    return "BAG";
            case BAR_CHART_ALT:
                    return "BAR-CHART-ALT";
            case BAR_CHART:
                    return "BAR-CHART";
            case BASKETBALL:
                    return "BASKETBALL";
            case BELL:
                    return "BELL";
            case BLACKBOARD:
                    return "BLACKBOARD";
            case BOLT_ALT:
                    return "BOLT-ALT";
            case BOLT:
                    return "BOLT";
            case BOOK:
                    return "BOOK";
            case BOOKMARK_ALT:
                    return "BOOKMARK-ALT";
            case BOOKMARK:
                    return "BOOKMARK";
            case BRIEFCASE:
                    return "BRIEFCASE";
            case BRUSH_ALT:
                    return "BRUSH-ALT";
            case BRUSH:
                    return "BRUSH";
            case CALENDAR:
                    return "CALENDAR";
            case CAMERA:
                    return "CAMERA";
            case CAR:
                    return "CAR";
            case CLIP:
                    return "CLIP";
            case CLIPBOARD:
                    return "CLIPBOARD";
            case CLOSE:
                    return "CLOSE";
            case CLOUD_DOWN:
                    return "CLOUD-DOWN";
            case CLOUD_UP:
                    return "CLOUD-UP";
            case CLOUD:
                    return "CLOUD";
            case COMMENT_ALT:
                    return "COMMENT-ALT";
            case COMMENT:
                    return "COMMENT";
            case COMMENTS_SMILEY:
                    return "COMMENTS-SMILEY";
            case COMMENTS:
                    return "COMMENTS";
            case CONTROL_BACKWARD:
                    return "CONTROL-BACKWARD";
            case CONTROL_EJECT:
                    return "CONTROL-EJECT";
            case CONTROL_FORWARD:
                    return "CONTROL-FORWARD";
            case CONTROL_PAUSE:
                    return "CONTROL-PAUSE";
            case CONTROL_PLAY:
                    return "CONTROL-PLAY";
            case CONTROL_RECORD:
                    return "CONTROL-RECORD";
            case CONTROL_SHUFFLE:
                    return "CONTROL-SHUFFLE";
            case CONTROL_SKIP_BACKWARD:
                    return "CONTROL-SKIP-BACKWARD";
            case CONTROL_SKIP_FORWARD:
                    return "CONTROL-SKIP-FORWARD";
            case CONTROL_STOP:
                    return "CONTROL-STOP";
            case CREDIT_CARD:
                    return "CREDIT-CARD";
            case CROWN:
                    return "CROWN";
            case CSS3:
                    return "CSS3";
            case CUP:
                    return "CUP";
            case CUT:
                    return "CUT";
            case DASHBOARD:
                    return "DASHBOARD";
            case DESKTOP:
                    return "DESKTOP";
            case DIRECTION_ALT:
                    return "DIRECTION-ALT";
            case DIRECTION:
                    return "DIRECTION";
            case DOWNLOAD:
                    return "DOWNLOAD";
            case DRIBBBLE:
                    return "DRIBBBLE";
            case DROPBOX_ALT:
                    return "DROPBOX-ALT";
            case DROPBOX:
                    return "DROPBOX";
            case DRUPAL:
                    return "DRUPAL";
            case EMAIL:
                    return "EMAIL";
            case ENVELOPE:
                    return "ENVELOPE";
            case ERASER:
                    return "ERASER";
            case EXCHANGE_VERTICAL:
                    return "EXCHANGE-VERTICAL";
            case EXPORT:
                    return "EXPORT";
            case EYE:
                    return "EYE";
            case FACE_SAD:
                    return "FACE-SAD";
            case FACE_SMILE:
                    return "FACE-SMILE";
            case FACEBOOK:
                    return "FACEBOOK";
            case FILE:
                    return "FILE";
            case FILES:
                    return "FILES";
            case FILTER:
                    return "FILTER";
            case FLAG_ALT_2:
                    return "FLAG-ALT-2";
            case FLAG_ALT:
                    return "FLAG-ALT";
            case FLAG:
                    return "FLAG";
            case FLICKR_ALT:
                    return "FLICKR-ALT";
            case FLICKR:
                    return "FLICKR";
            case FOLDER:
                    return "FOLDER";
            case FULLSCREEN:
                    return "FULLSCREEN";
            case GALLERY:
                    return "GALLERY";
            case GAME:
                    return "GAME";
            case GIFT:
                    return "GIFT";
            case GITHUB:
                    return "GITHUB";
            case GOOGLE:
                    return "GOOGLE";
            case HAND_DRAG:
                    return "HAND-DRAG";
            case HAND_OPEN:
                    return "HAND-OPEN";
            case HAND_POINT_DOWN:
                    return "HAND-POINT-DOWN";
            case HAND_POINT_LEFT:
                    return "HAND-POINT-LEFT";
            case HAND_POINT_RIGHT:
                    return "HAND-POINT-RIGHT";
            case HAND_POINT_UP:
                    return "HAND-POINT-UP";
            case HAND_STOP:
                    return "HAND-STOP";
            case HARDDRIVE:
                    return "HARDDRIVE";
            case HARDDRIVES:
                    return "HARDDRIVES";
            case HEADPHONE_ALT:
                    return "HEADPHONE-ALT";
            case HEADPHONE:
                    return "HEADPHONE";
            case HEART_BROKEN:
                    return "HEART-BROKEN";
            case HEART:
                    return "HEART";
            case HELP_ALT:
                    return "HELP-ALT";
            case HELP:
                    return "HELP";
            case HOME:
                    return "HOME";
            case HTML5:
                    return "HTML5";
            case HUMMER:
                    return "HUMMER";
            case CHECK_BOX:
                    return "CHECK-BOX";
            case CHECK:
                    return "CHECK";
            case ID_BADGE:
                    return "ID-BADGE";
            case IMAGE:
                    return "IMAGE";
            case IMPORT:
                    return "IMPORT";
            case INFINITE:
                    return "INFINITE";
            case INFO_ALT:
                    return "INFO-ALT";
            case INFO:
                    return "INFO";
            case INK_PEN:
                    return "INK-PEN";
            case INSTAGRAM:
                    return "INSTAGRAM";
            case ITALIC:
                    return "ITALIC";
            case JOOMLA:
                    return "JOOMLA";
            case JSFIDDLE:
                    return "JSFIDDLE";
            case KEY:
                    return "KEY";
            case LAYERS_ALT:
                    return "LAYERS-ALT";
            case LAYERS:
                    return "LAYERS";
            case LAYOUT_ACCORDION_LIST:
                    return "LAYOUT-ACCORDION-LIST";
            case LAYOUT_ACCORDION_MERGED:
                    return "LAYOUT-ACCORDION-MERGED";
            case LAYOUT_ACCORDION_SEPARATED:
                    return "LAYOUT-ACCORDION-SEPARATED";
            case LAYOUT_COLUMN2_ALT:
                    return "LAYOUT-COLUMN2-ALT";
            case LAYOUT_COLUMN2:
                    return "LAYOUT-COLUMN2";
            case LAYOUT_COLUMN3_ALT:
                    return "LAYOUT-COLUMN3-ALT";
            case LAYOUT_COLUMN3:
                    return "LAYOUT-COLUMN3";
            case LAYOUT_COLUMN4_ALT:
                    return "LAYOUT-COLUMN4-ALT";
            case LAYOUT_COLUMN4:
                    return "LAYOUT-COLUMN4";
            case LAYOUT_CTA_BTN_LEFT:
                    return "LAYOUT-CTA-BTN-LEFT";
            case LAYOUT_CTA_BTN_RIGHT:
                    return "LAYOUT-CTA-BTN-RIGHT";
            case LAYOUT_CTA_CENTER:
                    return "LAYOUT-CTA-CENTER";
            case LAYOUT_CTA_LEFT:
                    return "LAYOUT-CTA-LEFT";
            case LAYOUT_CTA_RIGHT:
                    return "LAYOUT-CTA-RIGHT";
            case LAYOUT_GRID2_ALT:
                    return "LAYOUT-GRID2-ALT";
            case LAYOUT_GRID2_THUMB:
                    return "LAYOUT-GRID2-THUMB";
            case LAYOUT_GRID2:
                    return "LAYOUT-GRID2";
            case LAYOUT_GRID3_ALT:
                    return "LAYOUT-GRID3-ALT";
            case LAYOUT_GRID3:
                    return "LAYOUT-GRID3";
            case LAYOUT_GRID4_ALT:
                    return "LAYOUT-GRID4-ALT";
            case LAYOUT_GRID4:
                    return "LAYOUT-GRID4";
            case LAYOUT_LINE_SOLID:
                    return "LAYOUT-LINE-SOLID";
            case LAYOUT_LIST_LARGE_IMAGE:
                    return "LAYOUT-LIST-LARGE-IMAGE";
            case LAYOUT_LIST_POST:
                    return "LAYOUT-LIST-POST";
            case LAYOUT_LIST_THUMB_ALT:
                    return "LAYOUT-LIST-THUMB-ALT";
            case LAYOUT_LIST_THUMB:
                    return "LAYOUT-LIST-THUMB";
            case LAYOUT_MEDIA_CENTER_ALT:
                    return "LAYOUT-MEDIA-CENTER-ALT";
            case LAYOUT_MEDIA_CENTER:
                    return "LAYOUT-MEDIA-CENTER";
            case LAYOUT_MEDIA_LEFT_ALT:
                    return "LAYOUT-MEDIA-LEFT-ALT";
            case LAYOUT_MEDIA_LEFT:
                    return "LAYOUT-MEDIA-LEFT";
            case LAYOUT_MEDIA_OVERLAY_ALT_2:
                    return "LAYOUT-MEDIA-OVERLAY-ALT-2";
            case LAYOUT_MEDIA_OVERLAY_ALT:
                    return "LAYOUT-MEDIA-OVERLAY-ALT";
            case LAYOUT_MEDIA_OVERLAY:
                    return "LAYOUT-MEDIA-OVERLAY";
            case LAYOUT_MEDIA_RIGHT_ALT:
                    return "LAYOUT-MEDIA-RIGHT-ALT";
            case LAYOUT_MEDIA_RIGHT:
                    return "LAYOUT-MEDIA-RIGHT";
            case LAYOUT_MENU_FULL:
                    return "LAYOUT-MENU-FULL";
            case LAYOUT_MENU_SEPARATED:
                    return "LAYOUT-MENU-SEPARATED";
            case LAYOUT_MENU_V:
                    return "LAYOUT-MENU-V";
            case LAYOUT_MENU:
                    return "LAYOUT-MENU";
            case LAYOUT_PLACEHOLDER:
                    return "LAYOUT-PLACEHOLDER";
            case LAYOUT_SIDEBAR_2:
                    return "LAYOUT-SIDEBAR-2";
            case LAYOUT_SIDEBAR_LEFT:
                    return "LAYOUT-SIDEBAR-LEFT";
            case LAYOUT_SIDEBAR_NONE:
                    return "LAYOUT-SIDEBAR-NONE";
            case LAYOUT_SIDEBAR_RIGHT:
                    return "LAYOUT-SIDEBAR-RIGHT";
            case LAYOUT_SLIDER_ALT:
                    return "LAYOUT-SLIDER-ALT";
            case LAYOUT_SLIDER:
                    return "LAYOUT-SLIDER";
            case LAYOUT_TAB_MIN:
                    return "LAYOUT-TAB-MIN";
            case LAYOUT_TAB_V:
                    return "LAYOUT-TAB-V";
            case LAYOUT_TAB_WINDOW:
                    return "LAYOUT-TAB-WINDOW";
            case LAYOUT_TAB:
                    return "LAYOUT-TAB";
            case LAYOUT_WIDTH_DEFAULT_ALT:
                    return "LAYOUT-WIDTH-DEFAULT-ALT";
            case LAYOUT_WIDTH_DEFAULT:
                    return "LAYOUT-WIDTH-DEFAULT";
            case LAYOUT_WIDTH_FULL:
                    return "LAYOUT-WIDTH-FULL";
            case LAYOUT:
                    return "LAYOUT";
            case LIGHT_BULB:
                    return "LIGHT-BULB";
            case LINE_DASHED:
                    return "LINE-DASHED";
            case LINE_DOTTED:
                    return "LINE-DOTTED";
            case LINE_DOUBLE:
                    return "LINE-DOUBLE";
            case LINK:
                    return "LINK";
            case LINKEDIN:
                    return "LINKEDIN";
            case LINUX:
                    return "LINUX";
            case LIST_OL:
                    return "LIST-OL";
            case LIST:
                    return "LIST";
            case LOCATION_ARROW:
                    return "LOCATION-ARROW";
            case LOCATION_PIN:
                    return "LOCATION-PIN";
            case LOCK:
                    return "LOCK";
            case LOOP:
                    return "LOOP";
            case MAGNET:
                    return "MAGNET";
            case MAP_ALT:
                    return "MAP-ALT";
            case MAP:
                    return "MAP";
            case MARKER_ALT:
                    return "MARKER-ALT";
            case MARKER:
                    return "MARKER";
            case MEDALL_ALT:
                    return "MEDALL-ALT";
            case MEDALL:
                    return "MEDALL";
            case MENU_ALT:
                    return "MENU-ALT";
            case MENU:
                    return "MENU";
            case MICROPHONE_ALT:
                    return "MICROPHONE-ALT";
            case MICROPHONE:
                    return "MICROPHONE";
            case MICROSOFT_ALT:
                    return "MICROSOFT-ALT";
            case MICROSOFT:
                    return "MICROSOFT";
            case MINUS:
                    return "MINUS";
            case MOBILE:
                    return "MOBILE";
            case MONEY:
                    return "MONEY";
            case MORE_ALT:
                    return "MORE-ALT";
            case MORE:
                    return "MORE";
            case MOUSE_ALT:
                    return "MOUSE-ALT";
            case MOUSE:
                    return "MOUSE";
            case MOVE:
                    return "MOVE";
            case MUSIC_ALT:
                    return "MUSIC-ALT";
            case MUSIC:
                    return "MUSIC";
            case NA:
                    return "NA";
            case NEW_WINDOW:
                    return "NEW-WINDOW";
            case NOTEPAD:
                    return "NOTEPAD";
            case NULL:
                    return "NULL";
            case PACKAGE:
                    return "PACKAGE";
            case PAINT_BUCKET:
                    return "PAINT-BUCKET";
            case PAINT_ROLLER:
                    return "PAINT-ROLLER";
            case PALETTE:
                    return "PALETTE";
            case PANEL:
                    return "PANEL";
            case PARAGRAPH:
                    return "PARAGRAPH";
            case PENCIL_ALT:
                    return "PENCIL-ALT";
            case PENCIL_ALT2:
                    return "PENCIL-ALT2";
            case PENCIL:
                    return "PENCIL";
            case PIE_CHART:
                    return "PIE-CHART";
            case PIN_ALT:
                    return "PIN-ALT";
            case PIN:
                    return "PIN";
            case PIN2:
                    return "PIN2";
            case PINTEREST_ALT:
                    return "PINTEREST-ALT";
            case PINTEREST:
                    return "PINTEREST";
            case PLUG:
                    return "PLUG";
            case PLUS:
                    return "PLUS";
            case POWER_OFF:
                    return "POWER-OFF";
            case PRINTER:
                    return "PRINTER";
            case PULSE:
                    return "PULSE";
            case QUOTE_LEFT:
                    return "QUOTE-LEFT";
            case QUOTE_RIGHT:
                    return "QUOTE-RIGHT";
            case RECEIPT:
                    return "RECEIPT";
            case REDDIT:
                    return "REDDIT";
            case RELOAD:
                    return "RELOAD";
            case ROCKET:
                    return "ROCKET";
            case RSS_ALT:
                    return "RSS-ALT";
            case RSS:
                    return "RSS";
            case RULER_ALT_2:
                    return "RULER-ALT-2";
            case RULER_ALT:
                    return "RULER-ALT";
            case RULER_PENCIL:
                    return "RULER-PENCIL";
            case RULER:
                    return "RULER";
            case SAVE_ALT:
                    return "SAVE-ALT";
            case SAVE:
                    return "SAVE";
            case SEARCH:
                    return "SEARCH";
            case SERVER:
                    return "SERVER";
            case SETTINGS:
                    return "SETTINGS";
            case SHARE_ALT:
                    return "SHARE-ALT";
            case SHARE:
                    return "SHARE";
            case SHARETHIS_ALT:
                    return "SHARETHIS-ALT";
            case SHARETHIS:
                    return "SHARETHIS";
            case SHIELD:
                    return "SHIELD";
            case SHIFT_LEFT_ALT:
                    return "SHIFT-LEFT-ALT";
            case SHIFT_LEFT:
                    return "SHIFT-LEFT";
            case SHIFT_RIGHT_ALT:
                    return "SHIFT-RIGHT-ALT";
            case SHIFT_RIGHT:
                    return "SHIFT-RIGHT";
            case SHINE:
                    return "SHINE";
            case SHOPPING_CART_FULL:
                    return "SHOPPING-CART-FULL";
            case SHOPPING_CART:
                    return "SHOPPING-CART";
            case SHORTCODE:
                    return "SHORTCODE";
            case SIGNAL:
                    return "SIGNAL";
            case SKYPE:
                    return "SKYPE";
            case SLICE:
                    return "SLICE";
            case SMALLCAP:
                    return "SMALLCAP";
            case SOUNDCLOUD:
                    return "SOUNDCLOUD";
            case SPLIT_H:
                    return "SPLIT-H";
            case SPLIT_V_ALT:
                    return "SPLIT-V-ALT";
            case SPLIT_V:
                    return "SPLIT-V";
            case SPRAY:
                    return "SPRAY";
            case STACK_OVERFLOW:
                    return "STACK-OVERFLOW";
            case STAMP:
                    return "STAMP";
            case STAR:
                    return "STAR";
            case STATS_DOWN:
                    return "STATS-DOWN";
            case STATS_UP:
                    return "STATS-UP";
            case SUPPORT:
                    return "SUPPORT";
            case TABLET:
                    return "TABLET";
            case TAG:
                    return "TAG";
            case TARGET:
                    return "TARGET";
            case TEXT:
                    return "TEXT";
            case THEMIFY_FAVICON_ALT:
                    return "THEMIFY-FAVICON-ALT";
            case THEMIFY_FAVICON:
                    return "THEMIFY-FAVICON";
            case THEMIFY_LOGO:
                    return "THEMIFY-LOGO";
            case THOUGHT:
                    return "THOUGHT";
            case THUMB_DOWN:
                    return "THUMB-DOWN";
            case THUMB_UP:
                    return "THUMB-UP";
            case TICKET:
                    return "TICKET";
            case TIME:
                    return "TIME";
            case TIMER:
                    return "TIMER";
            case TRASH:
                    return "TRASH";
            case TRELLO:
                    return "TRELLO";
            case TRUCK:
                    return "TRUCK";
            case TUMBLR_ALT:
                    return "TUMBLR-ALT";
            case TUMBLR:
                    return "TUMBLR";
            case TWITTER_ALT:
                    return "TWITTER-ALT";
            case TWITTER:
                    return "TWITTER";
            case UNDERLINE:
                    return "UNDERLINE";
            case UNLINK:
                    return "UNLINK";
            case UNLOCK:
                    return "UNLOCK";
            case UPLOAD:
                    return "UPLOAD";
            case UPPERCASE:
                    return "UPPERCASE";
            case USER:
                    return "USER";
            case VECTOR:
                    return "VECTOR";
            case VIDEO_CAMERA:
                    return "VIDEO-CAMERA";
            case VIDEO_CLAPPER:
                    return "VIDEO-CLAPPER";
            case VIEW_GRID:
                    return "VIEW-GRID";
            case VIEW_LIST_ALT:
                    return "VIEW-LIST-ALT";
            case VIEW_LIST:
                    return "VIEW-LIST";
            case VIMEO_ALT:
                    return "VIMEO-ALT";
            case VIMEO:
                    return "VIMEO";
            case VOLUME:
                    return "VOLUME";
            case WALLET:
                    return "WALLET";
            case WAND:
                    return "WAND";
            case WHEELCHAIR:
                    return "WHEELCHAIR";
            case WIDGET_ALT:
                    return "WIDGET-ALT";
            case WIDGET:
                    return "WIDGET";
            case WIDGETIZED:
                    return "WIDGETIZED";
            case WORDPRESS:
                    return "WORDPRESS";
            case WORLD:
                    return "WORLD";
            case WRITE:
                    return "WRITE";
            case YAHOO:
                    return "YAHOO";
            case YOUTUBE:
                    return "YOUTUBE";
            case ZIP:
                    return "ZIP";
            case ZOOM_IN:
                    return "ZOOM-IN";
            case ZOOM_OUT:
                    return "ZOOM-OUT";
            default: 
                return null;
            }
    }

}
