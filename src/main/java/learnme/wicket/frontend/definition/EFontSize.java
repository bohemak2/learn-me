package learnme.wicket.frontend.definition;

import learnme.wicket.interfaces.IMarkupClasses;

public enum EFontSize implements IMarkupClasses{
	SMALL, NORMAL, LARGE;
	
	@Override
    public String getMarkupClass() {
         switch(this){
            case SMALL:
                return "text-small";
            case NORMAL:
                return "text-normal";
            case LARGE:
                return "text-large";
            default:
                return null;
        }
    }
}
