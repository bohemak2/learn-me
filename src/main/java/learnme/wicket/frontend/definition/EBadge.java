package learnme.wicket.frontend.definition;

import learnme.wicket.interfaces.IMarkupClasses;

public enum EBadge implements IMarkupClasses{
    DEFAULT, PRIMARY, SUCCESS, WARNING, DANGER, INFO; 

    @Override
    public String getMarkupClass() {
        switch(this){
            case DEFAULT:
                return "default";
            case PRIMARY:
                return "primary";
            case SUCCESS:
                return "success";
            case INFO:
                return "info";
            case WARNING:
                return "warning";
            case DANGER:
                return "danger";
            default:
                return null;
        }
    }
    
    public String getMarkupClass(boolean inverse) {        
        if (inverse){
            return "inverse-" + getMarkupClass();
        }
        return getMarkupClass();
    }   
}
