// PUBLIC STATIC -> JSON

Loader = {
	AjaxFree: true, 
	AjaxInForeground: true,
	LoaderSelector: "",
	LoaderTimer: 200,
	LoaderInterval: null,
	LoaderIntervalTime: 50,
	Calls: [],
	Srcs: {},
	
	addCall: function(markupId, url){
		obj = {
			"callable": true,
			"markupId": markupId,
			"url": url,
		};
		Loader.Calls.push(obj);
		
		return obj;
	},

	getCall: function(markupId){
		for(i = 0; Loader.Calls.length > i; i++){
			if (Loader.Calls[i].markupId == markupId){
				return Loader.Calls[i];
			}
		};
	},

	showLoader: function(){
		if (Loader.LoaderInterval == undefined){
			var counter = 0;
	
			Loader.LoaderInterval = setInterval(function(){
				if (counter > 1 && !Loader.AjaxFree){
					$(Loader.LoaderSelector).fadeIn(Loader.LoaderTimer);
					
					Loader.clearInterval();
				}else{
					counter++;
					if (counter > 20){
						Loader.clearInterval();
					}
				}
			}, Loader.LoaderIntervalTime);
		}
	},
	
	showLoaderAnyway: function(){
		$(Loader.LoaderSelector).fadeIn(Loader.LoaderTimer);
	},
	
	hideLoader: function(){
		$(Loader.LoaderSelector).fadeOut(Loader.LoaderTimer);
	},
	
	onInitialize: function(LoaderSelector){
		Loader.LoaderSelector = LoaderSelector;
		
		$(window).bind('beforeunload', Loader.showLoaderAnyway);
		
		Wicket.Event.subscribe('/ajax/call/beforeSend', function(qEvent, attributes, jqXHR, errorThrown){
			Loader.AjaxFree = false;
			call = Loader.getCall(attributes.c);
			
			if (Loader.AjaxInForeground && (call == undefined || call.callable)){				
				Loader.showLoader();
			} else {
				Loader.AjaxInForeground = true;
			}
		});
		
		Wicket.Event.subscribe('/ajax/call/complete', function(qEvent, attributes, jqXHR, errorThrown){
			setTimeout(function(){
				call = Loader.getCall(attributes.c);
				
				if(call != undefined){
					if(jqXHR.responseXML.children[0].childElementCount == 0){
						call.callable = false;
					} else{
						if (!call.callable){
							call.callable = true;
						}
					}
				}
				
				Loader.AjaxFree = true;
				Loader.hideLoader();
			}, 25);
		}); 
	},
	
	clearInterval: function(){
		clearInterval(Loader.LoaderInterval);
		Loader.LoaderInterval = undefined;
	}
}