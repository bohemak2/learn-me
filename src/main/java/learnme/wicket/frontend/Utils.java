package learnme.wicket.frontend;


public class Utils {
    
    public static String addClass(String classes, String addedClass){
    	if (addedClass == null || addedClass.length() == 0) {
    		return classes;
    	}

    	if(classes == null) {
    		classes = "";
    	} 
    	
    	return replaceClass(classes, null, addedClass);
    }
    
    public static String replaceClass(String classes, String oldClass, String newClass){
    	if (classes == null) {
    		return "";
    	}
    	
    	if (newClass == null) {
    		return classes;
    	}
    	
    	if (oldClass == null) {
    		// TODO: THERE WILL BE ONE TEST, LIKE BELOW - is it real class (not just contains) - first, last...?
    		if (!classes.contains(" " + newClass + " ")) {
    			return classes + " " + newClass;    			
    		}
    		return classes;
    	}

    	// TODO: Just one regex
    	return classes.replaceAll(" " + oldClass + " ", " " + newClass + " ").replaceAll("^" + oldClass + "$", newClass).replaceAll("^" + oldClass + " ", newClass + " ").replaceAll(" " + oldClass + "$", " " + newClass);
    }
    
    public static String removeClass(String classes, String classToRemove) {
    	if (classes == null) {
    		return classes;
    	}
    	return classes.replaceAll(" " + classToRemove + " ", "").replaceAll("^" + classToRemove + "$", "").replaceAll("^" + classToRemove + " ", "").replaceAll(" " + classToRemove + "$", "");
    }
}
