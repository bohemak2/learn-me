package learnme.wicket.panels.general.language;

import java.util.Locale;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;

import learnme.application.AppConfig;
import learnme.wicket.components.icon.Icon;
import learnme.wicket.components.nonbasic.DropDown;
import learnme.wicket.frontend.definition.EFlags;
import learnme.wicket.interfaces.IIcon;
import learnme.wicket.panels.BasicPanel;


public class LocalePanel extends BasicPanel{
	
	private final String ID_CHOICES = "locale-choices";
    private final String ID_ITEM_LINK = "locale-item-link";
    private final String ID_ITEM_LABEL = "locale-item-label";
    private final String ID_ITEM_FLAG = "locale-item-flag";
    private final String ID_SELECTED_LABEL = "locale-selected-label";
    private final String ID_SELECTED_FLAG = "locale-selected-flag";
    
    DropDown<Locale> choices;
    
    public LocalePanel(String id) {
        super(id);
        
        choices = new DropDown<Locale>(ID_CHOICES, AppConfig.LOCALES){

			{
                setSelected(getSession().getLocale());
            }
            
            @Override
            protected void populateItem(ListItem<Locale> item) {
                item.add(new AjaxLink<String>(ID_ITEM_LINK) {
                    /**
					 * 
					 */
					private static final long serialVersionUID = 1L;

					{
                        add(new Label(ID_ITEM_LABEL, new Model<String>(item.getModel().getObject().getDisplayLanguage())));
                        add(new Icon(ID_ITEM_FLAG, new Model<IIcon>(EFlags.getFlagByLocale(item.getModel().getObject()))));
                    }
                    
                    @Override
                    public void onClick(AjaxRequestTarget target) {
                    	choices.setSelected(item.getModelObject());
                        getSession().setLocale(item.getModel().getObject());
                        setResponsePage(getPage());
                    }
                });
            }
        };
        
        if (AppConfig.LOCALES.size() < 2){
            choices.setVisible(false);
        }
     
        add(new Label(ID_SELECTED_LABEL, new PropertyModel(choices, "getSelected().getDisplayLanguage")));
     
        add(new Icon(ID_SELECTED_FLAG, new Model<IIcon>(){
			@Override
            public IIcon getObject() {
                return EFlags.getFlagByLocale(((Locale)choices.getSelected()));
            }            
        }));
        
        add(choices);
    }
}
