package learnme.wicket.panels.general.header;

import learnme.wicket.panels.BasicPanel;

import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.request.resource.ContextRelativeResource;

import learnme.wicket.pages.BasicPage;
import learnme.wicket.panels.general.language.LocalePanel;

public class HeaderPanel extends BasicPanel{

	private static final String ID_HOME_LINK = "home-link";
	private static final String ID_HOME_LOGO = "home-logo";
	private static final String ID_HOME_LANGUAGE = "home-language";
	
	public HeaderPanel(String id) {
        super(id);
        
        add(new Link<Image>(ID_HOME_LINK) {
        	
        	{
        		add(new Image(ID_HOME_LOGO, new ContextRelativeResource("img/logo.png")));
        	}

			@Override
			public void onClick() {
				setResponsePage(((BasicPage)getPage()).getLogoLinkPage());
			}
		});      
        
        add(new LocalePanel(ID_HOME_LANGUAGE));
    }
}
