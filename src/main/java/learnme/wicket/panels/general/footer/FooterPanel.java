package learnme.wicket.panels.general.footer;

import learnme.wicket.components.link.Link;
import learnme.wicket.pages.general.about.AboutPage;
import learnme.wicket.pages.general.contact.ContactPage;
import learnme.wicket.panels.BasicPanel;

import java.util.Optional;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.model.ResourceModel;


public class FooterPanel extends BasicPanel{
	private final static String ID_LINK_ABOUT = "about";
	private final static String ID_LINK_CONTACT = "contact";

	public FooterPanel(String id) {
        super(id);
        
        add(new Link(ID_LINK_ABOUT, new ResourceModel("about")) {

			@Override
			public void onClick(Optional<AjaxRequestTarget> target) {
				setResponsePage(AboutPage.class);				
			}
        });
    
    
        add(new Link(ID_LINK_CONTACT, new ResourceModel("contact")) {

			@Override
			public void onClick(Optional<AjaxRequestTarget> target) {			
				setResponsePage(ContactPage.class);
			}
        });
    }

}
