package learnme.wicket.panels;

import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import learnme.wicket.interfaces.IBasicPanel;


abstract public class BasicPanel extends Panel implements IBasicPanel{
    
    public BasicPanel(String id) {
        this(id, null);
    }
    
    public BasicPanel(String id, IModel<?> model) {
        super(id, model);
        
        setOutputMarkupId(true);
    }

}
