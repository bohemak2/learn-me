package learnme.wicket.panels.application.content;

import learnme.wicket.components.EmptyComponent;
import learnme.wicket.components.label.Label;
import learnme.wicket.components.label.LabelHeader;
import learnme.wicket.frontend.Template;
import learnme.wicket.panels.BasicPanel;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.model.IModel;


public class ContentPanel extends BasicPanel{
	public final static String ID_HEADER_WRAPPER = "header-wrapper";
    public final static String ID_HEADER = "header";
    public final static String ID_DESCRIPTION = "description";
    
    public final static String ID_BODY = "body";
    
    Component header;
    IModel description;
    
    Component body;

    public ContentPanel(String id) {
        super(id);
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        
        add(new WebMarkupContainer(ID_HEADER_WRAPPER) {
        	{
        		if (header != null){
	        		add(header);
	        	} else {
	        		add(new EmptyComponent(ID_HEADER));
	        	}
	        	
	        	if (description != null){
	        		add(new Label(ID_DESCRIPTION, description));
	        	} else {
	        		add(new EmptyComponent(ID_DESCRIPTION));
	        	}
	        	
	        	if(header == null && description == null) {
	        		setVisible(false);
	        	}
        	}
        });        
    }


    @Override
    protected void onConfigure() {
    	super.onConfigure();
        if (body == null){
            add(body = new EmptyComponent(ID_BODY));
        }    	
    }
    
    public Component getHeader() {
        return header;
    }

    public void clearHeader() {
    	this.header = null;
    }

    public void setHeader(Component header) {
        this.header = header;
    }
    
    public void setHeader(IModel header) {
        this.header = new LabelHeader(ID_HEADER, header, 4);
    }

    public IModel getDescription() {
        return description;
    }

    public void setDescription(IModel description) {
        this.description = description;
    }

    public Component getBody() {
        return body;
    }

    public void setBody(BasicPanel body) {
        setBody(body, null);
    }
    
    public void setBody(BasicPanel body, AjaxRequestTarget target) {
//    	if (this.body != null) {
//	    	remove(this.body);
//    	}
    	if (this.body != null) {
    		remove(this.body);
    	}
    	this.body = body;
    	if (this.body != null) {
    		add(this.body);
    	}
    	
//    	if (target != null) {
//	    	target.prependJavaScript(Template.showLoaderJS());
//	    	target.add(this.body);
//	    	target.prependJavaScript(Template.hideLoaderJS());
//    	}
    }
    
}
