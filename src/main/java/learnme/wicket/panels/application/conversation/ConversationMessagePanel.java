package learnme.wicket.panels.application.conversation;

import java.util.Optional;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.resource.ContextRelativeResource;

import learnme.hibernate.entities.Message;
import learnme.wicket.components.label.Label;
import learnme.wicket.components.label.LabelHeader;
import learnme.wicket.components.link.Link;
import learnme.wicket.interfaces.behaviours.IClickable;
import learnme.wicket.pages.application.users.UserPage;
import learnme.wicket.panels.BasicPanel;

public class ConversationMessagePanel extends BasicPanel implements IClickable{
	
	private final static String ID_USER_IMAGE = "user-image";
    private final static String ID_USER_FULLNAME = "user-fullname";
    
    private final static String ID_POST_TEXT = "post-text";
    private final static String ID_POST_SENT = "post-sent";

	private Message message;
	
	public ConversationMessagePanel(String id, Message message) {
		super(id);
		
		this.message = message;
	}
	
	@Override
	protected void onInitialize() {
		super.onInitialize();
		
		add(new Image(ID_USER_IMAGE, new ContextRelativeResource(message.getUser().getImage())));
		add(new Link(ID_USER_FULLNAME, new LabelHeader(Link.ID_LINK_CONTENT, new PropertyModel<String>(message.getUser(), "getNameAndSurname"), 5)) {
			
			@Override
			public void onClick(Optional<AjaxRequestTarget> target) {
				setResponsePage(new UserPage(getPage().getPageParameters(), message.getUser()));
			}
		});
		
		add(new Label(ID_POST_TEXT, new PropertyModel<String>(message, "getText")));
		add(new Label(ID_POST_SENT, new Model<String>(message.getSent().toGMTString())));
	}

	@Override
	public void onClick(Optional<AjaxRequestTarget> target) {
		setResponsePage(new UserPage(getPage().getPageParameters(), message.getUser()));
	}

	@Override
	public boolean isDisabled() {
		return false;
	}

}
