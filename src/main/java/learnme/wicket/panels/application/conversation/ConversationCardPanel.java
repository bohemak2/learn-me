package learnme.wicket.panels.application.conversation;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.model.StringResourceModel;

import learnme.hibernate.entities.Conversation;
import learnme.hibernate.entities.Message;
import learnme.hibernate.entities.User;
import learnme.hibernate.enums.EEntityLoaderType;
import learnme.hibernate.utils.EntityLoader;
import learnme.wicket.components.EmptyComponent;
import learnme.wicket.components.button.Button;
import learnme.wicket.components.card.Card;
import learnme.wicket.components.layout.VerticalLayout;
import learnme.wicket.components.link.Link;
import learnme.wicket.frontend.definition.EAligment;
import learnme.wicket.frontend.definition.EColors;
import learnme.wicket.pages.application.ApplicationPage;
import learnme.wicket.pages.application.posts.PostPage;
import learnme.wicket.panels.BasicPanel;

public class ConversationCardPanel extends BasicPanel {

	private static final String ID_CONVERSATION_WRAPPER = "conversation-wrapper";
	private static final String ID_CONVERSATION_REACT_BUTTON = "conversation-react-button";
	
	private static final int LIMIT = 2;

	private Conversation conversation;
	private boolean showComments;
	private boolean all;
	
	
	private Message firstMessage;
	private VerticalLayout list;
	
	private EntityLoader<Message> loader;
	
	public ConversationCardPanel(String id, Conversation conversation) {
		this(id, conversation, false, false, LIMIT);
	}

	public ConversationCardPanel(String id, Conversation conversation, boolean previewNewestMessage, boolean showComments) {
		this(id, conversation, previewNewestMessage, showComments, LIMIT);
	}
	
	public ConversationCardPanel(String id, Conversation conversation, boolean previewNewestMessage, boolean showComments, int limit) {
		super(id);
		
		this.conversation = conversation;
		this.showComments = showComments;

		firstMessage = conversation.getConversationFirstMessage(previewNewestMessage);
		list = new VerticalLayout(Card.ID_CONTENT) {
			{
				if (previewNewestMessage) {
					setReversed(true);					
				}
			}
		};
		
		if (showComments) {			
			list.add(loader = new EntityLoader<Message>(conversation.getConversationMessages(firstMessage), true) {
				
				{
// TODO: solve 		setLoaderString(new ResourceModel("MessageBefore").toString());
					setLoaderStringBefore(true);
					setLoaderType(EEntityLoaderType.CLICK);
					setLimit(limit);
				}
				
				@Override
				public Component renderComponent(LinkedHashSet<Message> messages) {
					if(messages.size() > 0) {
						fillMessages(messages);
						return list;
					}
					return null;
				}			
			});
		}
		
	}
	
	@Override
	protected void onInitialize() {
		super.onInitialize();
        		
		add(new Card(ID_CONVERSATION_WRAPPER, list, new ConversationMessagePanel(Card.ID_HEADER, firstMessage)));
		
		if (showComments) {
			fillMessages(loader.loadNext());
			
			add(new Button(ID_CONVERSATION_REACT_BUTTON, new ResourceModel("ReactButton"), true) {
					
					User me;
					Set<User> participants;
					
				{
					setBackgroundColor(EColors.SUCCESS);
					setAlignment(EAligment.BLOCK_CENTER);
					
					if (ConversationCardPanel.this.getPage() instanceof ApplicationPage) {
						me = ((ApplicationPage)ConversationCardPanel.this.getPage()).getUser();
					} else {
						setVisible(false);
					}
					
					Set<User> participants = conversation.getUsers();
					
					if (conversation.isMessage() && !conversation.getUsers().contains(me)) {
						setVisible(false);
					}
					
				}

				@Override
				public void onClick(Optional<AjaxRequestTarget> target) {
								   
						if (!conversation.isMessage() && !conversation.getUsers().contains(me)) {
						    conversation.addUser(me);
						}
	
						Message message = new Message();
						message.setUser(me);
						message.setText("REACT by BUTTON");
						conversation.addMessage(message);
						
						conversation.persist();
						
						setResponsePage(new PostPage(getPage().getPageParameters(), conversation));
					
				}
				
			});
		} else {
			list.add(new Link(list.newId(),  new StringResourceModel("ShowPost", new Model<Conversation>(conversation))) {
				
				@Override
				public void onClick(Optional<AjaxRequestTarget> target) {
					setResponsePage(new PostPage(getPage().getPageParameters(), conversation));	
				}
			});
			
			add(new EmptyComponent(ID_CONVERSATION_REACT_BUTTON));
		}
	}
	
	private void fillMessages(HashSet<Message> conversations) {
		
		for(Message message: conversations) {
			list.add(new ConversationMessagePanel(list.newId(), message));
		}
	}

	public Conversation getConversation() {
		return conversation;
	}

	public void setConversation(Conversation conversation) {
		this.conversation = conversation;
	}

	public boolean isShowComments() {
		return showComments;
	}

	public void setShowComments(boolean showComments) {
		this.showComments = showComments;
	}

	public boolean isAll() {
		return all;
	}

	public void setAll(boolean all) {
		this.all = all;
	}
}
