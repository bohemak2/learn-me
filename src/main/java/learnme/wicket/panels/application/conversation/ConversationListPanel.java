package learnme.wicket.panels.application.conversation;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Optional;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.model.StringResourceModel;
import org.hibernate.Criteria;

import learnme.hibernate.entities.Conversation;
import learnme.hibernate.entities.Message;
import learnme.hibernate.entities.User;
import learnme.hibernate.interfaces.IMessagable;
import learnme.hibernate.interfaces.IPostable;
import learnme.hibernate.utils.EntityLoader;
import learnme.wicket.components.card.Card;
import learnme.wicket.components.label.Label;
import learnme.wicket.components.layout.BasicLayout;
import learnme.wicket.components.layout.VerticalLayout;
import learnme.wicket.components.link.Link;
import learnme.wicket.components.table.Table;
import learnme.wicket.frontend.definition.EDirection;
import learnme.wicket.frontend.definition.ESpacing;
import learnme.wicket.pages.application.ApplicationPage;
import learnme.wicket.pages.application.posts.PostPage;
import learnme.wicket.pages.application.users.UserPage;
import learnme.wicket.panels.BasicPanel;
import learnme.wicket.panels.application.user.UserLittlePreviewPanel;

public class ConversationListPanel extends BasicPanel {
	
	private enum ERender {
		CARD, TABLE;
	}
	
	private static final String ID_POST_LIST = "user-posts";
	
	private Criteria criteria;
	private VerticalLayout list;
	private Table table;
	private EntityLoader<Conversation> loader;
	
	private ERender render;
	
	private boolean previewNewestMessage;
	
	public ConversationListPanel(String id, IMessagable entity) {
		super(id);
		
		this.criteria = entity.getLastMessages();
		this.previewNewestMessage = false;
		this.render = ERender.TABLE;
	}
	
	public ConversationListPanel(String id, IPostable entity) {
		super(id);
		this.criteria = entity.getLastPosts();
		this.previewNewestMessage = true;
		this.render = ERender.CARD;
	}
	
	@Override
	protected void onInitialize() {
		super.onInitialize();

		if (render == ERender.CARD) {
			list = new VerticalLayout(ID_POST_LIST);
			list.add(loader = new EntityLoader<Conversation>(criteria, true) {
	
				@Override
				public Component renderComponent(LinkedHashSet<Conversation> conversations) {
					if(conversations.size() > 0) {
						fillPosts(conversations);
						return list;
					}
					return null;
				}			
			});
		} else if (render == ERender.TABLE) {
			table = new Table(Card.ID_CONTENT);
			table.setBorders(true);
			table.add(loader = new EntityLoader<Conversation>(criteria, true) {
	
				@Override
				public Component renderComponent(LinkedHashSet<Conversation> conversations) {
					if(conversations.size() > 0) {
						fillPosts(conversations);
						return table;
					}
					return null;
				}			
			});
			table.setHeadRow(new ResourceModel("ConversationListConversation"), new ResourceModel("ConversationListLastMessage"), new ResourceModel("ConversationListLastMessagePosted"));
		}
		
		HashSet<Conversation> conversations = loader.loadNext();
		
		if (!conversations.isEmpty()) {
			fillPosts(conversations);			
		} else {
			if (render == ERender.CARD){
				list.add(new Label(list.newId(), new ResourceModel("NoRecords")));
			}
		}
		
		if (render == ERender.CARD) {
			add(list);
		} else if (render == ERender.TABLE) {
			add(new Card(ID_POST_LIST, table));
		}
	}
	
	private void fillPosts(HashSet<Conversation> conversations){
		
		for(Conversation conversation : conversations) {
			if (render == ERender.CARD) {
				list.add(new ConversationCardPanel(list.newId(), conversation, previewNewestMessage, false));
			} else if (render == ERender.TABLE) {
				User me = null;
				if (getPage() instanceof ApplicationPage) {
					me = ((ApplicationPage)getPage()).getUser();
				}
				
				Message message = conversation.getConversationFirstMessage(false);
				
				BasicLayout users = new BasicLayout(Table.ID_COMPONENT) {
					{
						setFloated(true);
					}
				};
				
				if (conversation.getName() != null) {
					users.add(new Label(users.newId(), new PropertyModel<>(conversation, "getName")));
				} else {
					for (User user : conversation.getUsers()) {
						if (!user.equals(me)) {
							users.add(new UserLittlePreviewPanel(users.newId(), user, true));
						}
					}
				}
				
				table.addRow(users, new Link(Table.ID_COMPONENT, new PropertyModel(message, "getText")) {
									
					@Override
					public void onClick(Optional<AjaxRequestTarget> target) {
						setResponsePage(new PostPage(getPage().getPageParameters(), conversation));
					}
				}, new Label(Table.ID_COMPONENT, new StringResourceModel("ConversationListSender", new Model<Message>(message))));
			}
		}
	}
}
