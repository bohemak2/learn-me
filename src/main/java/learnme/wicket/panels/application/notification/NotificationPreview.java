package learnme.wicket.panels.application.notification;

import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.request.resource.ContextRelativeResource;

import learnme.hibernate.entities.Notification;
import learnme.wicket.components.EmptyComponent;
import learnme.wicket.components.icon.Icon;
import learnme.wicket.components.label.Label;
import learnme.wicket.frontend.definition.EColors;
import learnme.wicket.frontend.definition.ESymbols;
import learnme.wicket.pages.application.notifications.NotificationsDetailPage;
import learnme.wicket.panels.BasicPanel;


public class NotificationPreview extends BasicPanel{
    private static final String ID_LINK = "link";
    
    private static final String ID_IMAGE = "image";
    private static final String ID_BADGES = "badges";
    private static final String ID_USERNAME = "username";
    private static final String ID_DESCRIPTION = "description";
    private static final String ID_TIME = "time";
    private static final String ID_ICON = "icon";
    
    private static final String CSS_SELECTOR = "notification";
    
    private static final String CSS_USERNAME = CSS_SELECTOR +"-user";
    private static final String CSS_MESSAGE = CSS_SELECTOR +"-msg";
    private static final String CSS_TIME = CSS_SELECTOR +"-time";
    
    private final Notification notification;
    private int preview = 60;
    private boolean card;
    
    public NotificationPreview(String id, Notification notification) {
    	this(id, notification, false);
    }
    
    public NotificationPreview(String id, Notification notification, boolean card) {
        super(id);
        
        this.notification = notification;
        this.card = card;
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        
        add(new Link<Object>(ID_LINK){
            
            @Override
            public void onClick() {
                setResponsePage(new NotificationsDetailPage(new PageParameters(), notification));
            }
            
            {
            	if (!isCard()) {
            		add(new Image(ID_IMAGE, new ContextRelativeResource(notification.getUser().getImage())));
                } else {
                	add(new EmptyComponent(ID_IMAGE));
                }
                add(new Label(ID_USERNAME, new PropertyModel<Notification>(notification, "getSender().getNameAndSurname")){{
                    setRenderBodyOnlyLabel(true);
                    addClass(CSS_USERNAME);
                }});
                add(new Label(ID_DESCRIPTION, new PropertyModel<Notification>(notification, "getDescription")){{
                    setRenderBodyOnlyLabel(true);
                    addClass(CSS_MESSAGE);
                }});
                add(new Label(ID_TIME, new PropertyModel<Notification>(notification, "getCreated")){{
                    addClass(CSS_TIME);
                }});
                
                add(new NotificationBadgesPanel(ID_BADGES, notification));
                
            	add(new Icon(ID_ICON, ESymbols.BELL) {
            		{
        				if (notification.getSeen() == null) {
        					setTextColor(EColors.DARK);
        				} else {
        					setVisible(false);
        				}
            		}
            	});
            }
        });
    }

    public int getPreview() {
        return preview;
    }

    public void setPreview(int preview) {
        this.preview = preview;
    }

	public Notification getNotification() {
		return notification;
	}

	public boolean isCard() {
		return card;
	}

	public void setCard(boolean card) {
		this.card = card;
	}
}
