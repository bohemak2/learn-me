package learnme.wicket.panels.application.notification;

import org.apache.wicket.model.ResourceModel;

import learnme.hibernate.entities.Notification;
import learnme.hibernate.enums.ENotificationTarget;
import learnme.hibernate.enums.ENotificationType;
import learnme.wicket.components.badge.Badge;
import learnme.wicket.components.layout.HorizontalLayout;
import learnme.wicket.frontend.definition.EBadge;
import learnme.wicket.panels.BasicPanel;

public class NotificationBadgesPanel extends BasicPanel {

	private final static String ID_WRAPPER = "badges-wrapper";
	
	Notification notification;
	
	public NotificationBadgesPanel(String id, Notification notification) {
		super(id);
		
		this.notification = notification;
	}
	
	@Override
	protected void onInitialize() {
		super.onInitialize();
		
		add(new HorizontalLayout(ID_WRAPPER) {
			{
				ENotificationTarget target = notification.getTarget();
				ENotificationType type = notification.getType();
				
				if (target != null) {
					switch (target) {
						case COURSE_STUDENT:
						case COURSE_TUTOR:
							add(new Badge(newId(), new ResourceModel("BadgeCourse"), EBadge.PRIMARY, true));
							break;
						case USER_FOLLOW:
							add(new Badge(newId(), new ResourceModel("BadgeFollow"), EBadge.SUCCESS, true));
							break;
						case POST_MESSAGE:
							add(new Badge(newId(), new ResourceModel("BadgeMessage"), EBadge.WARNING, true));
							break;
						case ASSIGNMENT: 
						case ASSIGNMENT_COURSE:
							add(new Badge(newId(), new ResourceModel("BadgeAssignment"), EBadge.DANGER, true));
							break;
							
						case SYSTEM_WELLCOME:
							add(new Badge(newId(), new ResourceModel("BadgeSystem")) {
								{
									setInverse(true);
								}
							});
						default:
							break;
					}
				}
				
				if (type != null) {
					switch (type) {
						case INSERT:
							add(new Badge(newId(), new ResourceModel("BadgeInsert"), true));
							break;
						case UPDATE:
							add(new Badge(newId(), new ResourceModel("BadgeUpdate"), true));
							break;
						case DELETE:
							add(new Badge(newId(), new ResourceModel("BadgeDelete"), true));
							break;
						default:
							break;
					}
				}
			}
		});
	}
}
