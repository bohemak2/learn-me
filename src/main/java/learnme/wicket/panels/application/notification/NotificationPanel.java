package learnme.wicket.panels.application.notification;

import java.util.LinkedHashSet;
import java.util.Optional;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.ResourceModel;

import learnme.hibernate.entities.Notification;
import learnme.hibernate.entities.User;
import learnme.hibernate.utils.EntityLoader;
import learnme.wicket.components.EmptyComponent;
import learnme.wicket.components.badge.Badge;
import learnme.wicket.components.icon.Icon;
import learnme.wicket.components.label.Label;
import learnme.wicket.components.layout.Layout;
import learnme.wicket.components.link.Link;
import learnme.wicket.frontend.definition.EAligment;
import learnme.wicket.frontend.definition.EDirection;
import learnme.wicket.frontend.definition.ESpacing;
import learnme.wicket.frontend.definition.ESymbols;
import learnme.wicket.frontend.definition.ETransform;
import learnme.wicket.pages.application.notifications.NotificationsPage;
import learnme.wicket.panels.BasicPanel;

public class NotificationPanel extends BasicPanel{
	
    private static final String ID_ICON = "icon";
    private static final String ID_COUNT_NEW = "count-new";
    private static final String ID_NOTIFICATIONS = "list-notifications";
    
    private static final String ID_HEADER = "header";
    
    User user;
    
    public NotificationPanel(String id, User user) {
        super(id);
        
        this.user = user;
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();  
        
        int notseen = 0;

        EntityLoader entities = new EntityLoader<Notification>(user.getLastNotifications(), true, 3) {
        	
			@Override
			public Component renderComponent(LinkedHashSet<Notification> list) {
				
				return null;
			}
		};
        
        LinkedHashSet<Notification> notifications = entities.loadNext();
        
        add(new Icon(ID_ICON, ESymbols.BELL));
        add(new Label(ID_HEADER, new ResourceModel("Header")){{ setTransform(ETransform.UPPERCASE); }});
        
        if(notifications.size() > 0){   
            Layout items = new Layout(ID_NOTIFICATIONS);
            
            for(Notification notification : notifications){
                items.add(new NotificationPreview(items.newChildId(), notification));
                if (notification.getSeen() == null){
                    notseen++;
                }
            }
            items.add(new Link(items.newChildId(), new ResourceModel("AllItems")) {
                {
                    addSpacing(ESpacing.PADDING, 1, EDirection.VERTICAL);
                    setRenderBodyOnly(false);
                    setAlignment(EAligment.TEXT_CENTER);
                }
                
                @Override
                public void onClick(Optional<AjaxRequestTarget> target) {
                    setResponsePage(NotificationsPage.class);
                }
            });
            add(items);
        }else{
            add(new Label(ID_NOTIFICATIONS, new ResourceModel("NoItems")));
        }
        
        if (notseen > 0){
            add(new Badge(ID_COUNT_NEW, new Model<Integer>(notseen)));
        }else{
            add(new EmptyComponent(ID_COUNT_NEW));
        }
    }
}
