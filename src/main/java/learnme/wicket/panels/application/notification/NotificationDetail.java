package learnme.wicket.panels.application.notification;

import java.util.Optional;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.request.resource.ContextRelativeResource;

import learnme.hibernate.entities.Conversation;
import learnme.hibernate.entities.Message;
import learnme.hibernate.entities.Notification;
import learnme.hibernate.entities.user.Student;
import learnme.wicket.components.button.Button;
import learnme.wicket.components.label.Label;
import learnme.wicket.components.label.LabelHeader;
import learnme.wicket.components.link.Link;
import learnme.wicket.frontend.definition.EDirection;
import learnme.wicket.frontend.definition.ESpacing;
import learnme.wicket.pages.application.calendar.CalendarPage;
import learnme.wicket.pages.application.courses.CoursePage;
import learnme.wicket.pages.application.posts.PostPage;
import learnme.wicket.pages.application.users.UserPage;
import learnme.wicket.panels.BasicPanel;


public class NotificationDetail extends BasicPanel{

    private static final String ID_USERNAME = "username";
    private static final String ID_MESSAGE = "message";
    private static final String ID_BADGES = "badges";
    private static final String ID_TIME = "time";
    private static final String ID_SHOW = "button";
    
    private final Notification notification;
    
    public NotificationDetail(String id, Notification notification) {
        super(id);
        
        if(notification != null){
            this.notification = notification;
        } else{
            this.notification = new Notification();
        }
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        
            add(new Link(ID_USERNAME, new LabelHeader(Link.ID_LINK_CONTENT, new PropertyModel<Notification>(notification, "getSender().getNameAndSurname"), 4)) {
				
				@Override
				public void onClick(Optional<AjaxRequestTarget> target) {
					setResponsePage(new UserPage(getPage().getPageParameters(), notification.getSender()));					
				}
			});
            
            add(new Label(ID_TIME, new PropertyModel<Notification>(notification, "getCreated")));
            
            add(new NotificationBadgesPanel(ID_BADGES, notification));

            if (notification.getMessage() != null) {
            	add(new Label(ID_MESSAGE, new PropertyModel<Notification>(notification, "getMessage().getText")));
            } else {
            	add(new Label(ID_MESSAGE, new PropertyModel<Notification>(notification, "getDescription()")));
            }
            
            add(new Button(ID_SHOW, new ResourceModel("NotificationShowButton")) {
            	
            	{
            		addSpacing(ESpacing.MARGIN, 3, EDirection.TOP);
            		
            		if (notification.getTarget() != null) {
	            		switch (notification.getTarget()) {
							case POST_MESSAGE:
								if (notification.getMessage() == null) {
									setVisible(false);
								}
								break;
							case SYSTEM_WELLCOME:
								setVisible(false);
							default:
								break;
						}
            		} else {
            			setVisible(false);
            		}
            	}
            	
				@Override
				public void onClick(Optional<AjaxRequestTarget> target) {
            		switch (notification.getTarget()) {
						case COURSE_STUDENT:
						case COURSE_TUTOR:
	//							TODO: change this hack (add Course to Notification? -> More universal...)
							for (Student student : notification.getSender().getCoursesAsStudent()) {
								if (student.getRegistered().equals(notification.getCreated())) {
									setResponsePage(new CoursePage(getPage().getPageParameters(), student.getCourse()));
									break;
								}
							}
							break;
						case USER_FOLLOW:
							setResponsePage(new UserPage(getPage().getPageParameters(), notification.getSender()));
							break;
						case POST_MESSAGE:
							setResponsePage(new PostPage(getPage().getPageParameters(), notification.getMessage().getConversation()));
							break;
						case ASSIGNMENT:
						case ASSIGNMENT_COURSE:
//							TODO: Add Date()
							setResponsePage(new CalendarPage(getPage().getPageParameters()));
							break;
						default:
							break;
					}
				}
			});
    }
    
    

}
