package learnme.wicket.panels.application.header;

import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.request.resource.ContextRelativeResource;

import learnme.hibernate.entities.User;
import learnme.wicket.components.EmptyComponent;
import learnme.wicket.pages.application.ApplicationPage;
import learnme.wicket.pages.general.home.HomePage;
import learnme.wicket.panels.BasicPanel;
import learnme.wicket.panels.application.notification.NotificationPanel;
import learnme.wicket.panels.application.user.UserDropdownPanel;
import learnme.wicket.panels.general.language.LocalePanel;


public class HeaderPanel extends BasicPanel{
	
	private static final String ID_HOME = "home-link";
	private static final String ID_LOGO = "home-logo";
    private static final String ID_LANGUAGE = "language";
    private static final String ID_NOTIFICATIONS = "notifications";
    private static final String ID_USER = "user";
 
    public HeaderPanel(String id) {
        super(id);
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        
        add(new Link<Image>(ID_HOME) {
			private static final long serialVersionUID = 1L;

			{
				add(new Image(ID_LOGO, new ContextRelativeResource("img/logo-fragment.png")));
			}

			@Override
			public void onClick() {
				setResponsePage(HomePage.class);
			}
        });
        add(new LocalePanel(ID_LANGUAGE));
        
        if(getPage() instanceof ApplicationPage){
            User user = ((ApplicationPage)getPage()).getUser();
            add(new NotificationPanel(ID_NOTIFICATIONS, user));
            add(new UserDropdownPanel(ID_USER, user));
        }else{
            add(new EmptyComponent(ID_NOTIFICATIONS));
            add(new EmptyComponent(ID_USER));
        }
    }    
}
