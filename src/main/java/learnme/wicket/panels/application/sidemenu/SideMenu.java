package learnme.wicket.panels.application.sidemenu;

import learnme.wicket.components.EmptyComponent;
import learnme.wicket.components.menu.Menu;
import learnme.wicket.panels.BasicPanel;


public class SideMenu extends BasicPanel{
    public static final String ID_MENU = "menu";
    
    private Menu menu;

    public SideMenu(String id) {
        super(id);
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        
        if (menu != null){
            add(getMenu());
        }else{
            add(new EmptyComponent(ID_MENU));
        }
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }
}
