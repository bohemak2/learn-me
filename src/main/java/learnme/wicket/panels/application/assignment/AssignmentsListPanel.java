package learnme.wicket.panels.application.assignment;

import java.util.LinkedHashSet;
import java.util.Optional;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.ResourceModel;

import learnme.hibernate.entities.Assignment;
import learnme.hibernate.entities.Conversation;
import learnme.hibernate.entities.Course;
import learnme.hibernate.entities.User;
import learnme.hibernate.utils.EntityLoader;
import learnme.wicket.components.link.Link;
import learnme.wicket.components.table.Table;
import learnme.wicket.pages.application.notifications.NotificationsDetailPage;
import learnme.wicket.pages.application.notifications.NotificationsPage;
import learnme.wicket.panels.BasicPanel;

public class AssignmentsListPanel extends BasicPanel {
	
	private final static String ID_ASSIGNMENTS_LIST = "assignments-list";

	private User user;
	private Course course;
	private EntityLoader<Assignment> loader;
	
	private Table table;
	
	public AssignmentsListPanel(String id) {
		this(id, null, null);
	}

	public AssignmentsListPanel(String id, User user, Course course) {
		super(id);
		
		this.course = course;
	}

	@Override
	protected void onInitialize() {
		super.onInitialize();
		
		table = new Table(ID_ASSIGNMENTS_LIST) {
			{
				setHeadRow(new ResourceModel("AssigmentsPanelName"), new ResourceModel("AssigmentsPanelDescription"), new ResourceModel("AssigmentsPanelSince"), new ResourceModel("AssigmentsPanelUntil"), new ResourceModel("AssigmentsPanelWhere"));
			}
		};
		table.add(loader = new EntityLoader<Assignment>(Assignment.getAssignments(user, course), true) {
			
			@Override
			public Component renderComponent(LinkedHashSet<Assignment> assignments) {
				if(assignments.size() > 0) {
					fillAssigments(assignments);
					return table;
				}
				return null;
			}			
		});
		fillAssigments(loader.loadNext());
		
		add(table);
	}

	private void fillAssigments(LinkedHashSet<Assignment> assignments) {
		for (Assignment assignment : assignments) {
			table.addRow(new Link(Table.ID_COMPONENT, new PropertyModel<String>(assignment, "getName")) {
				
				@Override
				public void onClick(Optional<AjaxRequestTarget> target) {
					setResponsePage(new NotificationsPage(getPage().getPageParameters()));
//					TODO: Exactly NotificationDetail - Teoreticky by se to dalo sparovat s Result.
//					setResponsePage(new NotificationsDetailPage(getPage().getPageParameters(), assignment.));
				}
			}, new Label(Table.ID_COMPONENT, new PropertyModel<String>(assignment, "getDescription")), new Label(Table.ID_COMPONENT, new PropertyModel<String>(assignment, "getSince().toGMTString")), new Label(Table.ID_COMPONENT, new PropertyModel<String>(assignment, "getUntil().toGMTString")), new Label(Table.ID_COMPONENT, new PropertyModel<String>(assignment, "getPlace().getAddress")));
		}
	}
}
