package learnme.wicket.panels.application.course;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.ResourceModel;

import learnme.hibernate.dto.DTOFieldsCount;
import learnme.hibernate.entities.Field;
import learnme.hibernate.entities.User;
import learnme.hibernate.interfaces.ICoursable;
import learnme.hibernate.utils.BasicLoader;
import learnme.wicket.components.menu.Menu;
import learnme.wicket.components.menu.MenuContainer;
import learnme.wicket.components.menu.MenuItem;
import learnme.wicket.frontend.definition.EColors;
import learnme.wicket.frontend.definition.EDirection;
import learnme.wicket.frontend.definition.ESpacing;
import learnme.wicket.frontend.definition.ESymbols;
import learnme.wicket.panels.BasicPanel;

public class CoursesSelectableListPanel extends BasicPanel {
	
	private final static String ID_MENU = "menu-list";
	private final static String ID_COURSE_LIST = "courses-list-panel";

	private final static String CSS_MENU = "courses-nav-menu";
	
	private int fieldsLimit = 5;
	
	private BasicLoader<DTOFieldsCount> fields;
	
	private Set<Field> selected = new HashSet<Field>();
	
	private CoursesListPanel listPanel;
	
	
	public CoursesSelectableListPanel(String id) {
		this(id, null);
	}
	
	public CoursesSelectableListPanel(String id, User user, Class<? extends ICoursable>... roles) {
		super(id);
		
		if (roles.length != 0) {
			fields = new BasicLoader<DTOFieldsCount>(Field.getPopularFields(user, roles), 5);
		} else {
			fields = new BasicLoader<DTOFieldsCount>(Field.getPopularFields(), 5);
		}
		
		add(listPanel = new CoursesListPanel(ID_COURSE_LIST, user, roles) {
			{
				setFields(selected);
			}
		});
	}
	
	@Override
	protected void onInitialize() {
		super.onInitialize();
		
		Set<DTOFieldsCount> list = fields.loadNext();
		
		if (!list.isEmpty()) {
			add(new Menu(ID_MENU) {
				{
					addClass(CSS_MENU);
					
					addMenuContainer(new MenuContainer(newItemId()) {
						{
							int loop = 1;
							
							for (DTOFieldsCount bo : list) {
								final int loopF = loop;
								
								addMenuItem(new MenuItem(newItemId(), new Model<String>(bo.field.getName()), ESymbols.PIN_ALT) {
									
									{
										setButton(true);
									}
									
									@Override
									protected void onRender() {
										if (selected.contains(bo.field)) {
											if (getIcon() == ESymbols.PIN_ALT) {
												setIcon(ESymbols.CLOSE);
											}
										} else {
											if (getIcon() == ESymbols.CLOSE ) {
												setIcon(ESymbols.PIN_ALT);
											}
										}
										
										super.onRender();
									}
									
									@Override
									protected void onInitialize() {
										super.onInitialize();
										
										if (loopF != 1) {
											addSpacing(ESpacing.MARGIN, 2, EDirection.LEFT);
										}
									}
									
									@Override
									public	void onClick(Optional<AjaxRequestTarget> target) {									
										addSelected(bo.field);
										listPanel.reloadBySelectedfields();
									}
									
								});
								
								loop++;
							}
							
							addMenuItem(new MenuItem(newItemId(), null, ESymbols.TRASH) {
								{
									setButton(true);
									
									if (list.isEmpty()) {
										setVisible(false);
									}
								}
								
								@Override
								protected void onInitialize() {
									super.onInitialize();
									addSpacing(ESpacing.MARGIN, 2, EDirection.LEFT);
									getButton().setBackgroundColor(EColors.WARNING);
								}
								
								@Override
								public	void onClick(Optional<AjaxRequestTarget> target) {									
									if (!selected.isEmpty()) {
										selected.clear();
										listPanel.reloadBySelectedfields();
									}
								}
							});
						}
					});
					
					addSpacing(ESpacing.MARGIN, 4, EDirection.BOTTOM);
				}
			});
		} else {
			add(new Label(ID_MENU, new ResourceModel("NoRecords")));
		}
	}

	private boolean addSelected(Field field) {
		if (!selected.contains(field)) {
			selected.add(field);
			return true;
		} else {
			selected.remove(field);
			return false;
		}
	}
}
