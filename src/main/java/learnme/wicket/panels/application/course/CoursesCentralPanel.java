package learnme.wicket.panels.application.course;

import org.apache.wicket.model.ResourceModel;

import learnme.hibernate.entities.User;
import learnme.hibernate.entities.user.Student;
import learnme.hibernate.entities.user.Tutor;
import learnme.wicket.components.tab.Tab;
import learnme.wicket.components.tab.TabItem;
import learnme.wicket.panels.BasicPanel;

public class CoursesCentralPanel extends BasicPanel{

	private final static String ID_TAB = "tabs";
	
	private User user;
	
	public CoursesCentralPanel(String id, User user) {
		super(id);
		
    	add(new Tab(ID_TAB) {
    		{
    			addTabItem(new TabItem(newTabId(), new ResourceModel("StudentLabel"), new CoursesSelectableListPanel(newContentId(), user, Student.class)));
    			addTabItem(new TabItem(newTabId(), new ResourceModel("TutorLabel"), new CoursesSelectableListPanel(newContentId(), user, Tutor.class)));
    		}
    	});	
	
	}
}
