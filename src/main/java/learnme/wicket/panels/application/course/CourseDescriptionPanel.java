package learnme.wicket.panels.application.course;

import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.model.StringResourceModel;

import learnme.hibernate.entities.Course;
import learnme.hibernate.entities.Field;
import learnme.hibernate.entities.Tag;
import learnme.wicket.components.card.Card;
import learnme.wicket.components.table.Table;
import learnme.wicket.panels.BasicPanel;

public class CourseDescriptionPanel extends BasicPanel {
	
	private static final String ID_CARD_TABLE = "card-table";

	private Course course;
	
	public CourseDescriptionPanel(String id, Course course) {
		super(id);
		
		this.course = course;
	}
	
	@Override
	protected void onInitialize() {
		super.onInitialize();
		
		add(new Card(ID_CARD_TABLE,
				new Table(Card.ID_CONTENT, true) {
	
					{
						setBorders(false);
						
						Model<Course> model = new Model<>(course);
						
						setHeadRow(new ResourceModel("CourseDescriptionDescription"), new ResourceModel("CourseDescriptionCapacity"), new ResourceModel("CourseDescriptionTerm"), new ResourceModel("CourseDescriptionAutoAcceptance"), new ResourceModel("CourseDescriptionFee"), new ResourceModel("CourseDescriptionPresence"), new ResourceModel("CourseDescriptionFields"), new ResourceModel("CourseDescriptionTags"));
						
						addRow(new PropertyModel<String>(course, "getDescription"));
						addRow(new StringResourceModel("CourseDescriptionCapacityValue", model));
						addRow(getTerm(model));
						addRow(new ResourceModel(String.valueOf(course.isAutoacceptance())));
						addRow(new StringResourceModel("CourseDescriptionFeeValue", model));
						addRow(new ResourceModel(String.valueOf(course.isPresence())));
						
						
//						TODO: Prolink to MarketPage(Tag tag)
						String tags = "";
						for(Tag tag : course.getTags()) {
							tags += tag.getField().getName() + " (" + tag.getName() + "), ";
						}
						
						addRow(new Model<String>(tags.substring(0, tags.length() - 2)));
					}
				
				}));
	}
	
	private IModel<String> getTerm(Model<Course> courseModel){
		IModel<String> term = new ResourceModel("CourseDescriptionTermValueAlways");
		
		if (course.getSince() != null && course.getUntil() != null) {
			
			term = new StringResourceModel("CourseDescriptionTermValueFull", courseModel);
		
		} else {
			
			if (course.getSince() != null) {
				term = new StringResourceModel("CourseDescriptionTermValueSince", courseModel);
			}
			
			if (course.getUntil() != null) {
				term = new StringResourceModel("CourseDescriptionTermValueUntil", courseModel);
			}
		}
		
		return term;
	}

}
