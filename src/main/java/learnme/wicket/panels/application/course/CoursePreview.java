	package learnme.wicket.panels.application.course;

import org.apache.wicket.model.PropertyModel;

import learnme.hibernate.entities.Course;
import learnme.wicket.components.EmptyComponent;
import learnme.wicket.components.badge.Badge;
import learnme.wicket.components.label.Label;
import learnme.wicket.components.layout.VerticalLayout;
import learnme.wicket.frontend.definition.EBadge;
import learnme.wicket.panels.BasicPanel;

public class CoursePreview extends BasicPanel {
	
	private final static String ID_LAYOUT = "course-preview-wrapper";

	private Course course;
	private boolean freecapacity;
	
	
	public CoursePreview(String id, Course course) {
		this(id, course, true);
	}
	
	public CoursePreview(String id, Course course, boolean freecapacity) {
		super(id);
		
		this.course = course;
		this.freecapacity = freecapacity;
	}

	@Override
	protected void onInitialize() {
		super.onInitialize();
		
		add(new VerticalLayout(ID_LAYOUT) {
			{
				add(new Label(newId(), new PropertyModel<String>(course, "getDescription")));
				if (freecapacity) {
					add(new Badge(newId(), new PropertyModel<Integer>(course, "getFreeCapacity"), course.getFreeCapacity() > 0 ? EBadge.SUCCESS : EBadge.DANGER));
				}
			}
		});
	}
}
