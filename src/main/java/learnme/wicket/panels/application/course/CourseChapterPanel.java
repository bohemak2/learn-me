package learnme.wicket.panels.application.course;

import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.model.StringResourceModel;

import learnme.hibernate.entities.Chapter;
import learnme.hibernate.entities.Course;
import learnme.wicket.components.card.Card;
import learnme.wicket.components.label.Label;
import learnme.wicket.components.table.Table;
import learnme.wicket.panels.BasicPanel;
import learnme.wicket.panels.application.user.UserLittlePreviewPanel;

public class CourseChapterPanel extends BasicPanel {

	private static final String ID_CARD_TABLE = "card-table";
	
	private Course course;
	
	public CourseChapterPanel(String id, Course course) {
		super(id);
		
		this.course = course;
	}
	
	@Override
	protected void onInitialize() {
		super.onInitialize();
		
		add(new Card(ID_CARD_TABLE,
				new Table(Card.ID_CONTENT) {
	
					{
						setBorders(false);
						setNoRecordsModel(new ResourceModel("NoChapters"));
						
						
						setHeadRow(new ResourceModel("CourseChapterName"), new ResourceModel("CourseChapterAuthor"), new ResourceModel("CourseChapterMinutes"));
						
						for (Chapter chapter : course.getChapters()) {
							Model<Chapter> model = new Model(chapter);
							addRow(new Label(ID_COMPONENT, new PropertyModel<>(model, "getName")), new UserLittlePreviewPanel(ID_COMPONENT, chapter.getAuthor().getUser(), true), new Label(ID_COMPONENT, new StringResourceModel("CourseChapterMinutesValue", model)));
						}
					}
				
				}));
	}

}
