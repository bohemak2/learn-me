package learnme.wicket.panels.application.course;

import java.util.Optional;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.resource.ContextRelativeResource;

import learnme.hibernate.entities.Course;
import learnme.hibernate.entities.User;
import learnme.hibernate.entities.user.Student;
import learnme.hibernate.entities.user.Tutor;
import learnme.wicket.components.button.Button;
import learnme.wicket.components.label.Label;
import learnme.wicket.components.link.Link;
import learnme.wicket.frontend.definition.EColors;
import learnme.wicket.pages.application.courses.CoursePage;
import learnme.wicket.pages.application.users.UserPage;
import learnme.wicket.panels.BasicPanel;

public class CourseProfilePanel extends BasicPanel {

	private final static String ID_WRAPPER = "course-preview-wrapper";
	
	private final static String ID_COURSE_NAME = "course-name";
	private final static String ID_COURSE_COVER = "course-cover";
	private final static String ID_COURSE_SIGN = "course-sign";
	private final static String ID_COURSE_OWNER_LINK = "course-owner-link";
	
	private User user;
	private Course course;
 	private boolean canSign;
 	
	
	public CourseProfilePanel(String id, User user, Course course) {
		super(id);
		
		this.user = user;
		this.course = course;
		this.canSign = canSign();
	}


	@Override
	protected void onInitialize() {
		super.onInitialize();
		
		Label owner = new Label(Link.ID_LINK_CONTENT, new PropertyModel<Course>(course, "getOwner().getNameAndSurname")) {
			{
				setRenderBodyOnlyLabel(true);
			}
		};
		
		add(new Label(ID_COURSE_NAME, new PropertyModel<Course>(course, "getName")));		
		add(new Image(ID_COURSE_COVER, new ContextRelativeResource(course.getCover())));
		add(new Link(ID_COURSE_OWNER_LINK, owner){
			
			{
				setTextColor(EColors.WHITE);
			}
			
			@Override
			public void onClick(Optional<AjaxRequestTarget> target) {
				setResponsePage(new UserPage(getPage().getPageParameters(), course.getOwner()));
			}
		});
		
		add(new Button(ID_COURSE_SIGN, new PropertyModel<String>(CourseProfilePanel.this, "getModel"), true) {
			
			{		
				
				{
					if (course.getOwner().equals(user) || course.getFreeCapacity() < 1) {
						setVisible(false);
					} else {
						for (Tutor tutor : course.getTutors()) {
							if (tutor.getUser().equals(user)) {
								setVisible(false);
							}
						}
					}
				}
				
				if (!canSign) {
					setBackgroundColor(EColors.DISABLED);
				}
			}
			
			@Override
			public void onClick(Optional<AjaxRequestTarget> target) {
				Student persist = null;
				
				if (canSign) {
					boolean toAdd = true;
					
					for (Student student : course.getStudentsResults()) {
						if (student.getUser().equals(user)) {
							student.setActive(true, true);
							toAdd = false;
							
							persist = student;
							break;
						}
					}
					
					if (toAdd) {
						course.addStudent(user);		
						for (Student student : user.getCoursesAsStudent()) {
							if (student.getCourse().equals(course)) {
								student.persist();
								break;
							}
						}
					}
					
					setBackgroundColor(EColors.DISABLED);
				} else {
					
					for (Student student : course.getStudentsResults()) {
						if (student.getUser().equals(user) && student.isActive()) {
							student.setActive(false, true);
							
							persist = student;
							break;
						}
					}
					
					setBackgroundColor(EColors.PRIMARY);
				}
				
				canSign = !canSign;
				
				if (persist != null) {
					persist.persist();
				}
				course.refresh();
				
				
				if (getPage() instanceof CoursePage) {
					setResponsePage(new CoursePage(getPage().getPageParameters(), course));
				} else {
					target.get().add(CourseProfilePanel.this);					
				}
			}
		});
	}
	
	
	public String getModel() {
		if(canSign) {
			return getLocalizer().getString("UserSignButton", this);
		} else {
			return getLocalizer().getString("UserUnsignButton", this);
		}
	}
	
	private boolean canSign() {
		user.refresh();
		
		for (Student student : user.getCoursesAsStudent()) {
			if (student.getCourse().equals(course) && student.isActive()) {
				return false;
			}
		}
		
		return true;
	}

}
