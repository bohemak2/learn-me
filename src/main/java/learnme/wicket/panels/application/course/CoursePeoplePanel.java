package learnme.wicket.panels.application.course;

import org.apache.wicket.model.ResourceModel;

import learnme.hibernate.entities.Course;
import learnme.hibernate.entities.User;
import learnme.hibernate.entities.user.Student;
import learnme.hibernate.entities.user.Tutor;
import learnme.wicket.components.card.Card;
import learnme.wicket.components.label.Label;
import learnme.wicket.components.label.LabelHeader;
import learnme.wicket.components.layout.HorizontalLayout;
import learnme.wicket.components.table.Table;
import learnme.wicket.frontend.definition.EGrid;
import learnme.wicket.panels.BasicPanel;
import learnme.wicket.panels.application.user.UserLittlePreviewPanel;

public class CoursePeoplePanel extends BasicPanel {

	private static final String ID_CARD_TABLE = "cards-wrapper";
	
	private Course course;
	
	public CoursePeoplePanel(String id, Course course) {
		super(id);
		
		this.course = course;
	}
	
	@Override
	protected void onInitialize() {
		super.onInitialize();
		
		add(new HorizontalLayout(ID_CARD_TABLE) {
			{
				setGrid(EGrid.MEDIUM, 6);
				
				add(new Card(newId(),
						new Table(Card.ID_CONTENT) {
			
							{
								setBorders(false);						
								
								User owner = course.getOwner();
								setHeadRow(new ResourceModel("CoursePeopleName"), new ResourceModel("CoursePeopleRole"));
								
								addRow(new UserLittlePreviewPanel(ID_COMPONENT, owner, true), new Label(ID_COMPONENT, new ResourceModel("CoursePeopleOwner")));
								
								for (Tutor tutor : course.getTutors()) {
									User user = tutor.getUser();
									if (tutor.isActive() && !user.equals(owner)) {
										addRow(new UserLittlePreviewPanel(ID_COMPONENT, user, true), new Label(ID_COMPONENT, new ResourceModel("CoursePeopleTutor")));								
									}
								}
							}
						
						}, new LabelHeader(Card.ID_HEADER, new ResourceModel("CoursePeopleTutors"), 5)));
				
				add(new Card(newId(),
						new Table(Card.ID_CONTENT) {
			
							{
								setBorders(false);						
								setNoRecordsModel(new ResourceModel("CoursePeopleNoStudents"));
								
								User owner = course.getOwner();
								setHeadRow(new ResourceModel("CoursePeopleName"));
								
								for (Student student : course.getStudentsResults()) {
									if(student.isActive()) {
										addRow(new UserLittlePreviewPanel(ID_COMPONENT, student.getUser(), true));										
									}
								}
							}
						
						}, new LabelHeader(Card.ID_HEADER, new ResourceModel("CoursePeopleStudents"), 5)));
				}
		});
	}

}
