package learnme.wicket.panels.application.course;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;

import javax.swing.text.html.HTML.Tag;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.model.PropertyModel;

import learnme.hibernate.entities.Course;
import learnme.hibernate.entities.Field;
import learnme.hibernate.entities.User;
import learnme.hibernate.interfaces.ICoursable;
import learnme.hibernate.utils.EntityLoader;
import learnme.wicket.components.card.Card;
import learnme.wicket.components.label.LabelHeader;
import learnme.wicket.components.layout.VerticalLayout;
import learnme.wicket.components.link.Link;
import learnme.wicket.frontend.definition.EColors;
import learnme.wicket.frontend.definition.EGrid;
import learnme.wicket.pages.application.courses.CoursePage;
import learnme.wicket.pages.application.users.UserPage;
import learnme.wicket.panels.BasicPanel;

public class CoursesListPanel extends BasicPanel {
	
	private final static String ID_LIST = "courses-list";

	private User user;
	
	private int coursesLimit = 12;
	
	private  Set<Field> fields = new HashSet<Field>();
	private Set<Tag> tags = new HashSet<Tag>();
	
	private EntityLoader<Course> courses;
	
	private VerticalLayout coursesList;
	Class<? extends ICoursable>[] roles;
	
	public CoursesListPanel(String id) {
		this(id, null, null, null, null);
	}
	
	public CoursesListPanel(String id, User user, Class<? extends ICoursable>... roles) {
		this(id, user, null, null, roles);
	}
	
	public CoursesListPanel(String id, User user, Set<Field> fields,  Set<Tag> tags, Class<? extends ICoursable>... roles) {
		super(id);
		
		this.user = user;
		this.fields = fields;
		this.tags = tags;
		this.roles = roles;
		
		add(coursesList = new VerticalLayout(ID_LIST) {
			{
				setGrid(EGrid.SMALL, 6);
				setGrid(EGrid.MEDIUM, 3);
			}
		});
	}


	@Override
	protected void onInitialize() {	
		
		reloadBySelectedfields();
		
		super.onInitialize();
	}
	
	
	public void reloadBySelectedfields() {
		coursesList.removeAll();
		courses = new EntityLoader<Course>(Course.getCoursesByField(fields, user, roles)) {

			{
				setLimit(coursesLimit);
			}
			
			@Override
			public Component renderComponent(LinkedHashSet<Course> list) {
				if (!list.isEmpty()) {
					fillCourses();
					return coursesList;
				}
				return null;
			}
			
		};
		
		fillCourses();
	}
	
	private void fillCourses() {
		for(Course course : courses.loadNext()) {
			coursesList.add(new Card(coursesList.newId(), new CoursePreview(Card.ID_CONTENT, course), new VerticalLayout(Card.ID_HEADER) {
					{
						if (user != null) {
//							TODO: Zde se jedna o jiny vypis - zapsane kurzy
						} else { 
//							TODO: Nasleduje to, co je nyni - prepsat na *PreviewPanel
						}
						
						add(new Link(newId(), new LabelHeader(Link.ID_LINK_CONTENT, new PropertyModel<String>(course, "getName"), 3)) {
							
							{
								setTextColor(EColors.DARK);
							}
							
							@Override
							public void onClick(Optional<AjaxRequestTarget> target) {
								setResponsePage(new CoursePage(getPage().getPageParameters(), course));
							}
						});
						
						add(new Link(newId(), new PropertyModel<String>(course, "getOwner().getNameAndSurname")) {

							{
								setTextColor(EColors.DARK);
							}
							
							@Override
							public void onClick(Optional<AjaxRequestTarget> target) {
								setResponsePage(new UserPage(getPage().getPageParameters(), course.getOwner()));	
							}
							
						});
					}
				}));			
		}
	}

	public Set<Field> getFields() {
		return fields;
	}


	public void setFields(Set<Field> fields) {
		this.fields = fields;
	}


	public Set<Tag> getTags() {
		return tags;
	}


	public void setTags(Set<Tag> tags) {
		this.tags = tags;
	}

	public VerticalLayout getCoursesList() {
		return coursesList;
	}
}
