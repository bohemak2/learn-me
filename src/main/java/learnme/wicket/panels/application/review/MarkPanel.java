package learnme.wicket.panels.application.review;

import java.util.ArrayList;
import java.util.List;

import org.apache.wicket.Page;
import org.apache.wicket.ajax.AjaxEventBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;

import learnme.hibernate.entities.Course;
import learnme.hibernate.entities.Review;
import learnme.hibernate.entities.User;
import learnme.hibernate.entities.user.Student;
import learnme.hibernate.enums.EMark;
import learnme.wicket.components.icon.Icon;
import learnme.wicket.components.layout.HorizontalLayout;
import learnme.wicket.frontend.definition.EColors;
import learnme.wicket.frontend.definition.ECursors;
import learnme.wicket.frontend.definition.ESymbols;
import learnme.wicket.pages.application.ApplicationPage;
import learnme.wicket.panels.BasicPanel;

public class MarkPanel extends BasicPanel {

	private static final String ID_WRAPPER = "mark-wrapper";
	
	private static final String CSS_WRAPPER = "marks";
	
	private User user;
	private Course course;
	
	private EMark mark;
	
	private boolean markable;
	
	List<Icon> icons = new ArrayList<Icon>();
	
	public MarkPanel(String id, User user) {
		this(id, user, true);
	}
		
	public MarkPanel(String id, Course course) {
		super(id);
		
		this.course = course;
		this.mark = EMark.COURSE;
	}
	
	public MarkPanel(String id, User user, boolean total) {
		super(id);
		
		this.user = user;
		
		if(total) {
			this.mark = EMark.TOTAL;
		} else {
			this.mark = EMark.USER;
		}
	}
	
	@Override
	protected void onInitialize() {
		super.onInitialize();
		
		
		add(new HorizontalLayout(ID_WRAPPER) {
			{
				double significancy = 0f;
				
				switch (mark) {
				case TOTAL:
					significancy = user.getSignificancy().getTotalMark();			
					break;
					
				case USER:
					significancy = user.getSignificancy().getUserMark();
					break;
					
				case COURSE:
					significancy = user.getSignificancy().getCourseMark();
					break;
					
				default:
					break;
				}	
				
				for (int i = 1; i <= EMark.MAX_MARK; i++) {		
					final int mark = i; 
					final double value = significancy;
					
					Icon icon = new Icon(newId(), ESymbols.STAR) {
						{
							if (markable) {
								setCursor(ECursors.POINTER);
							
								add(new AjaxEventBehavior("click") {
									
									@Override
									protected void onEvent(AjaxRequestTarget target) {
										setMark(mark);
										
										target.add(MarkPanel.this);
									}
								});
							}
							
							if (value >= mark * mark) {
								setTextColor(EColors.WARNING);
							}
						}
					};
					icons.add(icon);

					add(icon);
				}
				
				addClass(CSS_WRAPPER);
			}
		});
	}

	
	private void setMark(int value) {
		Page page = getPage();
		
		if (page instanceof ApplicationPage) {
			User me = ((ApplicationPage)page).getUser();
			
			switch (mark) {
				case TOTAL:
//					TODO: AddReview			
					break;
					
				case USER:
//					TODO: AddReview					
					break;
					
				case COURSE:
					for(Student student : course.getStudentsResults()) {
						if (student.getUser().equals(me)) {
							Review review = student.getReview();
							
							if (review == null) {
								review = new Review() {
									{
										setCourse(course);
										setUser(me);
										setMark(value);
									}
								};
							}
							
							student.setReview(review);
							review.persist();
						}
					}
					break;
				default:
					break;
			}
		}
	}

	public boolean isMarkable() {
		return markable;
	}

	public void setMarkable(boolean markable) {
		this.markable = markable;
	}
	
}
