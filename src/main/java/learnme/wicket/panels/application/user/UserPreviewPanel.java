package learnme.wicket.panels.application.user;

import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.resource.ContextRelativeResource;

import learnme.hibernate.entities.User;
import learnme.wicket.components.EmptyComponent;
import learnme.wicket.components.badge.Badge;
import learnme.wicket.components.icon.Icon;
import learnme.wicket.frontend.definition.EBadge;
import learnme.wicket.frontend.definition.ESymbols;
import learnme.wicket.panels.BasicPanel;


public class UserPreviewPanel extends BasicPanel{
	
	private final static String ID_USER_IMAGE = "user-image";
    
	private final static String ID_USER_PEOPLE_ICON = "user-people-icon";
	private final static String ID_USER_PEOPLE_FOLLOWERS = "user-people-followers";
    
    private final static String ID_USER_COURSES_ICON = "user-courses-icon";
    private final static String ID_USER_COURSES_OFFERED = "user-courses-offered";

    private User user;
    
    private boolean head;
    private boolean image;
    
    public UserPreviewPanel(String id, User user) {
    	this(id, user , false, false);
    }
    
    public UserPreviewPanel(String id, User user, boolean image, boolean head) {
        super(id);
        
        this.head = head;
        this.image = image;
        this.user = user;
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        
        if (head) {
        	add(new UserLittlePreviewPanel(ID_USER_IMAGE, user, true, true, false, false));
        } else {        	
        	if (image) {
        		add(new Image(ID_USER_IMAGE, new ContextRelativeResource(user.getImage())));
        	} else {
        		add(new EmptyComponent(ID_USER_IMAGE));
        	}
        }
        
        
        PropertyModel model;
        
        add(new Icon(ID_USER_PEOPLE_ICON, ESymbols.ID_BADGE));
        add(new Badge(ID_USER_PEOPLE_FOLLOWERS, model = new PropertyModel<String>(user, "getFollowers().size"), "FollowersLabel", (int)model.getObject() > 0 ? EBadge.INFO : EBadge.DEFAULT));
        
        add(new Icon(ID_USER_COURSES_ICON, ESymbols.BLACKBOARD));        
        add(new Badge(ID_USER_COURSES_OFFERED, model = new PropertyModel<String>(user, "getCoursesAsTutor().size"), "CoursesOfferingLabel", (int)model.getObject() > 0 ? EBadge.SUCCESS : EBadge.DEFAULT));
    }
    
    
    
}
