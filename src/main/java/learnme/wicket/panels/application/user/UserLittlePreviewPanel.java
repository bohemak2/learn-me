package learnme.wicket.panels.application.user;

import java.util.Optional;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.resource.ContextRelativeResource;

import learnme.hibernate.entities.User;
import learnme.wicket.components.EmptyComponent;
import learnme.wicket.components.label.LabelHeader;
import learnme.wicket.components.link.Link;
import learnme.wicket.pages.application.users.UserPage;
import learnme.wicket.panels.BasicPanel;
import learnme.wicket.panels.application.review.MarkPanel;

public class UserLittlePreviewPanel extends BasicPanel {
	
	private final static String ID_LITTLE_PREVIEW_AVATAR = "user-little-preview-avatar";
	private final static String ID_LITTLE_PREVIEW_LINK = "user-little-preview-link";
	private final static String ID_LITTLE_PREVIEW_MARKS = "user-little-preview-marks";
	
		
	public UserLittlePreviewPanel(String id, User user) {
		this(id, user, false, false, false, false);
	}
	
	public UserLittlePreviewPanel(String id, User user, boolean avatar) {
		this(id, user, avatar, false, false, false);
	}
	
	public UserLittlePreviewPanel(String id, User user, boolean avatar, boolean header, boolean marks, boolean nolink) {
		super(id);
		
		if (avatar) {
			add(new Image(ID_LITTLE_PREVIEW_AVATAR, new ContextRelativeResource(user.getImage())));
		} else {
			add(new EmptyComponent(ID_LITTLE_PREVIEW_AVATAR));
		}
		
		Component content = null;
		if (header) {
			content = new LabelHeader(Link.ID_LINK_CONTENT, new PropertyModel<User>(user, "getNameAndSurname"), 5);
		} else {
			content = new Label(Link.ID_LINK_CONTENT, new PropertyModel<User>(user, "getNameAndSurname"));
		}
		
		if (nolink) {
			add(new Label(ID_LITTLE_PREVIEW_LINK, new PropertyModel<User>(user, "getNameAndSurname")) {
				@Override
				protected void onComponentTag(ComponentTag tag) {
					super.onComponentTag(tag);
					
					tag.setName("span");
				}
			});
		} else {
			add(new Link(ID_LITTLE_PREVIEW_LINK, content) {
				
				@Override
				public void onClick(Optional<AjaxRequestTarget> target) {
					setResponsePage(new UserPage(getPage().getPageParameters(), user));
				}
			});			
		}
		
		if (marks) {
			add(new MarkPanel(ID_LITTLE_PREVIEW_MARKS, user));
		} else {
			add(new EmptyComponent(ID_LITTLE_PREVIEW_MARKS));
		}
	}
	
	
}
