package learnme.wicket.panels.application.user;

import java.util.Optional;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.request.resource.ContextRelativeResource;

import learnme.hibernate.entities.User;
import learnme.wicket.components.icon.Icon;
import learnme.wicket.components.menu.Menu;
import learnme.wicket.components.menu.MenuContainer;
import learnme.wicket.components.menu.MenuItem;
import learnme.wicket.frontend.definition.ESymbols;
import learnme.wicket.pages.application.ApplicationPage;
import learnme.wicket.pages.application.messages.MessagesPage;
import learnme.wicket.pages.application.notifications.NotificationsPage;
import learnme.wicket.pages.application.settings.SettingsPage;
import learnme.wicket.pages.application.users.UserPage;
import learnme.wicket.pages.general.login.LoginPage;
import learnme.wicket.panels.BasicPanel;


public class UserDropdownPanel extends BasicPanel{

    private final static String ID_USER_IMAGE = "user-image";
    private final static String ID_USER_FULLNAME = "user-fullname";
    private final static String ID_USER_MORE_ICON = "user-more-icon";
    
    private final static String ID_MENU = "menu";
    
    User user;
    
    public UserDropdownPanel(String id, User user) {
        super(id);
        
        this.user = user;
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();

        add(new Image(ID_USER_IMAGE, new ContextRelativeResource(user.getImage())));
        add(new Label(ID_USER_FULLNAME, new Model<>(user.getNameAndSurname())));
        add(new Icon(ID_USER_MORE_ICON, ESymbols.ANGLE_DOWN));
        add(new Menu(ID_MENU){
            {
             addMenuContainer(new MenuContainer(newItemId()){
                 {
                     addMenuItem(new MenuItem(newItemId(), new ResourceModel("MenuProfileItem"), ESymbols.USER, UserPage.class));
                     addMenuItem(new MenuItem(newItemId(), new ResourceModel("MenuSettingsItem"), ESymbols.SETTINGS, SettingsPage.class));
                     addMenuItem(new MenuItem(newItemId(), new ResourceModel("MenuLogoutItem"), ESymbols.ANGLE_DOUBLE_RIGHT, null) {
						private static final long serialVersionUID = 1L;

						@Override
 						public void onClick(Optional<AjaxRequestTarget> target) {
 							ApplicationPage.logUserOut(getSession());
 							setResponsePage(LoginPage.class);
 						}
                     });
                 }
             });
            }
        });
    }
    
    
}
