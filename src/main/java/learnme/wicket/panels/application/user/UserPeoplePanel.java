package learnme.wicket.panels.application.user;

import org.apache.wicket.model.Model;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.model.StringResourceModel;

import learnme.hibernate.entities.User;
import learnme.wicket.components.card.Card;
import learnme.wicket.components.label.LabelHeader;
import learnme.wicket.components.layout.HorizontalLayout;
import learnme.wicket.components.table.Table;
import learnme.wicket.frontend.definition.EGrid;
import learnme.wicket.panels.BasicPanel;

public class UserPeoplePanel extends BasicPanel {

	private static final String ID_CARD_TABLE = "card-wrapper";
	
	private User user;
	
	public UserPeoplePanel(String id, User user) {
		super(id);
		
		this.user = user;
	}
	
	@Override
	protected void onInitialize() {
		super.onInitialize();
		
		add(new HorizontalLayout(ID_CARD_TABLE) {
			{
				setGrid(EGrid.MEDIUM, 6);
				
				Model<User> model = new Model<>(user);
				
				add(new Card(newId(),
						new Table(Card.ID_CONTENT) {
			
							{
								setBorders(false);														
								setNoRecordsModel(new ResourceModel("UserPeopleNoFollowers"));
																
								for (User user : user.getFollowers()) {
									addRow(new UserLittlePreviewPanel(ID_COMPONENT, user, true, false, false, false));
								}
							}
						
						}, new LabelHeader(Card.ID_HEADER, new StringResourceModel("UserPeopleFollowers", model), 5)));
				
				add(new Card(newId(),
						new Table(Card.ID_CONTENT) {
			
							{
								setBorders(false);														
								setNoRecordsModel(new ResourceModel("UserPeopleNoFollowing"));
								
								for (User user : user.getFollowing()) {
									addRow(new UserLittlePreviewPanel(ID_COMPONENT, user, true));
								}
							}
						
						}, new LabelHeader(Card.ID_HEADER, new StringResourceModel("UserPeopleFollowing", model), 5)));
				}
		});
	}

}
