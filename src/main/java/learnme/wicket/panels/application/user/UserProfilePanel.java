package learnme.wicket.panels.application.user;

import java.util.Optional;
import java.util.Set;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.request.resource.ContextRelativeResource;

import learnme.hibernate.entities.Conversation;
import learnme.hibernate.entities.Message;
import learnme.hibernate.entities.User;
import learnme.wicket.components.button.Button;
import learnme.wicket.components.label.Label;
import learnme.wicket.frontend.definition.EColors;
import learnme.wicket.frontend.definition.EDirection;
import learnme.wicket.frontend.definition.ESpacing;
import learnme.wicket.frontend.definition.ESymbols;
import learnme.wicket.pages.application.ApplicationPage;
import learnme.wicket.pages.application.messages.MessagesPage;
import learnme.wicket.pages.application.users.UserPage;
import learnme.wicket.panels.BasicPanel;

public class UserProfilePanel extends BasicPanel {

	private final static String ID_USER_IMAGE = "user-image";
	private final static String ID_USER_COVER = "user-cover";
    private final static String ID_USER_FULLNAME = "user-fullname";
    private final static String ID_USER_FOLLOW = "user-follow";
    private final static String ID_USER_MESSAGE = "user-message";

    private final static String ID_USER_FOLLOWERS_LABEL = "user-followers";
    private final static String ID_USER_COURSES_LABEL = "user-courses";
    
    private User user;
    private User me;
    private boolean canFollow;
    
    
	public UserProfilePanel(String id, User user) {
		super(id);
		
		this.user = user;
	}

	@Override
	protected void onInitialize() {
		super.onInitialize();
		
		if (getPage() instanceof ApplicationPage) {
			this.me = ((ApplicationPage)getPage()).getUser();
			this.canFollow = canFollow();
		}
		
		add(new Image(ID_USER_IMAGE, new ContextRelativeResource(user.getImage())));
		add(new Image(ID_USER_COVER, new ContextRelativeResource(user.getCover())));
		
		add(new Label(ID_USER_FULLNAME, new PropertyModel<User>(user, "getNameAndSurname")));
		add(new Label(ID_USER_FOLLOWERS_LABEL, new StringResourceModel("UserFollowersLabel", new Model<User>(user))));
		add(new Label(ID_USER_COURSES_LABEL, new StringResourceModel("UserCoursesLabel", new Model<User>(user))));
		 
		add(new Button(ID_USER_MESSAGE, new ResourceModel("UserSendMessageButton"), true) {
			
			{		
				if (me.equals(user)) {
					setVisible(false);
				}
				
				setBackgroundColor(EColors.WARNING);
				addSpacing(ESpacing.MARGIN, 2, EDirection.RIGHT);
			}
			
			@Override
			public void onClick(Optional<AjaxRequestTarget> target) {
				me.refresh();
				
			    Conversation postedConversation = null;
				
				// TODO: MAYBE Set<Conversation> getConversations in Conversation - faster				
				for (Conversation conversation : me.getPosts()) {
					if (conversation.isMessage()) {
						Set<User> participants = conversation.getUsers();
						if (participants.size() == 2) {
							for(User participant : participants){
								if (participant.equals(user)) {
									postedConversation = conversation;
								}
							}
						}
					}
				}
				
				if (postedConversation == null) {
					postedConversation = new Conversation(true);
				    postedConversation.addUser(user);
				    postedConversation.addUser(me);
				}
				

				Message message = new Message();
				message.setUser(me);
				message.setText("POST by PROFILE BUTTON");
				postedConversation.addMessage(message);
				
				postedConversation.persist();
				
				setResponsePage(new MessagesPage(getPage().getPageParameters()));
			}
		});
		
		add(new Button(ID_USER_FOLLOW, new PropertyModel<String>(UserProfilePanel.this, "getModel"), true) {
			
			{		
				if (me.equals(user)) {
					setVisible(false);
				}
				
				if (!canFollow) {
					setBackgroundColor(EColors.DISABLED);
				}
			}
			
			@Override
			public void onClick(Optional<AjaxRequestTarget> target) {
				me.refresh();
				
				if (canFollow) {
					setBackgroundColor(EColors.DISABLED);
					me.addToFollow(user);
				} else {
					setBackgroundColor(EColors.PRIMARY);
					me.removeFromFollow(user);
					
//					TODO: Notification on unfollow? Rather not...
				}
				
				canFollow = !canFollow;
				
				me.persist();
				user.persist();
				if (getPage() instanceof UserPage) {
					setResponsePage(new UserPage(getPage().getPageParameters(), user));
				} else {
					target.get().add(UserProfilePanel.this);					
				}
			}
		});
	}
	
	public String getModel() {
		if(canFollow) {
			return getLocalizer().getString("UserFollowButton", this);
		} else {
			return getLocalizer().getString("UserFollowedButton", this);
		}
	}
	
	private boolean canFollow() {
		user.refresh();
		
		if(!me.equals(user) && !user.getFollowers().contains(me)) {
			return true;
		}
		
		return false;
	}
}
