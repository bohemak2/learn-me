

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import org.apache.wicket.Component;
import org.apache.wicket.util.tester.WicketTester;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import learnme.application.AppConfig;
import learnme.hibernate.entities.User;
import learnme.wicket.WicketApplication;
import learnme.wicket.pages.application.ApplicationPage;
import learnme.wicket.pages.application.dashboard.DashboardPage;
import learnme.wicket.pages.general.home.HomePage;
import learnme.wicket.pages.general.login.LoginPage;

/**
 * Testing pages with WicketTester
 */
public class TestPages
{
	private WicketTester tester;

	@Before
	public void setUp()
	{
		// init tester
		tester = new WicketTester(new WicketApplication() {
			
			@Override
			public void init() {
				super.init();
				
				// Turn on wicket:id for getTagByWicketId
				getMarkupSettings().setStripWicketTags(false);
			}
		});

	}
	
	@After
	public void tearDown(){
		// Clear any side effect occurred during test.
		tester.destroy();
	}
	
	@Test
	public void testUrls()
	{
		// Exists response on these basic urls?
		tester.executeUrl("./");
		tester.executeUrl("./404");
		
		tester.executeUrl("./login");
		tester.executeUrl("./about");
		tester.executeUrl("./contact");
		
		tester.executeUrl("./app");
	}

	@Test
	public void homepageRendersSuccessfully()
	{
		tester.startPage(HomePage.class);

		// Is it really HomePage?
		tester.assertRenderedPage(HomePage.class);
		
	}
	
	@Test
	public void homepageComponents()
	{
		HomePage homePage = tester.startPage(HomePage.class);
		
		// Is HomePage routed as root "/"?
		assertEquals("", homePage.getPageRelativePath());
		
		// Is allowed and visible basic component in HomePage?
		Component c = tester.getComponentFromLastRenderedPage("home-content");
		assertNotNull(c);
		
		tester.assertEnabled("home-content");
		tester.assertVisible("home-content");

		// Home Page must contains at least one Link component
		assertFalse(tester.getTagsByWicketId("link").size() == 0);
	}
	
	@Test
	public void applicationRendersSuccessfully()
	{
		// We can't render ApplicationPage - there must be redirect on Login if no User
		tester.startPage(ApplicationPage.class);
		tester.assertRenderedPage(LoginPage.class);
		
		// Otherwise go to DashboardPage TODO: omit database
		tester.getSession().setAttribute(AppConfig.SESSION_USER_ID, User.getTestUser());
		tester.startPage(ApplicationPage.class);
		tester.assertRenderedPage(DashboardPage.class);
		
	}
}
