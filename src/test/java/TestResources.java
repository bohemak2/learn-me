

import static org.junit.Assert.assertNotNull;

import org.apache.wicket.request.resource.ContextRelativeResource;
import org.apache.wicket.util.tester.WicketTester;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import learnme.wicket.WicketApplication;
import learnme.wicket.components.icon.Icon;
import learnme.wicket.frontend.definition.ESymbols;
import learnme.wicket.interfaces.IIcon;

/**
 * Testing resources with WicketTester
 */
public class TestResources
{
	private WicketTester tester;

	@Before
	public void setUp()
	{ 
		tester = new WicketTester(new WicketApplication() {
			
			@Override
			public void init() {
				super.init();
				
				// Turn on wicket:id for getTagByWicketId
				getMarkupSettings().setStripWicketTags(false);
			}
		}, getClass().getResource("/").getPath() + "../../src/main/webapp");
	}

	@After
	public void tearDown(){
		// Clear any side effect occurred during test.
		tester.destroy();
	}
	
	
	@Test
	public void markupIcon()
	{
		String id = "markup-test";
		// Choose random icon from enum
		IIcon icon = ESymbols.AGENDA;
		Icon component = new Icon(id, icon);
		
		// Start Component
		tester.startComponentInPage(component);
	
		// Now it should contains class string
		assertNotNull(tester.getTagByWicketId(id).getChild("class", icon.getMarkupClass()));
	}
	
	@Test
	public void mainLogo() {
		// We need logo!
		tester.startResource(new ContextRelativeResource("img/logo.png"));
	}
	
}
