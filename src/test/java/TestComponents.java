

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.Optional;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.model.Model;
import org.apache.wicket.util.tester.WicketTester;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import learnme.wicket.WicketApplication;
import learnme.wicket.components.button.Button;
import learnme.wicket.components.label.Label;
import learnme.wicket.components.layout.BasicLayout;
import learnme.wicket.components.layout.HorizontalLayout;
import learnme.wicket.components.layout.VerticalLayout;

/**
 * Testing components with WicketTester
 */
public class TestComponents
{
	private WicketTester tester;

	@Before
	public void setUp()
	{ 
		tester = new WicketTester(new WicketApplication() {
			
			@Override
			public void init() {
				super.init();
				
				// Turn on wicket:id for getTagByWicketId
				getMarkupSettings().setStripWicketTags(false);
			}
		});
	}

	@After
	public void tearDown(){
		// Clear any side effect occurred during test.
		tester.destroy();
	}
	
	
	@Test
	public void buttonComponent()
	{
		// Button settings
		boolean ajax = false;
		String id = "button-test";
		// Button text?
		String text = "Prosty text";
		
		// Button init
		Button component = new Button(id, Model.of(text), ajax) {
			@Override
			public void onClick(Optional<AjaxRequestTarget> target) {}
		};
		
		// Not started yet 
		assertNull(component.get("button"));
		// Start of component
		tester.startComponentInPage(component);
		// Now it has started
		assertNotNull(component.get("button"));
		
		// Button should be visible/enabled
		tester.assertEnabled(id);
		tester.assertVisible(id);
		
		// Path to link
		String pathTo = id + ":button:link";
		// Button should be clickable
		tester.clickLink(pathTo, ajax);
	}
	
	@Test
	public void layoutsComponent()
	{
		// Initialize layout
		String wrapperId = "wrapper-test";
		BasicLayout layout = new BasicLayout(wrapperId);
		VerticalLayout vl = new VerticalLayout(layout.newId());
		HorizontalLayout hl = new HorizontalLayout(layout.newId());
		
		// Add layouts to wrapper
		layout.add(vl);
		layout.add(hl);
		
		// Initialize components
		String addToVerticalText = "Vertical test";
		String addToHorizontalText = "Horizontal test";
		
		// Add to Vertical/Horiznotal
		vl.add(new Label(vl.newId(), Model.of(addToVerticalText)));
		hl.add(new Label(hl.newId(), Model.of(addToHorizontalText)));
		
		
		// Start of component
		tester.startComponentInPage(layout);
		// Button should contains text somewhere
		assertFalse(!tester.getTagByWicketId(wrapperId).getValue().contains(addToVerticalText));
		assertFalse(!tester.getTagByWicketId(wrapperId).getValue().contains(addToHorizontalText));
	}
	
}
