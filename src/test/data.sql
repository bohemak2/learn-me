-- MySQL dump 10.16  Distrib 10.1.29-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: learnme
-- ------------------------------------------------------
-- Server version	10.1.29-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activity`
--

DROP TABLE IF EXISTS `activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activity` (
  `id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `significancy` int(11) NOT NULL DEFAULT '1',
  `type` int(11) DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `participant_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKad1q04q5dxv9lyur6pyighso` (`course_id`),
  KEY `FKhxnmgxk8b3fpbt8pro7mg96e9` (`participant_id`),
  KEY `FKr8wdbwcm475hs03x515cjlftv` (`user_id`),
  CONSTRAINT `FKad1q04q5dxv9lyur6pyighso` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`),
  CONSTRAINT `FKhxnmgxk8b3fpbt8pro7mg96e9` FOREIGN KEY (`participant_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FKr8wdbwcm475hs03x515cjlftv` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activity`
--

LOCK TABLES `activity` WRITE;
/*!40000 ALTER TABLE `activity` DISABLE KEYS */;
INSERT INTO `activity` VALUES (2,'2018-07-28 21:00:35',1,2,NULL,NULL,1),(3,'2018-07-28 21:00:35',1,2,NULL,NULL,1),(4,'2018-07-28 21:00:35',1,2,NULL,NULL,1),(5,'2018-07-28 21:00:35',1,2,NULL,NULL,1),(10,'2018-07-28 21:00:35',1,2,NULL,NULL,9),(11,'2018-07-28 21:00:35',1,2,NULL,NULL,9),(12,'2018-07-28 21:00:35',1,2,NULL,NULL,9),(14,'2018-07-28 21:00:35',1,2,NULL,NULL,13),(20,'2018-07-28 21:00:35',1,2,NULL,NULL,19),(29,'2018-07-28 21:00:35',1,2,NULL,NULL,28),(31,'2018-07-28 21:00:35',1,2,NULL,NULL,30),(37,'2018-07-28 21:00:35',1,2,NULL,NULL,36),(53,'2018-07-28 21:00:35',1,2,NULL,NULL,52),(55,'2018-07-28 21:00:35',1,2,NULL,NULL,54),(56,'2018-07-28 21:00:35',1,2,NULL,NULL,54),(57,'2018-07-28 21:00:35',1,2,NULL,NULL,54),(63,'2018-07-28 21:00:35',1,2,NULL,NULL,62),(75,'2018-07-28 21:00:35',1,2,NULL,NULL,74),(106,'2018-07-28 21:00:35',1,3,NULL,NULL,1),(107,'2018-07-28 21:00:35',1,8,205,NULL,1),(108,'2018-07-28 21:00:35',1,8,165,NULL,1),(109,'2018-07-28 21:00:35',1,8,180,NULL,9),(110,'2018-07-28 21:00:35',1,8,182,NULL,9),(111,'2018-07-28 21:00:35',1,8,165,NULL,9),(113,'2018-07-28 21:00:35',1,8,163,NULL,19),(114,'2018-07-28 21:00:35',1,8,165,NULL,19),(116,'2018-07-28 21:00:35',1,8,139,NULL,52),(117,'2018-07-28 21:00:36',1,8,147,NULL,52),(148,'2018-07-28 21:00:35',1,8,156,NULL,74),(261,'2018-07-28 21:00:37',1,1,NULL,NULL,1);
/*!40000 ALTER TABLE `activity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assignment`
--

DROP TABLE IF EXISTS `assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assignment` (
  `id` int(11) NOT NULL,
  `attempts` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `minimum` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `since` datetime DEFAULT NULL,
  `until` datetime DEFAULT NULL,
  `conversation_id` int(11) DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `sender_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKdy8wvfnxyie5a7mbtscyel5dp` (`conversation_id`),
  KEY `FKrop26uwnbkstbtfha3ormxp85` (`course_id`),
  KEY `FK7hes8meym22rrygettpt8t17y` (`sender_id`),
  CONSTRAINT `FK7hes8meym22rrygettpt8t17y` FOREIGN KEY (`sender_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FKdy8wvfnxyie5a7mbtscyel5dp` FOREIGN KEY (`conversation_id`) REFERENCES `conversation` (`id`),
  CONSTRAINT `FKrop26uwnbkstbtfha3ormxp85` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assignment`
--

LOCK TABLES `assignment` WRITE;
/*!40000 ALTER TABLE `assignment` DISABLE KEYS */;
INSERT INTO `assignment` VALUES (206,0,NULL,0,'Op. 46, 72. - Slavonic Dances','2018-07-28 21:00:35',NULL,NULL,205,1),(216,0,NULL,0,'Op. 95 - New World','2018-08-04 21:00:35',NULL,NULL,205,1),(226,0,NULL,0,'Op. 88 - G Major','2018-08-11 21:00:35',NULL,NULL,205,1),(236,0,NULL,0,'Op. 62 - Homeland','2018-08-18 21:00:35',NULL,NULL,205,1),(246,0,'I would like to say hello and start this course. Presence required!',0,'First session','2018-07-31 21:00:35',NULL,NULL,147,NULL),(256,0,NULL,0,'Coffee Niles + Frasier','2018-08-27 21:00:35',NULL,NULL,NULL,9);
/*!40000 ALTER TABLE `assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assignment_event`
--

DROP TABLE IF EXISTS `assignment_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assignment_event` (
  `id` int(11) NOT NULL,
  `place_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKq6ln1iv0qa97190j47yvojh1x` (`place_id`),
  CONSTRAINT `FKinvjcmd3y3hvcs8r5alx5io3g` FOREIGN KEY (`id`) REFERENCES `assignment` (`id`),
  CONSTRAINT `FKq6ln1iv0qa97190j47yvojh1x` FOREIGN KEY (`place_id`) REFERENCES `place` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assignment_event`
--

LOCK TABLES `assignment_event` WRITE;
/*!40000 ALTER TABLE `assignment_event` DISABLE KEYS */;
INSERT INTO `assignment_event` VALUES (256,NULL),(206,105),(216,105),(226,105),(236,105),(246,105);
/*!40000 ALTER TABLE `assignment_event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assignment_result`
--

DROP TABLE IF EXISTS `assignment_result`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assignment_result` (
  `Assignment_id` int(11) NOT NULL,
  `results_id` int(11) NOT NULL,
  PRIMARY KEY (`Assignment_id`,`results_id`),
  UNIQUE KEY `UK_ndkc7ar2pnv4vploaik1ytwt2` (`results_id`),
  CONSTRAINT `FK3fbt8qny1qlk3pl38uv3oyexr` FOREIGN KEY (`Assignment_id`) REFERENCES `assignment` (`id`),
  CONSTRAINT `FKe3nilspjr0p3ls638h91tabo7` FOREIGN KEY (`results_id`) REFERENCES `result` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assignment_result`
--

LOCK TABLES `assignment_result` WRITE;
/*!40000 ALTER TABLE `assignment_result` DISABLE KEYS */;
INSERT INTO `assignment_result` VALUES (206,207),(206,214),(206,215),(216,217),(216,224),(216,225),(226,227),(226,234),(226,235),(236,237),(236,244),(236,245),(246,247),(246,254),(246,255),(256,257),(256,260);
/*!40000 ALTER TABLE `assignment_result` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assignment_task`
--

DROP TABLE IF EXISTS `assignment_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assignment_task` (
  `file` tinyblob,
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FKnqmrhxh6xb6rvweeye3qqb6kp` FOREIGN KEY (`id`) REFERENCES `assignment` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assignment_task`
--

LOCK TABLES `assignment_task` WRITE;
/*!40000 ALTER TABLE `assignment_task` DISABLE KEYS */;
/*!40000 ALTER TABLE `assignment_task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assignment_test`
--

DROP TABLE IF EXISTS `assignment_test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assignment_test` (
  `random` bit(1) NOT NULL,
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FKdmeeqo47b763pjq8v8nj0c2dp` FOREIGN KEY (`id`) REFERENCES `assignment` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assignment_test`
--

LOCK TABLES `assignment_test` WRITE;
/*!40000 ALTER TABLE `assignment_test` DISABLE KEYS */;
/*!40000 ALTER TABLE `assignment_test` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chapter`
--

DROP TABLE IF EXISTS `chapter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chapter` (
  `id` int(11) NOT NULL,
  `active` bit(1) NOT NULL DEFAULT b'1',
  `description` varchar(300) DEFAULT NULL,
  `minutes` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `position` int(11) NOT NULL,
  `since` datetime DEFAULT NULL,
  `until` datetime DEFAULT NULL,
  `author_id` int(11) DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKlpapojmul7pfcmbuj8htqghjl` (`author_id`),
  KEY `FKhhaina8rg7bpmg1qesiluu8vu` (`course_id`),
  CONSTRAINT `FKhhaina8rg7bpmg1qesiluu8vu` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`),
  CONSTRAINT `FKlpapojmul7pfcmbuj8htqghjl` FOREIGN KEY (`author_id`) REFERENCES `course_tutor` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chapter`
--

LOCK TABLES `chapter` WRITE;
/*!40000 ALTER TABLE `chapter` DISABLE KEYS */;
INSERT INTO `chapter` VALUES (140,'','In this chapter, I will show you some basic excercises.',30,'Basics of fitness',0,NULL,NULL,138,139),(142,'','By warm up body hould starts each excercise.',15,'Warming up',0,NULL,NULL,138,139);
/*!40000 ALTER TABLE `chapter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content`
--

DROP TABLE IF EXISTS `content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content` (
  `id` int(11) NOT NULL,
  `active` bit(1) NOT NULL DEFAULT b'1',
  `name` varchar(255) DEFAULT NULL,
  `position` int(11) NOT NULL,
  `chapter_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKyc1ks2ryq8nrn5stp1e9pbp5` (`chapter_id`),
  CONSTRAINT `FKyc1ks2ryq8nrn5stp1e9pbp5` FOREIGN KEY (`chapter_id`) REFERENCES `chapter` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content`
--

LOCK TABLES `content` WRITE;
/*!40000 ALTER TABLE `content` DISABLE KEYS */;
INSERT INTO `content` VALUES (141,'',NULL,0,140),(143,'',NULL,0,142),(144,'',NULL,0,142),(145,'',NULL,0,142);
/*!40000 ALTER TABLE `content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_html`
--

DROP TABLE IF EXISTS `content_html`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_html` (
  `html` varchar(255) DEFAULT NULL,
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FKsnb6ghuuvpkkdj3gvdup8udj6` FOREIGN KEY (`id`) REFERENCES `content` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_html`
--

LOCK TABLES `content_html` WRITE;
/*!40000 ALTER TABLE `content_html` DISABLE KEYS */;
INSERT INTO `content_html` VALUES ('<p>First of all, you have to control your <b>body</b>.</p>',141),('<p>Warm <b>hands</b> and <b>legs</b>.</p>',143),('<p>Rotate with <b>head</b> and <b>body</b>.</p>',144);
/*!40000 ALTER TABLE `content_html` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_pdf`
--

DROP TABLE IF EXISTS `content_pdf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_pdf` (
  `url` varchar(255) DEFAULT NULL,
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FKmyqim8iyksl1oahs7tvt35u7n` FOREIGN KEY (`id`) REFERENCES `content` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_pdf`
--

LOCK TABLES `content_pdf` WRITE;
/*!40000 ALTER TABLE `content_pdf` DISABLE KEYS */;
/*!40000 ALTER TABLE `content_pdf` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_video`
--

DROP TABLE IF EXISTS `content_video`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_video` (
  `url` varchar(255) DEFAULT NULL,
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FK2anuifdxpahsfaocr4y7mm2nk` FOREIGN KEY (`id`) REFERENCES `content` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_video`
--

LOCK TABLES `content_video` WRITE;
/*!40000 ALTER TABLE `content_video` DISABLE KEYS */;
INSERT INTO `content_video` VALUES ('/upload/videos/warming.mp4',145);
/*!40000 ALTER TABLE `content_video` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conversation`
--

DROP TABLE IF EXISTS `conversation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conversation` (
  `id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `message` bit(1) NOT NULL DEFAULT b'0',
  `name` varchar(255) DEFAULT NULL,
  `replies` bit(1) NOT NULL DEFAULT b'1',
  `course_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKnwuel0cmwyk1vo6il3u1yh9kr` (`course_id`),
  CONSTRAINT `FKnwuel0cmwyk1vo6il3u1yh9kr` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conversation`
--

LOCK TABLES `conversation` WRITE;
/*!40000 ALTER TABLE `conversation` DISABLE KEYS */;
INSERT INTO `conversation` VALUES (118,'2018-07-28 21:00:35','\0',NULL,'',NULL),(120,'2018-07-28 21:00:35','\0',NULL,'',NULL),(135,'2018-07-28 21:00:35','\0',NULL,'',NULL),(153,'2018-07-28 21:00:35','\0',NULL,'',NULL),(172,'2018-07-28 21:00:35','\0',NULL,'',NULL),(176,'2018-07-28 21:00:35','\0',NULL,'',NULL),(184,'2018-07-28 21:00:35','\0',NULL,'',NULL),(187,'2018-07-28 21:00:35','\0',NULL,'',NULL),(190,'2018-07-28 21:00:35','',NULL,'',NULL);
/*!40000 ALTER TABLE `conversation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conversation_conversation_message`
--

DROP TABLE IF EXISTS `conversation_conversation_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conversation_conversation_message` (
  `Conversation_id` int(11) NOT NULL,
  `messages_id` int(11) NOT NULL,
  PRIMARY KEY (`Conversation_id`,`messages_id`),
  UNIQUE KEY `UK_mihv7thfn1wn1sghuq7lsomvg` (`messages_id`),
  CONSTRAINT `FK64ephs3c6t804knsvki0l27sq` FOREIGN KEY (`Conversation_id`) REFERENCES `conversation` (`id`),
  CONSTRAINT `FK6qhur0efhu31wxb6g0xjup1fl` FOREIGN KEY (`messages_id`) REFERENCES `conversation_message` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conversation_conversation_message`
--

LOCK TABLES `conversation_conversation_message` WRITE;
/*!40000 ALTER TABLE `conversation_conversation_message` DISABLE KEYS */;
INSERT INTO `conversation_conversation_message` VALUES (118,119),(120,121),(120,122),(120,125),(120,126),(120,127),(120,128),(135,136),(135,137),(153,154),(172,173),(172,174),(172,175),(176,177),(184,185),(184,186),(187,188),(187,189),(190,191),(190,193),(190,195),(190,197),(190,199),(190,201);
/*!40000 ALTER TABLE `conversation_conversation_message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conversation_message`
--

DROP TABLE IF EXISTS `conversation_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conversation_message` (
  `id` int(11) NOT NULL,
  `file` tinyblob,
  `sent` datetime DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  `conversation_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKhh4tgo7pncgc8xchsutwtvvg7` (`conversation_id`),
  KEY `FK2ejdheps735a131j72ajh9ai3` (`user_id`),
  CONSTRAINT `FK2ejdheps735a131j72ajh9ai3` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FKhh4tgo7pncgc8xchsutwtvvg7` FOREIGN KEY (`conversation_id`) REFERENCES `conversation` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conversation_message`
--

LOCK TABLES `conversation_message` WRITE;
/*!40000 ALTER TABLE `conversation_message` DISABLE KEYS */;
INSERT INTO `conversation_message` VALUES (7,NULL,'2018-07-28 21:00:35','Wellcome in LearnMe! \n We hope that you will find this educational social network usefull and it brings you a lot of joy. \n With best, Creators.',NULL,NULL),(16,NULL,'2018-07-28 21:00:35','Wellcome in LearnMe! \n We hope that you will find this educational social network usefull and it brings you a lot of joy. \n With best, Creators.',NULL,NULL),(22,NULL,'2018-07-28 21:00:35','Wellcome in LearnMe! \n We hope that you will find this educational social network usefull and it brings you a lot of joy. \n With best, Creators.',NULL,NULL),(26,NULL,'2018-07-28 21:00:35','Wellcome in LearnMe! \n We hope that you will find this educational social network usefull and it brings you a lot of joy. \n With best, Creators.',NULL,NULL),(33,NULL,'2018-07-28 21:00:35','Wellcome in LearnMe! \n We hope that you will find this educational social network usefull and it brings you a lot of joy. \n With best, Creators.',NULL,NULL),(39,NULL,'2018-07-28 21:00:35','Wellcome in LearnMe! \n We hope that you will find this educational social network usefull and it brings you a lot of joy. \n With best, Creators.',NULL,NULL),(43,NULL,'2018-07-28 21:00:35','Wellcome in LearnMe! \n We hope that you will find this educational social network usefull and it brings you a lot of joy. \n With best, Creators.',NULL,NULL),(47,NULL,'2018-07-28 21:00:35','Wellcome in LearnMe! \n We hope that you will find this educational social network usefull and it brings you a lot of joy. \n With best, Creators.',NULL,NULL),(59,NULL,'2018-07-28 21:00:35','Wellcome in LearnMe! \n We hope that you will find this educational social network usefull and it brings you a lot of joy. \n With best, Creators.',NULL,NULL),(65,NULL,'2018-07-28 21:00:35','Wellcome in LearnMe! \n We hope that you will find this educational social network usefull and it brings you a lot of joy. \n With best, Creators.',NULL,NULL),(71,NULL,'2018-07-28 21:00:35','Wellcome in LearnMe! \n We hope that you will find this educational social network usefull and it brings you a lot of joy. \n With best, Creators.',NULL,NULL),(77,NULL,'2018-07-28 21:00:35','Wellcome in LearnMe! \n We hope that you will find this educational social network usefull and it brings you a lot of joy. \n With best, Creators.',NULL,NULL),(81,NULL,'2018-07-28 21:00:35','Wellcome in LearnMe! \n We hope that you will find this educational social network usefull and it brings you a lot of joy. \n With best, Creators.',NULL,NULL),(86,NULL,'2018-07-28 21:00:35','Wellcome in LearnMe! \n We hope that you will find this educational social network usefull and it brings you a lot of joy. \n With best, Creators.',NULL,NULL),(95,NULL,'2018-07-28 21:00:35','Wellcome in LearnMe! \n We hope that you will find this educational social network usefull and it brings you a lot of joy. \n With best, Creators.',NULL,NULL),(99,NULL,'2018-07-28 21:00:35','Wellcome in LearnMe! \n We hope that you will find this educational social network usefull and it brings you a lot of joy. \n With best, Creators.',NULL,NULL),(103,NULL,'2018-07-28 21:00:35','Wellcome in LearnMe! \n We hope that you will find this educational social network usefull and it brings you a lot of joy. \n With best, Creators.',NULL,NULL),(119,NULL,'2018-07-28 21:00:35','Comic-Con in Vegas just in next month! I can\'t wait...',118,62),(121,NULL,'2018-07-28 21:00:35','Is anyone planning something tonight?',120,54),(122,NULL,'2018-07-28 21:00:35','I am going to theatre with Niles today. Join us!',120,30),(125,NULL,'2018-07-28 21:00:35','What are you going to see?',120,54),(126,NULL,'2018-07-28 21:00:35','La Fille Mal Gardée, it is a ballet.',120,30),(127,NULL,'2018-07-28 21:00:35','No, thanks :-) I would rather be at home.',120,54),(128,NULL,'2018-07-28 21:00:35','Come to my place.',120,52),(136,NULL,'2018-07-28 21:00:35','I have already started some interesting fitness courses. You should check it now until they are free...',135,52),(137,NULL,'2018-07-28 21:00:35','Looks good, thanks for echo.',135,30),(154,NULL,'2018-07-28 21:00:35','Visited Flavour of Greek restaurant. They totally deserves this name! 1/5...',153,74),(173,NULL,'2018-07-28 21:00:35','I am posting interesting topic each time...',172,9),(174,NULL,'2018-07-28 21:00:35','I don\'t thing.',172,28),(175,NULL,'2018-07-28 21:00:35','Yes, it is rubish.',172,51),(177,NULL,'2018-07-28 21:00:35','Tommorow, I am starting Wine sommelier course. Anyone else to join?',176,9),(185,NULL,'2018-07-28 21:00:35','I was in Metropolitan this weekend. It was awesome. Everyone should see it!',184,1),(186,NULL,'2018-07-28 21:00:35','You have never been in Prado, haven\'t you? Then you couldn\'t say that.',184,1),(188,NULL,'2018-07-28 21:00:35','Me, Frasier and Lilith prepared course of Psychology.',187,1),(189,NULL,'2018-07-28 21:00:35','Yes, it is a quick brief into Psychology. Very usefull.',187,9),(191,NULL,'2018-07-28 21:00:35','Hi Niles. Wouldn\'t you mind of taking my wine course?',190,9),(193,NULL,'2018-07-28 21:00:35','Why should I do that? You hardly improve my knowledge about wine...',190,1),(195,NULL,'2018-07-28 21:00:35','Ok, never mind. Anyway, which chapter will you take for Lilith\'s philosophy course?',190,9),(197,NULL,'2018-07-28 21:00:35','I was thinking about Jung... And you?',190,1),(199,NULL,'2018-07-28 21:00:35','Freud...',190,9),(201,NULL,'2018-07-28 21:00:35','Then it is settled.',190,1),(209,NULL,'2018-07-28 21:00:36','Dvorak\'s piano,\n Op. 46, 72. - Slavonic Dances,\n 28 Jul 2018 13:00:35 GMT,\n Seattle.',NULL,NULL),(211,NULL,'2018-07-28 21:00:36','Dvorak\'s piano,\n Op. 46, 72. - Slavonic Dances,\n 28 Jul 2018 13:00:35 GMT,\n Seattle.',NULL,NULL),(213,NULL,'2018-07-28 21:00:36','Dvorak\'s piano,\n Op. 46, 72. - Slavonic Dances,\n 28 Jul 2018 13:00:35 GMT,\n Seattle.',NULL,NULL),(219,NULL,'2018-07-28 21:00:36','Dvorak\'s piano,\n Op. 95 - New World,\n 4 Aug 2018 13:00:35 GMT,\n Seattle.',NULL,NULL),(221,NULL,'2018-07-28 21:00:36','Dvorak\'s piano,\n Op. 95 - New World,\n 4 Aug 2018 13:00:35 GMT,\n Seattle.',NULL,NULL),(223,NULL,'2018-07-28 21:00:36','Dvorak\'s piano,\n Op. 95 - New World,\n 4 Aug 2018 13:00:35 GMT,\n Seattle.',NULL,NULL),(229,NULL,'2018-07-28 21:00:36','Dvorak\'s piano,\n Op. 88 - G Major,\n 11 Aug 2018 13:00:35 GMT,\n Seattle.',NULL,NULL),(231,NULL,'2018-07-28 21:00:36','Dvorak\'s piano,\n Op. 88 - G Major,\n 11 Aug 2018 13:00:35 GMT,\n Seattle.',NULL,NULL),(233,NULL,'2018-07-28 21:00:36','Dvorak\'s piano,\n Op. 88 - G Major,\n 11 Aug 2018 13:00:35 GMT,\n Seattle.',NULL,NULL),(239,NULL,'2018-07-28 21:00:36','Dvorak\'s piano,\n Op. 62 - Homeland,\n 18 Aug 2018 13:00:35 GMT,\n Seattle.',NULL,NULL),(241,NULL,'2018-07-28 21:00:36','Dvorak\'s piano,\n Op. 62 - Homeland,\n 18 Aug 2018 13:00:35 GMT,\n Seattle.',NULL,NULL),(243,NULL,'2018-07-28 21:00:36','Dvorak\'s piano,\n Op. 62 - Homeland,\n 18 Aug 2018 13:00:35 GMT,\n Seattle.',NULL,NULL),(249,NULL,'2018-07-28 21:00:36','One hand Basketball\'s tricks,\n First session,\n 31 Jul 2018 13:00:35 GMT,\n Seattle.',NULL,NULL),(251,NULL,'2018-07-28 21:00:36','One hand Basketball\'s tricks,\n First session,\n 31 Jul 2018 13:00:35 GMT,\n Seattle.',NULL,NULL),(253,NULL,'2018-07-28 21:00:36','One hand Basketball\'s tricks,\n First session,\n 31 Jul 2018 13:00:35 GMT,\n Seattle.',NULL,NULL),(259,NULL,'2018-07-28 21:00:36','Coffee Niles + Frasier,\n 27 Aug 2018 13:00:35 GMT.',NULL,NULL);
/*!40000 ALTER TABLE `conversation_message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conversation_user`
--

DROP TABLE IF EXISTS `conversation_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conversation_user` (
  `conversation` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  PRIMARY KEY (`conversation`,`user`),
  KEY `FKafwg8w8elgeywtj02db8ac981` (`user`),
  CONSTRAINT `FKafwg8w8elgeywtj02db8ac981` FOREIGN KEY (`user`) REFERENCES `user` (`id`),
  CONSTRAINT `FKonomx25i5y11o4x2yu6pvhvy7` FOREIGN KEY (`conversation`) REFERENCES `conversation` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conversation_user`
--

LOCK TABLES `conversation_user` WRITE;
/*!40000 ALTER TABLE `conversation_user` DISABLE KEYS */;
INSERT INTO `conversation_user` VALUES (118,62),(120,54),(135,52),(153,74),(172,9),(176,9),(184,1),(187,1),(190,1),(190,9);
/*!40000 ALTER TABLE `conversation_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course`
--

DROP TABLE IF EXISTS `course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course` (
  `id` int(11) NOT NULL,
  `active` bit(1) NOT NULL DEFAULT b'0',
  `autoacceptance` bit(1) NOT NULL DEFAULT b'0',
  `capacity` int(11) NOT NULL DEFAULT '10',
  `cover` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `currency` int(11) DEFAULT NULL,
  `description` varchar(300) DEFAULT NULL,
  `fee` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `presence` bit(1) NOT NULL,
  `since` datetime DEFAULT NULL,
  `until` datetime DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL,
  `tags_name` varchar(255) DEFAULT NULL,
  `tags_field_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKrwo7afdgjbouh3em9xmfxnjx1` (`owner_id`),
  KEY `FKpiig484ch70yor040wxtiqn35` (`tags_name`,`tags_field_name`),
  CONSTRAINT `FKpiig484ch70yor040wxtiqn35` FOREIGN KEY (`tags_name`, `tags_field_name`) REFERENCES `tag` (`name`, `field_name`),
  CONSTRAINT `FKrwo7afdgjbouh3em9xmfxnjx1` FOREIGN KEY (`owner_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course`
--

LOCK TABLES `course` WRITE;
/*!40000 ALTER TABLE `course` DISABLE KEYS */;
INSERT INTO `course` VALUES (139,'','',1,NULL,'2018-07-28 21:00:35',NULL,'Do you want to be slim and fit? It is easy with me. Just enroll this course and after 14 days you will suit to swimming clothes.',0,'Fitness coaching','\0',NULL,NULL,52,NULL,NULL),(147,'','',15,NULL,'2018-07-28 21:00:36',NULL,'Like LeBron James or Shaq - be the best and charm your friends.',0,'One hand Basketball\'s tricks','\0',NULL,NULL,52,NULL,NULL),(156,'','',10,NULL,'2018-07-28 21:00:35',1,'Prepare your tongue for ecstasy! I will show you the best technics for cooking. You will never go to restaurant again.',20,'The 10 best tricks in Cooking','\0',NULL,NULL,74,NULL,NULL),(163,'','',5,NULL,'2018-07-28 21:00:35',NULL,'Introduction to psychology. Take first steps into magic of human\'s mind. Now without fee!',0,'Psychology I.','\0',NULL,NULL,19,NULL,NULL),(165,'','',10,NULL,'2018-07-28 21:00:35',1,'Intensive course of intermediate psychology with Dr. Sternin et al. Make one the most decisions in your life - Freud or Jung?',200,'Psychology II.','\0',NULL,NULL,19,NULL,NULL),(180,'','',3,NULL,'2018-07-28 21:00:35',NULL,'Nothing is more important than our life and pleasures that brings. I am offering one of the pearl of life\'s joys - wine courses.',0,'Wine sommelier','\0',NULL,NULL,9,NULL,NULL),(182,'','',3,NULL,'2018-07-28 21:00:35',1,'King\'s game. Control the board and impress your friends.',30,'Chess','\0',NULL,NULL,9,NULL,NULL),(205,'','',3,NULL,'2018-07-28 21:00:35',1,'Beauty of Bohemian virtuous. Take a moment to appreciate his best opuses. Ability of piano control is not required.',120,'Dvorak\'s piano','\0',NULL,NULL,1,NULL,NULL);
/*!40000 ALTER TABLE `course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course_student`
--

DROP TABLE IF EXISTS `course_student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course_student` (
  `id` int(11) NOT NULL,
  `achieved` datetime DEFAULT NULL,
  `active` bit(1) NOT NULL DEFAULT b'1',
  `registered` datetime DEFAULT NULL,
  `value` int(11) NOT NULL,
  `course_id` int(11) DEFAULT NULL,
  `ratedBy_id` int(11) DEFAULT NULL,
  `review_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKlmj50qx9k98b7li5li74nnylb` (`course_id`),
  KEY `FK9vkah5xehp2v0alorb2are54w` (`ratedBy_id`),
  KEY `FK8jm0305rd1469k0wwydbfuwnv` (`review_id`),
  KEY `FK9rbx2um6e21kwknwmkt9glol6` (`user_id`),
  CONSTRAINT `FK8jm0305rd1469k0wwydbfuwnv` FOREIGN KEY (`review_id`) REFERENCES `review` (`id`),
  CONSTRAINT `FK9rbx2um6e21kwknwmkt9glol6` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK9vkah5xehp2v0alorb2are54w` FOREIGN KEY (`ratedBy_id`) REFERENCES `course_tutor` (`id`),
  CONSTRAINT `FKlmj50qx9k98b7li5li74nnylb` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course_student`
--

LOCK TABLES `course_student` WRITE;
/*!40000 ALTER TABLE `course_student` DISABLE KEYS */;
INSERT INTO `course_student` VALUES (112,NULL,'','2018-07-28 21:00:35',0,182,NULL,NULL,13),(123,NULL,'','2018-07-28 21:00:35',0,182,NULL,NULL,28),(124,NULL,'','2018-07-28 21:00:35',0,139,NULL,NULL,30),(129,NULL,'','2018-07-28 21:00:35',0,205,NULL,NULL,54),(130,NULL,'','2018-07-28 21:00:35',0,180,NULL,NULL,54),(150,NULL,'','2018-07-28 21:00:35',0,205,NULL,NULL,84),(151,NULL,'','2018-07-28 21:00:35',0,156,NULL,NULL,84),(157,NULL,'','2018-07-28 21:00:35',0,156,NULL,NULL,51),(158,NULL,'','2018-07-28 21:00:35',0,163,NULL,NULL,51),(159,NULL,'','2018-07-28 21:00:35',0,165,NULL,NULL,51),(160,NULL,'','2018-07-28 21:00:36',0,147,NULL,NULL,51),(178,NULL,'','2018-07-28 21:00:36',0,147,NULL,NULL,9),(203,NULL,'','2018-07-28 21:00:36',0,147,NULL,NULL,1);
/*!40000 ALTER TABLE `course_student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course_tag`
--

DROP TABLE IF EXISTS `course_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course_tag` (
  `Course_id` int(11) NOT NULL,
  `tags_name` varchar(255) NOT NULL,
  `tags_field_name` varchar(255) NOT NULL,
  PRIMARY KEY (`Course_id`,`tags_name`,`tags_field_name`),
  KEY `FKg3leahsyisjy1i068wkv31018` (`tags_name`,`tags_field_name`),
  CONSTRAINT `FK7cc75f0m1931hemdsjwyb7kwl` FOREIGN KEY (`Course_id`) REFERENCES `course` (`id`),
  CONSTRAINT `FKg3leahsyisjy1i068wkv31018` FOREIGN KEY (`tags_name`, `tags_field_name`) REFERENCES `tag` (`name`, `field_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course_tag`
--

LOCK TABLES `course_tag` WRITE;
/*!40000 ALTER TABLE `course_tag` DISABLE KEYS */;
INSERT INTO `course_tag` VALUES (139,'Fitness','Sport'),(139,'Loosing weight','Sport'),(147,'Basketball','Sport'),(147,'Basketball Tricks','Skills'),(156,'Cooking','Skills'),(156,'Tricks in Kitchen','Food'),(163,'Introduction','Psychology'),(165,'C. G. Jung','Psychology'),(165,'Sigmund Freud','Psychology'),(180,'Wine','Food'),(182,'Chess','Skills'),(182,'Chess','Sport'),(205,'Antonin Dvorak','Music'),(205,'Piano','Music');
/*!40000 ALTER TABLE `course_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course_tutor`
--

DROP TABLE IF EXISTS `course_tutor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course_tutor` (
  `id` int(11) NOT NULL,
  `active` bit(1) NOT NULL DEFAULT b'1',
  `registered` datetime DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK8igqax88aax7ne87qm5coeta5` (`course_id`),
  KEY `FK3ci1det2q6wuf8i66oquuas5` (`user_id`),
  CONSTRAINT `FK3ci1det2q6wuf8i66oquuas5` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK8igqax88aax7ne87qm5coeta5` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course_tutor`
--

LOCK TABLES `course_tutor` WRITE;
/*!40000 ALTER TABLE `course_tutor` DISABLE KEYS */;
INSERT INTO `course_tutor` VALUES (138,'','2018-07-28 21:00:35',139,52),(146,'','2018-07-28 21:00:36',147,52),(155,'','2018-07-28 21:00:35',156,74),(162,'','2018-07-28 21:00:35',163,19),(164,'','2018-07-28 21:00:35',165,19),(166,'','2018-07-28 21:00:35',165,9),(167,'','2018-07-28 21:00:35',165,1),(179,'','2018-07-28 21:00:35',180,9),(181,'','2018-07-28 21:00:35',182,9),(204,'','2018-07-28 21:00:35',205,1);
/*!40000 ALTER TABLE `course_tutor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field`
--

DROP TABLE IF EXISTS `field`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field` (
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field`
--

LOCK TABLES `field` WRITE;
/*!40000 ALTER TABLE `field` DISABLE KEYS */;
INSERT INTO `field` VALUES ('Food'),('Music'),('Psychology'),('Skills'),('Sport');
/*!40000 ALTER TABLE `field` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` VALUES (262),(262),(262),(262),(262),(262),(262),(262),(262),(262),(262),(262),(262),(262),(262);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notification`
--

DROP TABLE IF EXISTS `notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notification` (
  `id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `seen` datetime DEFAULT NULL,
  `target` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `message_id` int(11) DEFAULT NULL,
  `sender_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK3n6s3hbu1fj6636w68m5nokrp` (`message_id`),
  KEY `FKnbt1hengkgjqru2q44q8rlc2c` (`sender_id`),
  KEY `FKb0yvoep4h4k92ipon31wmdf7e` (`user_id`),
  CONSTRAINT `FK3n6s3hbu1fj6636w68m5nokrp` FOREIGN KEY (`message_id`) REFERENCES `conversation_message` (`id`),
  CONSTRAINT `FKb0yvoep4h4k92ipon31wmdf7e` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FKnbt1hengkgjqru2q44q8rlc2c` FOREIGN KEY (`sender_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notification`
--

LOCK TABLES `notification` WRITE;
/*!40000 ALTER TABLE `notification` DISABLE KEYS */;
INSERT INTO `notification` VALUES (6,'2018-07-28 21:00:35','Wellcome in LearnMe! \n We hope that you will find this educational social net...',NULL,7,NULL,7,1,1),(8,'2018-07-28 21:00:35','Since now, I am your new follower!',NULL,4,NULL,NULL,9,1),(15,'2018-07-28 21:00:35','Wellcome in LearnMe! \n We hope that you will find this educational social net...',NULL,7,NULL,16,13,13),(17,'2018-07-28 21:00:35','Since now, I am your new follower!',NULL,4,NULL,NULL,9,13),(21,'2018-07-28 21:00:35','Wellcome in LearnMe! \n We hope that you will find this educational social net...',NULL,7,NULL,22,19,19),(23,'2018-07-28 21:00:35','Since now, I am your new follower!',NULL,4,NULL,NULL,9,19),(25,'2018-07-28 21:00:35','Wellcome in LearnMe! \n We hope that you will find this educational social net...',NULL,7,NULL,26,9,9),(27,'2018-07-28 21:00:35','Since now, I am your new follower!',NULL,4,NULL,NULL,28,9),(32,'2018-07-28 21:00:35','Wellcome in LearnMe! \n We hope that you will find this educational social net...',NULL,7,NULL,33,30,30),(34,'2018-07-28 21:00:35','Since now, I am your new follower!',NULL,4,NULL,NULL,28,30),(38,'2018-07-28 21:00:35','Wellcome in LearnMe! \n We hope that you will find this educational social net...',NULL,7,NULL,39,36,36),(40,'2018-07-28 21:00:35','Since now, I am your new follower!',NULL,4,NULL,NULL,28,36),(42,'2018-07-28 21:00:35','Wellcome in LearnMe! \n We hope that you will find this educational social net...',NULL,7,NULL,43,28,28),(44,'2018-07-28 21:00:35','Since now, I am your new follower!',NULL,4,NULL,NULL,45,28),(46,'2018-07-28 21:00:35','Wellcome in LearnMe! \n We hope that you will find this educational social net...',NULL,7,NULL,47,45,45),(50,'2018-07-28 21:00:35','Since now, I am your new follower!',NULL,4,NULL,NULL,51,9),(58,'2018-07-28 21:00:35','Wellcome in LearnMe! \n We hope that you will find this educational social net...',NULL,7,NULL,59,54,54),(60,'2018-07-28 21:00:35','Since now, I am your new follower!',NULL,4,NULL,NULL,52,54),(61,'2018-07-28 21:00:35','Since now, I am your new follower!',NULL,4,NULL,NULL,62,54),(64,'2018-07-28 21:00:35','Wellcome in LearnMe! \n We hope that you will find this educational social net...',NULL,7,NULL,65,62,62),(66,'2018-07-28 21:00:35','Since now, I am your new follower!',NULL,4,NULL,NULL,51,62),(68,'2018-07-28 21:00:35','Since now, I am your new follower!',NULL,4,NULL,NULL,51,54),(70,'2018-07-28 21:00:35','Wellcome in LearnMe! \n We hope that you will find this educational social net...',NULL,7,NULL,71,52,52),(72,'2018-07-28 21:00:35','Since now, I am your new follower!',NULL,4,NULL,NULL,51,52),(76,'2018-07-28 21:00:35','Wellcome in LearnMe! \n We hope that you will find this educational social net...',NULL,7,NULL,77,74,74),(78,'2018-07-28 21:00:35','Since now, I am your new follower!',NULL,4,NULL,NULL,51,74),(80,'2018-07-28 21:00:35','Wellcome in LearnMe! \n We hope that you will find this educational social net...',NULL,7,NULL,81,51,51),(83,'2018-07-28 21:00:35','Since now, I am your new follower!',NULL,4,NULL,NULL,84,9),(85,'2018-07-28 21:00:35','Wellcome in LearnMe! \n We hope that you will find this educational social net...',NULL,7,NULL,86,84,84),(89,'2018-07-28 21:00:35','Since now, I am your new follower!',NULL,4,NULL,NULL,30,1),(90,'2018-07-28 21:00:35','Since now, I am your new follower!',NULL,4,NULL,NULL,28,1),(91,'2018-07-28 21:00:35','Since now, I am your new follower!',NULL,4,NULL,NULL,51,1),(94,'2018-07-28 21:00:35','Wellcome in LearnMe! \n We hope that you will find this educational social net...',NULL,7,NULL,95,93,93),(98,'2018-07-28 21:00:35','Wellcome in LearnMe! \n We hope that you will find this educational social net...',NULL,7,NULL,99,97,97),(102,'2018-07-28 21:00:35','Wellcome in LearnMe! \n We hope that you will find this educational social net...',NULL,7,NULL,103,101,101),(115,'2018-07-28 21:00:35','I have just enrolled Psychology I. course!',NULL,0,0,NULL,51,19),(131,'2018-07-28 21:00:35','I have just enrolled Fitness coaching course!',NULL,0,0,NULL,30,52),(132,'2018-07-28 21:00:36','I have just enrolled One hand Basketball\'s tricks course!',NULL,0,0,NULL,1,52),(133,'2018-07-28 21:00:36','I have just enrolled One hand Basketball\'s tricks course!',NULL,0,0,NULL,51,52),(134,'2018-07-28 21:00:36','I have just enrolled One hand Basketball\'s tricks course!',NULL,0,0,NULL,9,52),(149,'2018-07-28 21:00:35','I have just enrolled The 10 best tricks in Cooking course!',NULL,0,0,NULL,84,74),(152,'2018-07-28 21:00:35','I have just enrolled The 10 best tricks in Cooking course!',NULL,0,0,NULL,51,74),(161,'2018-07-28 21:00:35','I have just enrolled Psychology II. course!',NULL,0,0,NULL,51,19),(168,'2018-07-28 21:00:35','I have just enrolled Wine sommelier course!',NULL,0,0,NULL,54,9),(169,'2018-07-28 21:00:35','I have just enrolled Chess course!',NULL,0,0,NULL,13,9),(170,'2018-07-28 21:00:35','I have just enrolled Chess course!',NULL,0,0,NULL,28,9),(171,'2018-07-28 21:00:35','I have just enrolled Psychology II. course!',NULL,0,0,NULL,51,9),(183,'2018-07-28 21:00:35','I have just enrolled Psychology II. course!',NULL,0,0,NULL,51,1),(192,'2018-07-28 21:00:35','Hi Niles. Wouldn\'t you mind of taking my wine course?',NULL,2,NULL,191,9,1),(194,'2018-07-28 21:00:35','Why should I do that? You hardly improve my knowledge about wine...',NULL,2,NULL,193,1,9),(196,'2018-07-28 21:00:35','Ok, never mind. Anyway, which chapter will you take for Lilith\'s philosophy c...',NULL,2,NULL,195,9,1),(198,'2018-07-28 21:00:35','I was thinking about Jung... And you?',NULL,2,NULL,197,1,9),(200,'2018-07-28 21:00:35','Freud...',NULL,2,NULL,199,9,1),(202,'2018-07-28 21:00:35','Then it is settled.',NULL,2,NULL,201,1,9),(208,'2018-07-28 21:00:36','Op. 46, 72. - Slavonic Dances',NULL,6,0,209,1,54),(210,'2018-07-28 21:00:36','Op. 46, 72. - Slavonic Dances',NULL,6,0,211,1,84),(212,'2018-07-28 21:00:36','Op. 46, 72. - Slavonic Dances',NULL,6,0,213,1,1),(218,'2018-07-28 21:00:36','Op. 95 - New World',NULL,6,0,219,1,54),(220,'2018-07-28 21:00:36','Op. 95 - New World',NULL,6,0,221,1,84),(222,'2018-07-28 21:00:36','Op. 95 - New World',NULL,6,0,223,1,1),(228,'2018-07-28 21:00:36','Op. 88 - G Major',NULL,6,0,229,1,54),(230,'2018-07-28 21:00:36','Op. 88 - G Major',NULL,6,0,231,1,84),(232,'2018-07-28 21:00:36','Op. 88 - G Major',NULL,6,0,233,1,1),(238,'2018-07-28 21:00:36','Op. 62 - Homeland',NULL,6,0,239,1,54),(240,'2018-07-28 21:00:36','Op. 62 - Homeland',NULL,6,0,241,1,84),(242,'2018-07-28 21:00:36','Op. 62 - Homeland',NULL,6,0,243,1,1),(248,'2018-07-28 21:00:36','First session',NULL,6,0,249,52,51),(250,'2018-07-28 21:00:36','First session',NULL,6,0,251,52,9),(252,'2018-07-28 21:00:36','First session',NULL,6,0,253,52,1),(258,'2018-07-28 21:00:36','Coffee Niles + Frasier',NULL,5,0,259,9,1);
/*!40000 ALTER TABLE `notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `place`
--

DROP TABLE IF EXISTS `place`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `place` (
  `id` int(11) NOT NULL,
  `east` float NOT NULL,
  `north` float NOT NULL,
  `city` varchar(255) DEFAULT NULL,
  `country` int(11) DEFAULT NULL,
  `street` varchar(255) DEFAULT NULL,
  `zip` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `place`
--

LOCK TABLES `place` WRITE;
/*!40000 ALTER TABLE `place` DISABLE KEYS */;
INSERT INTO `place` VALUES (105,549968,5272950,'Seattle',NULL,NULL,0);
/*!40000 ALTER TABLE `place` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `result`
--

DROP TABLE IF EXISTS `result`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `result` (
  `id` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `mark` int(11) NOT NULL,
  `marked` datetime DEFAULT NULL,
  `seen` datetime DEFAULT NULL,
  `signed_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKplw1mprcs4dq47vbixvhi74ww` (`signed_id`),
  KEY `FKpjjrrf0483ih2cvyfmx70a16b` (`user_id`),
  CONSTRAINT `FKpjjrrf0483ih2cvyfmx70a16b` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FKplw1mprcs4dq47vbixvhi74ww` FOREIGN KEY (`signed_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `result`
--

LOCK TABLES `result` WRITE;
/*!40000 ALTER TABLE `result` DISABLE KEYS */;
INSERT INTO `result` VALUES (207,NULL,0,NULL,NULL,NULL,1),(214,NULL,0,NULL,NULL,NULL,54),(215,NULL,0,NULL,NULL,NULL,84),(217,NULL,0,NULL,NULL,NULL,1),(224,NULL,0,NULL,NULL,NULL,54),(225,NULL,0,NULL,NULL,NULL,84),(227,NULL,0,NULL,NULL,NULL,1),(234,NULL,0,NULL,NULL,NULL,54),(235,NULL,0,NULL,NULL,NULL,84),(237,NULL,0,NULL,NULL,NULL,1),(244,NULL,0,NULL,NULL,NULL,54),(245,NULL,0,NULL,NULL,NULL,84),(247,NULL,0,NULL,NULL,NULL,1),(254,NULL,0,NULL,NULL,NULL,51),(255,NULL,0,NULL,NULL,NULL,9),(257,NULL,0,NULL,NULL,NULL,1),(260,NULL,0,NULL,NULL,NULL,9);
/*!40000 ALTER TABLE `result` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `review`
--

DROP TABLE IF EXISTS `review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `review` (
  `id` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `mark` int(11) NOT NULL,
  `course_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKprox8elgnr8u5wrq1983degk` (`course_id`),
  KEY `FKiyf57dy48lyiftdrf7y87rnxi` (`user_id`),
  CONSTRAINT `FKiyf57dy48lyiftdrf7y87rnxi` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FKprox8elgnr8u5wrq1983degk` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `review`
--

LOCK TABLES `review` WRITE;
/*!40000 ALTER TABLE `review` DISABLE KEYS */;
/*!40000 ALTER TABLE `review` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `significancy`
--

DROP TABLE IF EXISTS `significancy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `significancy` (
  `id` int(11) NOT NULL,
  `courseMark` double NOT NULL,
  `totalMark` double NOT NULL,
  `userMark` double NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK2rd9kgba9f74qsrv4w4jdljp2` (`user_id`),
  CONSTRAINT `FK2rd9kgba9f74qsrv4w4jdljp2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `significancy`
--

LOCK TABLES `significancy` WRITE;
/*!40000 ALTER TABLE `significancy` DISABLE KEYS */;
INSERT INTO `significancy` VALUES (18,0,0.24375000000000002,0.48750000000000004,13),(24,1,1.24375,0.48750000000000004,19),(35,0,0.05625,0.1125,30),(41,0,0.05625,0.1125,36),(48,0,0.05,0.1,45),(49,0,0.05,0.1,28),(67,0,0.05625,0.1125,62),(69,0,0.18828124999999998,0.37656249999999997,54),(73,1,1.05625,0.1125,52),(79,0.5,0.55625,0.1125,74),(82,0,0.05,0.1,51),(87,0,0.05,0.1,84),(88,1.5,1.55,0.1,9),(92,1,1.05,0.1,1),(96,0,0.05,0.1,93),(100,0,0.05,0.1,97),(104,0,0.05,0.1,101);
/*!40000 ALTER TABLE `significancy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tag`
--

DROP TABLE IF EXISTS `tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tag` (
  `name` varchar(255) NOT NULL,
  `field_name` varchar(255) NOT NULL,
  PRIMARY KEY (`name`,`field_name`),
  KEY `FKyfcyrdfbc4jxrn5s8bayuacv` (`field_name`),
  CONSTRAINT `FKyfcyrdfbc4jxrn5s8bayuacv` FOREIGN KEY (`field_name`) REFERENCES `field` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tag`
--

LOCK TABLES `tag` WRITE;
/*!40000 ALTER TABLE `tag` DISABLE KEYS */;
INSERT INTO `tag` VALUES ('Antonin Dvorak','Music'),('Basketball','Sport'),('Basketball Tricks','Skills'),('C. G. Jung','Psychology'),('Chess','Skills'),('Chess','Sport'),('Cooking','Skills'),('Fitness','Sport'),('Introduction','Psychology'),('Loosing weight','Sport'),('Piano','Music'),('Sigmund Freud','Psychology'),('Tricks in Kitchen','Food'),('Wine','Food');
/*!40000 ALTER TABLE `tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `active` bit(1) NOT NULL,
  `birthday` date DEFAULT NULL,
  `cover` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `logged` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `registered` datetime DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `place_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKbmpyfnf3uo5ni2p1no20e6eoj` (`place_id`),
  CONSTRAINT `FKbmpyfnf3uo5ni2p1no20e6eoj` FOREIGN KEY (`place_id`) REFERENCES `place` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'\0',NULL,NULL,'niles.crane@learnme.com',NULL,NULL,'Niles',NULL,'2018-07-28 21:00:35','Crane',NULL),(9,'\0',NULL,NULL,'frasier@frasier.com',NULL,NULL,'Frasier',NULL,'2018-07-28 21:00:35','Crane',NULL),(13,'\0',NULL,NULL,'frederick@frasier.com',NULL,NULL,'Frederick',NULL,'2018-07-28 21:00:35','Crane',NULL),(19,'\0',NULL,NULL,'devil@frasier.com',NULL,NULL,'Lilith',NULL,'2018-07-28 21:00:35','Sternin',NULL),(28,'\0',NULL,NULL,'martin@frasier.com',NULL,NULL,'Martin',NULL,'2018-07-28 21:00:35','Crane',NULL),(30,'\0',NULL,NULL,'daphne@frasier.com',NULL,NULL,'Daphne',NULL,'2018-07-28 21:00:35','Moon',NULL),(36,'\0',NULL,NULL,'eddie@frasier.com',NULL,NULL,'Eddie',NULL,'2018-07-28 21:00:35',NULL,NULL),(45,'\0',NULL,NULL,'sherry@frasier.com',NULL,NULL,'Sherry',NULL,'2018-07-28 21:00:35','Dempsey',NULL),(51,'\0',NULL,NULL,'boss@frasier.com',NULL,NULL,'Kenny',NULL,'2018-07-28 21:00:35','Dally',NULL),(52,'\0',NULL,NULL,'bulldock@frasier.com',NULL,NULL,'Bob',NULL,'2018-07-28 21:00:35','Briscoe',NULL),(54,'\0',NULL,NULL,'roz@frasier.com',NULL,NULL,'Roz',NULL,'2018-07-28 21:00:35','Doyle',NULL),(62,'\0',NULL,NULL,'noel@frasier.com',NULL,NULL,'Noel',NULL,'2018-07-28 21:00:35','Schempsky',NULL),(74,'\0',NULL,NULL,'cooker@frasier.com',NULL,NULL,'Gil',NULL,'2018-07-28 21:00:35','Chesterton',NULL),(84,'\0',NULL,NULL,'faye@frasier.com',NULL,NULL,'Faye',NULL,'2018-07-28 21:00:35','Moskowitz',NULL),(93,'\0',NULL,NULL,'ghost@frasier.com',NULL,NULL,'Maris',NULL,'2018-07-28 21:00:35','Crane',NULL),(97,'\0',NULL,NULL,'ellis@frasier.com',NULL,NULL,'Ellis',NULL,'2018-07-28 21:00:35','Doyle',NULL),(101,'\0',NULL,NULL,'mel@frasier.com',NULL,NULL,'Mel',NULL,'2018-07-28 21:00:35','Karnofsky',NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_followers`
--

DROP TABLE IF EXISTS `user_followers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_followers` (
  `followers` int(11) NOT NULL,
  `following` int(11) NOT NULL,
  PRIMARY KEY (`followers`,`following`),
  KEY `FK597gbl036c4r1utl3d5h7cpet` (`following`),
  CONSTRAINT `FK1970fpe23ihrmgecn1ikwupef` FOREIGN KEY (`followers`) REFERENCES `user` (`id`),
  CONSTRAINT `FK597gbl036c4r1utl3d5h7cpet` FOREIGN KEY (`following`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_followers`
--

LOCK TABLES `user_followers` WRITE;
/*!40000 ALTER TABLE `user_followers` DISABLE KEYS */;
INSERT INTO `user_followers` VALUES (9,1),(9,13),(9,19),(28,1),(28,9),(28,30),(28,36),(30,1),(45,28),(51,9),(51,52),(51,54),(51,62),(51,74),(52,54),(62,54),(84,9);
/*!40000 ALTER TABLE `user_followers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'learnme'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-27 16:11:42
